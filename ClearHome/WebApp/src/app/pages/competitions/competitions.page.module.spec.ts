import { CompetitionsPageModule } from './competitions.page.module';

describe('HeirarchyPageModule', () => {
  let competitionsModule: CompetitionsPageModule;

  beforeEach(() => {
    competitionsModule = new CompetitionsPageModule();
  });

  it('should create an instance', () => {
    expect(competitionsModule).toBeTruthy();
  });
});
