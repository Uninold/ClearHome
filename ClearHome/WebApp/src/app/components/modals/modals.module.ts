import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { NgSelectModule } from '@ng-select/ng-select';
import { EmbedVideoService } from 'ngx-embed-video';


import { EMPLOYEE_MODALS } from './employee/modals';
import { ACCOUNT_MODALS } from './accounts/modals';
import { COMPETITIONS_MODALS } from './competitions/modals';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ArchwizardModule,
    NgSelectModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    EMPLOYEE_MODALS,
    ACCOUNT_MODALS,
    COMPETITIONS_MODALS
  ],
  entryComponents: [
    EMPLOYEE_MODALS,
    ACCOUNT_MODALS,
    COMPETITIONS_MODALS
  ],
  providers: [EmbedVideoService]
})
export class ModalsModule { }
