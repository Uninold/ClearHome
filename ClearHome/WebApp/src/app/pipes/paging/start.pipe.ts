import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'startPage'
})
export class StartPagePipe implements PipeTransform {

  transform(total_filtered: number, args?: any): any {
    if (total_filtered > 0) {
      return (args.page - 1) * args.limit + 1;
    }
    return 0;
  }

}
