import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'endPage'
})
export class EndPagePipe implements PipeTransform {

  transform(total_filtered: any, args?: any): any {
    if (total_filtered > 0) {
      if (args.limit * args.page > args.total) {
        return args.total;
      }
      return args.limit * args.page;
    }
    return 0;
  }

}
