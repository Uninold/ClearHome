﻿using System;
using System.Collections.Generic;
using RepModels = ClearHomeEntity.Models.Representative;

namespace ClearHomeEntity.Factories.Representative
{
    public class RepresentativeFactory
    {
        public static RepModels.RepTSIScore GetRepresentative(RepModels.RepTSIScoreModel rep)
        {
            var repItem = new RepModels.RepTSIScore
            {
                cms_id = rep.CMSID,
                name = rep.RepName,
                regional_name = rep.RegionalName,
                manager_name = rep.ManagerName,
                tsi = Math.Round((decimal)rep.TSI, 2),
                qsi = Math.Round((decimal)rep.QSI, 2),
                dtv_activated = rep.DTVActivated,
                auto_pay = Math.Round((decimal)0, 2),
                low_risk = Math.Round((decimal)0, 2),
                processing_fee = Math.Round((decimal)0, 2),
                same_day = Math.Round((decimal)0, 2),
                dtv_sold = rep.DTVSold,
                internet_sold = rep.InternetSold,
                cellphone_sold = rep.CellPhoneSold,
                security_sold = rep.SecuritySold,
                total_sold = rep.DTVSold + rep.InternetSold + rep.InternetSold + rep.CellPhoneSold + rep.SecuritySold
            };
            if (rep.AutoPay > 0 && rep.DTVActivated > 0)
            {
                repItem.auto_pay = Math.Round((decimal)(rep.AutoPay / rep.DTVActivated), 2);
            }
            else
            {
                repItem.auto_pay = 0;
            }
            if (rep.LowRisk > 0 && rep.DTVActivated > 0)
            {
                repItem.low_risk = Math.Round((decimal)(rep.LowRisk / rep.DTVActivated), 2);
            }
            else
            {
                repItem.low_risk = 0;
            }
            if (rep.ProcessingFee > 0 && rep.DTVActivated > 0)
            {
                repItem.processing_fee = Math.Round((decimal)(rep.ProcessingFee / rep.DTVActivated), 2);
            }
            else
            {
                repItem.processing_fee = 0;
            }
            if (rep.SameDay > 0 && rep.DTVActivated > 0)
            {
                repItem.same_day = Math.Round((decimal)(rep.SameDay / rep.DTVActivated), 2);
            }
            else
            {
                repItem.same_day = 0;
            }
            return repItem;
        }

        public static RepModels.RepTSISales GetRepresentative(RepModels.RepTSISalesModel rep)
        {
            var repItem = new RepModels.RepTSISales
            {
                work_order_id = rep.WorkOrderID,
                cms_id = rep.CMSID,
                name = rep.RepName,
                tsi = Math.Round((decimal)rep.TSI, 2),
                qsi = Math.Round((decimal)rep.QSI, 2),
                dtv_activated = rep.DTVActivated,
                auto_pay = Math.Round((decimal)0, 2),
                low_risk = Math.Round((decimal)0, 2),
                processing_fee = Math.Round((decimal)0, 2),
                same_day = Math.Round((decimal)0, 2),
                dtv_sold = rep.DTVSold,
                internet_sold = rep.InternetSold,
                cellphone_sold = rep.CellPhoneSold,
                security_sold = rep.SecuritySold,
                total_sold = rep.DTVSold + rep.InternetSold + rep.InternetSold + rep.CellPhoneSold + rep.SecuritySold
            };
            if (rep.AutoPay > 0)
            {
                repItem.auto_pay = Math.Round((decimal)(rep.AutoPay / rep.DTVActivated), 2);
            }
            if (rep.LowRisk > 0)
            {
                repItem.low_risk = Math.Round((decimal)(rep.LowRisk / rep.DTVActivated), 2);
            }
            if (rep.ProcessingFee > 0)
            {
                repItem.processing_fee = Math.Round((decimal)(rep.ProcessingFee / rep.DTVActivated), 2);
            }
            if (rep.SameDay > 0)
            {
                repItem.same_day = Math.Round((decimal)(rep.SameDay / rep.DTVActivated), 2);
            }
            return repItem;
        }

        public static List<RepModels.RepTSIScore> GetRepresentatives(ICollection<RepModels.RepTSIScoreModel> reps)
        {
            var repList = new List<RepModels.RepTSIScore>();
            foreach (var rep in reps)
            {
                repList.Add(GetRepresentative(rep));
            }
            return repList;
        }

        public static List<RepModels.RepTSISales> GetRepresentatives(ICollection<RepModels.RepTSISalesModel> reps)
        {
            var repList = new List<RepModels.RepTSISales>();
            foreach (var rep in reps)
            {
                repList.Add(GetRepresentative(rep));
            }
            return repList;
        }

    }
}
