export class EmployeeListModel {
  id: number;
  first_name: string;
  last_name: string;
  phone_number: string;
  email: string;
  type: string;
}
