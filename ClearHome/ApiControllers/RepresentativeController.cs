﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Representative;
using ClearHomeEntity.Factories.Representative;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using RepModels = ClearHomeEntity.Models.Representative;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Representative API Controller
    /// </summary>
    [RoutePrefix("api/representatives")]
    public class RepresentativeController : ApiController
    {
        /// <summary>
        /// Get Representative TSI Scores
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Representatives" })]
        [HttpGet, ValidateModel, Route("tsi-scores")]
        public async Task<HttpResponseMessage> GetRepTSIScores([FromUri]RepTSIScoresQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    // Check if there is regional name
                    data.fromDate.AddHours(0);
                    data.toDate.AddHours(24);
                    query = query.Where(x => x.TSIDate >= data.fromDate && x.TSIDate <= data.toDate);
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.regional_name))
                    {
                        query = query.Where(x => x.RegionalName.Contains(data.regional_name));
                    }
                    
                    // Check if there is manager name
                    //if (!CommonUtil.IsNullEmptyWhitespace(data?.manager_name))
                    //{
                    //    query = query.Where(x => x.ManagerName.Contains(data.manager_name));
                    //}

                    // Get total reps tsi scores
                    total = query.GroupBy(x => x.RepName).Count();

                    var query2 = query.GroupBy(x => x.RepName).Select(x => new RepModels.RepTSIScoreModel
                    {
                        CMSID = x.FirstOrDefault().RepId,
                        RepName = x.Key,
                        //ManagerName = x.FirstOrDefault().ManagerName,
                        RegionalName = x.FirstOrDefault().RegionalName,
                        TSI = x.Sum(s => s.TSI),
                        QSI = 0,
                        DTVActivated =  x.Where(s => s.DTVActivated == true).Count(),
                        DTVSold = x.Where(s => s.DTVSold == true).Count(),
                        InternetSold = x.Where(s => s.InternetSold == true).Count(),
                        CellPhoneSold = x.Where(s => s.CellPhoneSold == true).Count(),
                        SecuritySold = x.Where(s => s.SecuritySold == true).Count(),
                        AutoPay = x.Where(s => s.AutoPay == true).Count(),
                        LowRisk = x.Where(s => s.LowRisk == true).Count(),
                        ProcessingFee = x.Where(s => s.ProcessingFee == true).Count(),
                        SameDay = x.Where(s => s.SameDay == true).Count(),
                    }).AsQueryable();

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query2 = query2.Where(x => x.RepName.Contains(data.search));
                    }

                    query2 = query2.OrderByDescending(x => x.TSI).ThenBy(x => x.RepName);

                    //var tsiScores = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    var tsiScores = await query2.ToListAsync();

                    total_filtered = tsiScores.Count();

                    var tsiScoreList = RepresentativeFactory.GetRepresentatives(tsiScores);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tsi_scores = tsiScoreList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get Representative TSI Sales
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Representatives" })]
        [HttpGet, ValidateModel, Route("tsi-sales")]
        public async Task<HttpResponseMessage> GetRepTSISales([FromUri]RepTSISalesQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    // Check if there is cms id
                    if (data?.cms_id != null && data?.cms_id != 0)
                    {
                        query = query.Where(x => x.RepId == data.cms_id);
                    }

                    // Get total reps tsi scores
                    total = query.Count();

                    var query2 = query.Select(x => new RepModels.RepTSISalesModel
                    {
                        WorkOrderID = x.Id,
                        CMSID = x.RepId,
                        RepName = x.RepName,
                        TSI = x.TSI,
                        QSI = 0,
                        DTVActivated = Convert.ToInt32(x.DTVActivated),
                        DTVSold = Convert.ToInt32(x.DTVSold),
                        InternetSold = Convert.ToInt32(x.InternetSold),
                        CellPhoneSold = Convert.ToInt32(x.CellPhoneSold),
                        SecuritySold = Convert.ToInt32(x.SecuritySold),
                        AutoPay = Convert.ToInt32(x.AutoPay),
                        LowRisk = Convert.ToInt32(x.LowRisk),
                        ProcessingFee =  Convert.ToInt32(x.ProcessingFee),
                        SameDay = Convert.ToInt32(x.SameDay)
                    }).AsQueryable();

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query2 = query2.Where(x => x.RepName.Contains(data.search));
                    }

                    query2 = query2.OrderByDescending(x => x.TSI).ThenBy(x => x.RepName);

                    var tsiSales = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    total_filtered = tsiSales.Count();

                    var tsiSaleList = RepresentativeFactory.GetRepresentatives(tsiSales);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tsi_sales = tsiSaleList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get Representative Names
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Representatives" })]
        [HttpGet, ValidateModel, Route("names")]
        public async Task<HttpResponseMessage> GetRepNames([FromUri]RepNamesQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query = query.Where(x => x.RepName.Contains(data.search));
                    }

                    var repNames = await query.GroupBy(x => x.RepName).Select(x => x.Key).OrderBy(x => x).ToArrayAsync();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        rep_names = repNames,
                        total = repNames.Length
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}
