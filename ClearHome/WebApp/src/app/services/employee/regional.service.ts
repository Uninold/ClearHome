import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';

@Injectable()
export class RegionalService {

  constructor(private _http: HttpClient) { }

  getRegionalNames(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}regionals/names`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getRegionalTsiScores(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 50 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search,
      fromDate: args.fromDate,
      toDate: args.toDate
    });
    return this._http.get(`${ServerConfig.API}regionals/tsi-scores`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getRegionalTsiSales(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      cms_id: typeof args.cms_id === 'undefined' ? null : args.cms_id,
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 50 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}regionals/tsi-sales`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
