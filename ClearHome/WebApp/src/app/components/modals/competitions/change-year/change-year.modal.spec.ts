import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeYearModalComponent } from './change-year.modal';

describe('ChangeYearModalComponent', () => {
  let component: ChangeYearModalComponent;
  let fixture: ComponentFixture<ChangeYearModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeYearModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeYearModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
