import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeListsPageComponent } from './lists/lists.page';
import { EmployeeAccountsPageComponent } from './accounts/accounts.page';

const routes: Routes = [
  { path: 'lists', component: EmployeeListsPageComponent },
  { path: 'accounts', component: EmployeeAccountsPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeePageRoutingModule { }
