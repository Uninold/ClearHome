import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { RequestHelper } from '../../helpers/request.helper';
import { ServerConfig } from '../../config/server.config';
import { LoginModel, AuthModel } from '../../models/auth.model';

@Injectable()
export class AuthService {

  constructor(private _http: HttpClient) { }

  authenticate(auth_data: LoginModel): Observable<AuthModel> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    });
    const authData = RequestHelper.ToQueryString(auth_data);
    return this._http.put(`${ServerConfig.URL}token`, authData, { headers }).pipe(
      map((response: any) => {
        return <AuthModel>response;
      })
    );
  }

}
