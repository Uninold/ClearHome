import { TestBed, inject } from '@angular/core/testing';

import { RegionalStore } from './regional.store';

describe('RegionalStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegionalStore]
    });
  });

  it('should be created', inject([RegionalStore], (service: RegionalStore) => {
    expect(service).toBeTruthy();
  }));
});
