import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';

import { EmployeeListModel } from '../../../../models/employee.model';
import { EmployeeService } from '../../../../services/employee/employee.service';
import { EmployeeAuthService } from '../../../../services/employee/employee-auth.service';
import { EmployeeAuthStore } from '../../../../stores/employee/employee-auth.store';

@Component({
  selector: 'app-add-account-modal',
  templateUrl: './add-account.modal.html',
  styleUrls: ['./add-account.modal.css']
})
export class AddAccountModalComponent implements OnInit {

  employees$: Observable<EmployeeListModel[]>;
  employeeLoading = false;
  employeeInput$ = new Subject<string>();

  addAccountForm: FormGroup;
  loading: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _employeeService: EmployeeService,
    private _employeeAuthService: EmployeeAuthService,
    private _employeeAuthStore: EmployeeAuthStore
  ) { }

  ngOnInit() {
    this.loading = false;
    this._initForm();
    this._searchEmployees();
  }

  onChangeEmployee($event: any): void {
    if (typeof $event !== 'undefined') {
      this.addAccountForm.patchValue({
        employee_id: $event.id,
        first_name: $event.first_name,
        last_name: $event.last_name,
        email: $event.email,
        phone_number: $event.phone_number
      });
    }
  }

  clearEmployee($event: any): void {
    this.addAccountForm.patchValue({
      employee_id: null,
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      username: '',
      password: '',
      confirm_password: ''
    });
  }

  addAccount() {
    const formData = this.addAccountForm.getRawValue();
    if (this.addAccountForm.valid) {
      this.loading = true;
      this._employeeAuthService.addEmployeeAccount(formData).subscribe(
        data => {
          this.loading = false;
          this._employeeAuthStore.loadEmployeeAccounts().subscribe();
          this.activeModal.close();
        }
      );
    }
  }

  private _initForm(): void {
    this.addAccountForm = this._fb.group({
      employee_id: [null, Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      phone_number: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirm_password: ['', Validators.required]
    });
  }

  private _searchEmployees() {
    this.employees$ = concat(
      of([]), // default items
      this.employeeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.employeeLoading = true),
        switchMap(term => this._employeeService.getAllEmployees({ type: 'Employee', search: term }).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.employeeLoading = false)
        ))
      )
    );
  }

}
