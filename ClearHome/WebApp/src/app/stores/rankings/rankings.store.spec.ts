import { TestBed, inject } from '@angular/core/testing';

import { RankingStore } from './rankings.store';

describe('TsiSalesStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RankingStore]
    });
  });

  it('should be created', inject([RankingStore], (service: RankingStore) => {
    expect(service).toBeTruthy();
  }));
});
