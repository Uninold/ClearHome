import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

declare var Swal: any;

import { AddAccountModalComponent } from '../../../components/modals/accounts/add-account/add-account.modal';
import { EmployeeAuthService } from '../../../services/employee/employee-auth.service';
import { EmployeeAuthStore } from '../../../stores/employee/employee-auth.store';
import { EditAccountModalComponent } from '../../../components/modals/accounts/edit-account/edit-account.modal';

@Component({
  selector: 'app-employee-accounts-page',
  templateUrl: './accounts.page.html',
  styleUrls: ['./accounts.page.css']
})
export class EmployeeAccountsPageComponent implements OnInit {

  employee_accounts$: Observable<any[]>;
  total$: Observable<number>;
  total_filtered$: Observable<number>;
  limit$: Observable<number>;
  loading$: Observable<boolean>;

  page: number;
  search: string;

  constructor(
    private _modal: NgbModal,
    private _employeeAuthStore: EmployeeAuthStore,
    private _employeeAuthService: EmployeeAuthService
  ) { }

  ngOnInit() {
    this.employee_accounts$ = this._employeeAuthStore.employee_accounts;
    this.total$ = this._employeeAuthStore.total;
    this.total_filtered$ = this._employeeAuthStore.total_filtered;
    this.limit$ = this._employeeAuthStore.limit;
    this.loading$ = this._employeeAuthStore.loading;

    this._employeeAuthStore.search.subscribe(search => this.search = search);
    this._employeeAuthStore.page.subscribe(page => this.page = page);

    this._employeeAuthStore.setLoading(true);
    this._employeeAuthStore.setSearch(null);
    this._employeeAuthStore.setPage(1);
    this._employeeAuthStore.loadEmployeeAccounts().subscribe();
  }

  addAccount(): void {
    const modalRef = this._modal.open(AddAccountModalComponent, {
      backdrop: 'static',
      keyboard: false
    });
  }

  editAccount(id: number): void {
    const modalRef = this._modal.open(EditAccountModalComponent, {
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.id = id;
  }

  deleteAccount(id: number): void {
    Swal({
      title: 'Confirmation',
      text: 'Are you sure you want to remove this account?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this._employeeAuthService.deleteEmployeeAccount(id).subscribe(
          data => {
            Swal(
              'Deleted!',
              'User has been successfully removed.',
              'success'
            );
            this._employeeAuthStore.loadEmployeeAccounts().subscribe();
          }
        );
      }
    });
  }

  onSearch($event: any): void {
    if ($event.keyCode === 13) {
      this._employeeAuthStore.setSearch(this.search);
      this._employeeAuthStore.loadEmployeeAccounts().subscribe();
    }
  }

  loadPage(page: number): void {
    if (page > 0) {
      this._employeeAuthStore.setPage(page);
      this._employeeAuthStore.loadEmployeeAccounts().subscribe();
    }
  }

}
