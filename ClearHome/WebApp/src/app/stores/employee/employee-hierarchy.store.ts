import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EmployeeHierarchyService } from '../../services/employee/employee-hierarchy.service';

@Injectable()
export class EmployeeHierarchyStore {
  private readonly _employeehierarchy: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(private _employeeHierarchyService: EmployeeHierarchyService) { }

  get employeeHierarchy() {
    return this._employeehierarchy.asObservable();
  }
  loadEmployeeHierarchy(id: number): Observable<any> {
    return this._employeeHierarchyService.getEmployeeHierarchy({
      id: id
    }).pipe(
      map(data => {
      this._employeehierarchy.next(data);
        return data;
      })
    );
  }
}
