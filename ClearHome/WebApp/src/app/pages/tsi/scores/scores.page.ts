import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
import { UserStore } from '../../../stores/user/user.store';
import { map } from 'rxjs/operators';
import { RegionalStore } from '../../../stores/employee/regional.store';
import { ManagerStore } from '../../../stores/employee/manager.store';
import { RepresentativeStore } from '../../../stores/employee/representative.store';
@Component({
  selector: '[app-tsi-scores-page]',
  templateUrl: './scores.page.html',
  styleUrls: ['./scores.page.css']
})
export class TsiScoresPageComponent implements OnInit, OnDestroy, AfterViewInit {

  tsi_scores$: Observable<any[]>;
  total$: Observable<number>;
  total_filtered$: Observable<number>;
  loading$: Observable<boolean>;
  dateEnd: { year: number, month: number, day: number };
  dateStart: { year: number, month: number, day: number };
  fromDate$: Observable<any>;
  toDate$: Observable<any>;
  private _route$: Subscription;

  constructor(
    private _userStore: UserStore,
    private _regionalStore: RegionalStore,
    private _managerStore: ManagerStore,
    private _repStore: RepresentativeStore,
    private _route: ActivatedRoute,
    private calendar: NgbCalendar
  ) {
    this.fromDate$ = this._userStore.currentFromDate;
    this.toDate$ = this._userStore.currentToDate;
    this._route$ = this._route.params.pipe(
      map(params => {
        if (typeof params.type !== 'undefined') {
          if (params.type === 'regionals') {
            this.tsi_scores$ = this._regionalStore.tsi_scores;
            this.total$ = this._regionalStore.total;
            this.total_filtered$ = this._regionalStore.total_filtered;
            this.loading$ = this._regionalStore.loading;
          } else if (params.type === 'managers') {
            this.tsi_scores$ = this._managerStore.tsi_scores;
            this.total$ = this._managerStore.total;
            this.total_filtered$ = this._managerStore.total_filtered;
            this.loading$ = this._managerStore.loading;
          } else if (params.type === 'reps') {
            this.tsi_scores$ = this._repStore.tsi_scores;
            this.total$ = this._repStore.total;
            this.total_filtered$ = this._repStore.total_filtered;
            this.loading$ = this._repStore.loading;
          }
        }
        return params;
      }),
    ).subscribe(
      params => {
        if (this._userStore.currentFromDateValue.year) {
          if (typeof params.type !== 'undefined') {
            if (params.type === 'regionals') {
              this._regionalStore.setLoading(true);
              this._regionalStore.loadTsiScores(
                this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
                this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
            } else if (params.type === 'managers') {
              this._managerStore.setLoading(true);
              const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
              this._managerStore.loadTsiScores(
                this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
                this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
                regionalName).subscribe();
            } else if (params.type === 'reps') {
              this._repStore.setLoading(true);
              const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
              const managerName = this._route.snapshot.queryParamMap.get('manager-name');
              this._repStore.loadTsiScores(
                this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
                this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
                regionalName, managerName).subscribe();
            }
          }
        } else {
          var date = this.calendar.getToday();
          if (typeof params.type !== 'undefined') {
            if (params.type === 'regionals') {
              this._regionalStore.setLoading(true);
              this._regionalStore.loadTsiScores(
                date.year + '-' + date.month + '-' + date.day,
                date.year + '-' + date.month + '-' + date.day ).subscribe();
            } else if (params.type === 'managers') {
              this._managerStore.setLoading(true);
              const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
              this._managerStore.loadTsiScores(
                date.year + '-' + date.month + '-' + date.day,
                date.year + '-' + date.month + '-' + date.day,
                regionalName).subscribe();
            } else if (params.type === 'reps') {
              this._repStore.setLoading(true);
              const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
              const managerName = this._route.snapshot.queryParamMap.get('manager-name');
              this._repStore.loadTsiScores(
                date.year + '-' + date.month + '-' + date.day,
                date.year + '-' + date.month + '-' + date.day,
                regionalName, managerName).subscribe();
            }
          }
        }
      }
    );
 
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    const chBreadcrumb = $('.ch-breadcrumb .breadcrumb').width();
    $('.ch-breadcrumb .breadcrumb').scrollLeft(chBreadcrumb);
  }

  ngOnDestroy() {

  }
  
  filterdate() {
    this._route$ = this._route.params.pipe(
      map(params => {
        if (typeof params.type !== 'undefined') {
          if (params.type === 'regionals') {
            this.tsi_scores$ = this._regionalStore.tsi_scores;
            this.total$ = this._regionalStore.total;
            this.total_filtered$ = this._regionalStore.total_filtered;
            this.loading$ = this._regionalStore.loading;
          } else if (params.type === 'managers') {
            this.tsi_scores$ = this._managerStore.tsi_scores;
            this.total$ = this._managerStore.total;
            this.total_filtered$ = this._managerStore.total_filtered;
            this.loading$ = this._managerStore.loading;
          } else if (params.type === 'reps') {
            this.tsi_scores$ = this._repStore.tsi_scores;
            this.total$ = this._repStore.total;
            this.total_filtered$ = this._repStore.total_filtered;
            this.loading$ = this._repStore.loading;
          }
        }
        return params;
      }),
    ).subscribe(
      params => {
        if (typeof params.type !== 'undefined') {
          if (params.type === 'regionals') {
            this._regionalStore.setLoading(true);
            this._regionalStore.loadTsiScores(this.dateStart.month + '/' + this.dateStart.day + '/' + this.dateStart.year, this.dateEnd.month + '/' + this.dateEnd.day + '/' + this.dateEnd.year,).subscribe();
          } else if (params.type === 'managers') {
            this._managerStore.setLoading(true);
            const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
            this._managerStore.loadTsiScores(this.dateStart.month + '/' + this.dateStart.day + '/' + this.dateStart.year, this.dateEnd.month + '/' + this.dateEnd.day + '/' + this.dateEnd.year, regionalName).subscribe();
          } else if (params.type === 'reps') {
            this._repStore.setLoading(true);
            const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
            const managerName = this._route.snapshot.queryParamMap.get('manager-name');
            this._repStore.loadTsiScores(this.dateStart.month + '/' + this.dateStart.day + '/' + this.dateStart.year, this.dateEnd.month + '/' + this.dateEnd.day + '/' + this.dateEnd.year, regionalName, managerName).subscribe();
          }
        }
      }
    );
  }
  get type() {
    return this._route.snapshot.paramMap.get('type');
  }

}
