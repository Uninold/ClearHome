import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-change-year-modal',
  templateUrl: './change-year.modal.html',
  styleUrls: ['./change-year.modal.css']
})
export class ChangeYearModalComponent implements OnInit {
  

  constructor(
  ) { }

  ngOnInit() {
  }
  

}
