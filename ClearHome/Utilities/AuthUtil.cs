﻿using ClearHome.Models;
using ClearHomeEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace ClearHome.Utilities
{
    public class AuthUtil
    {
        internal static Dictionary<string, string> GetClaimsData(ClaimsIdentity identity)
        {
            var claims = new Dictionary<string, string>();
            foreach (var claim in identity.Claims)
            {
                claims.Add(claim.Type, claim.Value);
            }
            return claims;
        }

        internal static async Task<AuthUserItem> GetUserDataFromAuth()
        {
            try
            {
                ClaimsIdentity identity = (ClaimsIdentity)HttpContext.Current.User.Identity;

                // If not authorized
                if (!identity.IsAuthenticated)
                {
                    throw new Exception("Unauthorized access");
                }

                // Get user data
                var user = new AuthUserItem();

                var claims = GetClaimsData(identity);
                if (claims.ContainsKey("identity") && claims.ContainsKey("user_id"))
                {
                    using (var conn = new ClearHomeContainer())
                    {
                        int userID = Int32.Parse(claims["user_id"]);
                        if (conn.EmployeeAuths.AsNoTracking().Any(x => x.Id == userID))
                        {
                            user.UserID = userID;
                            user.Identity = claims["identity"].ToString();
                        }
                    }
                }

                return user;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}