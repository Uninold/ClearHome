import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;


import { UserStore } from '../../stores/user/user.store';
import { EmployeeStore } from '../../stores/employee/employee.store';
import { last } from '@angular/router/src/utils/collection';
@Component({
  selector: '[app-home-page]',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
})
export class HomePageComponent implements OnInit {
  employee$: Observable<any>;
  currentUser$: Observable<any>;
  fromDate$: Observable<any>;
  toDate$: Observable<any>;
  tsi_scores$: Observable<any[]>;
  total$: Observable<number>;
  total_filtered$: Observable<number>;
  loading$: Observable<boolean>;
  private _route$: Subscription;
  constructor(
    private calendar: NgbCalendar,
    private _userStore: UserStore,
    private _employeeStore: EmployeeStore,
    private _route: ActivatedRoute,
  ) {
    var date = this.calendar.getToday();
  this.currentUser$ = this._userStore.currentUser;
    this.employee$ = this._employeeStore.employeestat;
    this.fromDate$ = this._userStore.currentFromDate;
    this.toDate$ = this._userStore.currentToDate;
    if (this._userStore.currentFromDateValue.year) {
      this._employeeStore.loadEmployeeStat(
        this._userStore.currentUserEmployeeId,
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
      ).subscribe();
    } else {
      this._employeeStore.loadEmployeeStat(
        this._userStore.currentUserEmployeeId,
        date.year + '-' + date.month + '-' + date.day,
        date.year + '-' + date.month + '-' + date.day
      ).subscribe();
    }
    
    console.log('user', this.currentUser$);
  }

  ngOnInit() {
    

  }

}
