import { HttpResponse } from '@angular/common/http';

export class ResponseHelper {

  public static Format(response: HttpResponse<any>, callback: any, other_api: boolean = false): void {
    const _response = {
      error: 1,
      data: [],
      message: ''
    };
    try {
      if (response.status === 200) {
        if (other_api) {
          callback(response);
        } else {
          callback({
            error: response.body.error,
            data: response.body.data,
            message: response.body.message,
          });
        }
      }
    } catch (e) {
      _response.message = 'Snap! Server Error.';
      if (response.body) {
        _response.data = response.body;
      }
    }
    callback(_response);
  }

}
