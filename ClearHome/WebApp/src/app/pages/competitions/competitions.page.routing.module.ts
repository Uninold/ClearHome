import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompetitionsListsComponent } from './lists/lists.page';
import { CompetitionDetailsComponent } from './competition-details/competition-details.page';
const routes: Routes = [
  { path: 'lists', component: CompetitionsListsComponent },
  { path: 'competition_details/:id', component: CompetitionDetailsComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompetitionsPageRoutingModule { }
