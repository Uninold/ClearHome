import { Route } from '@angular/router';

import { AuthGuard } from '../guards/auth.guard';

import { CommonLayoutComponent } from '../layouts/common/common.layout';

import { HomePageComponent } from '../pages/home/home.page';

// COMMON LAYOUT ROUTES
export const COMMON_ROUTES: Route = {
  path: '',
  component: CommonLayoutComponent,
  children: [
    { path: 'home', component: HomePageComponent, canActivate: [AuthGuard] },
    { path: 'user', loadChildren: './pages/user/user.page.module#UserPageModule', canActivate: [AuthGuard] },
    { path: 'tsi', loadChildren: './pages/tsi/tsi.page.module#TsiPageModule', canActivate: [AuthGuard] },
    { path: 'employee', loadChildren: './pages/employee/employee.page.module#EmployeePageModule', canActivate: [AuthGuard] },
    { path: 'heirarchy', loadChildren: './pages/heirarchy/heirarchy.page.module#HeirarchyPageModule', canActivate: [AuthGuard] },
    { path: 'rankings', loadChildren: './pages/rankings/rankings.page.module#RankingsPageModule', canActivate: [AuthGuard] },
    // tslint:disable-next-line:max-line-length
    { path: 'competitions', loadChildren: './pages/competitions/competitions.page.module#CompetitionsPageModule', canActivate: [AuthGuard] }
  ]
};

