import { TestBed, inject } from '@angular/core/testing';

import { TournamentStore } from './tournament.store';

describe('EmployeeAuthStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TournamentStore]
    });
  });

  it('should be created', inject([TournamentStore], (service: TournamentStore) => {
    expect(service).toBeTruthy();
  }));
});
