import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeirarchyPageRoutingModule } from './heirarchy.page.routing.module';
import { HeirarchyListsComponent } from './lists/lists.page';

@NgModule({
  imports: [
    CommonModule,
    HeirarchyPageRoutingModule
  ],
  declarations: [
    HeirarchyListsComponent
  ]
})
export class HeirarchyPageModule { }
