export class NotoficationHelper {

  public static isUrlNotifiable(url: string): boolean {
    const urls = [
      '/token'
    ];
    for (let index = 0; index < urls.length; index++) {
      if (url.indexOf(urls[index]) > -1) {
        return false;
      }
    }
    return true;
  }

}
