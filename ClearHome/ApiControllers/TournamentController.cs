﻿using ClearHome.Filters;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Tournament;
using ClearHomeEntity.Factories.Tournament;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Representative API Controller
    /// </summary>
    [RoutePrefix("api/tournament")]
    public class TournamentController : ApiController
    {
        /// <summary>
        /// Add Tournament Details
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpPost, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> AddTournament([FromBody]AddTournament data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {

                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;
                    var iconType = typePicture(data.icon);
                    var bgType = typePicture(data.bg_image);
                    data.bg_image = convertPicture(data.bg_image);
                    data.icon = convertPicture(data.icon);

                    byte[] newBytes = System.Convert.FromBase64String(data.bg_image);
                    byte[] newBytess = System.Convert.FromBase64String(data.icon);
                    var tournament = new Tournament
                    {
                        Name = data.name,
                        BgImage = newBytes,
                        BgImageType = bgType,
                        PromotionalVideoLink = data.promotional_video_link,
                        Details = data.details,
                        Icon = newBytess,
                        IconType = iconType,
                        NumberOfParticipants = data.number_of_participants,
                        CreatedBy = data.created_by,
                        CreatedDateTime = DateTime.Now,
                        StartDate = data.start_date,
                        EndDate = data.end_date,
                        Type = data.type,
                        Format = data.format,
                        Status = true,
                        Employee = conn.Employees.Where(x => x.Id == data.created_by).FirstOrDefault()
                };

                    conn.Tournaments.Add(tournament);
               

                    //await conn.SaveChangesAsync();
                    int id = tournament.Id;

                    List<Tournament_Timelines> tournament_timelines = new List<Tournament_Timelines>();

                    foreach (var timeline in data.timelines)
                    {
                        var tournament_timeline = new Tournament_Timelines
                        {
                            TournamentId = timeline.tournament_id,
                            Title = timeline.title,
                            StartDate = timeline.start_date,
                            EndDate = timeline.end_date,
                            Description = timeline.description
                        };
                        conn.Tournament_Timelines.Add(tournament_timeline);
                    }
                    List<Tournament_Prizes> tournament_prizes = new List<Tournament_Prizes>();
                    var counter = 0;
                    foreach (var prize in data.prizes)
                    {
                        counter++;
                        var badgeType = typePicture(prize.badge);
                        prize.badge = convertPicture(prize.badge);
                        byte[] badge = System.Convert.FromBase64String(prize.badge);
                        var tournament_prize = new Tournament_Prizes
                        {
                            TournamentId = id,
                            PrizeTitle = prize.prize_title,
                            Description = prize.description,
                            Points = prize.points,
                            Badge = badge,
                            BadgeType =  badgeType
                        };
                        conn.Tournament_Prizes.Add(tournament_prize);
                    }
                    //if (data.consolation != null)
                    //{
                    //    foreach (var consolation in data.consolation)
                    //    {
                    //        var badgeType = typePicture(consolation.badge);
                    //        consolation.badge = convertPicture(consolation.badge);
                    //        byte[] badge = System.Convert.FromBase64String(consolation.badge);
                    //        var tournament_prize = new Tournament_Prizes
                    //        {
                    //            TournamentId = id,
                    //            PrizeTitle = consolation.prize_title,
                    //            Description = consolation.description,
                    //            Points = 2,
                    //            Badge = badge,
                    //            BadgeType = badgeType
                    //        };
                    //        conn.Tournament_Prizes.Add(tournament_prize);
                    //    }
                    //}
                    
                    var counterseed = 0;
                    List<Tournament_Seeding> tournament_seeding = new List<Tournament_Seeding>();
                    foreach (var seeding in data.seeding)
                    {
                        counterseed++;
                        var tournament_seed = new Tournament_Seeding
                        {
                            TournamentId = id,
                            SeedNumber = counterseed,
                            Participant = seeding.participant
                        };
                        conn.Tournament_Seeding.Add(tournament_seed);
                    }

                    await conn.SaveChangesAsync();

                    //var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournament_id = id,
                        message = "New competition successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        /// <summary>
        /// Get Tournament
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpGet, ValidateModel, Route("{id}")]
        public async Task<HttpResponseMessage> GetTournament(int id)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var tournament = await conn.Tournaments.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
                    if (tournament == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Tournament does not exist");
                    }

                    var tournamentItem = TournamentFactory.GetTournament(tournament);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournament = tournamentItem
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpGet, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> GetTournaments()
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var tournament = await conn.Tournaments.AsNoTracking().ToListAsync();
                    if (tournament == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Tournament does not exist");
                    }

                    var tournamentList = TournamentFactory.GetTournaments(tournament);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournamentList = tournamentList
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Add Prizes
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpPost, ValidateModel, Route("prizes")]
        public async Task<HttpResponseMessage> AddTournamentPrizes([FromBody]List<TournamentPrizesList> data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 100;
                    conn.Configuration.ProxyCreationEnabled = false;
                    List<Tournament_Prizes> tournament_prizes = new List<Tournament_Prizes>();
                    foreach (var prize in data)
                    {
                        var badgeType = typePicture(prize.badge);
                        prize.badge = convertPicture(prize.badge);
                        byte[] badge = System.Convert.FromBase64String(prize.badge);
                        var tournament_prize = new Tournament_Prizes
                        {
                            TournamentId = prize.tournament_id,
                            PrizeTitle = prize.prize_title,
                            Description = prize.description,
                            Points = prize.points,
                            Badge = badge
                        };
                        tournament_prizes.Add(tournament_prize);
                    }


                    await conn.SaveChangesAsync();

                    //var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournament_prizes = tournament_prizes,
                        message = "Prizes successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpPost, ValidateModel, Route("seeding")]
        public async Task<HttpResponseMessage> AddTournamentSeeding([FromBody]List<TournamentSeedingList> data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 100;
                    conn.Configuration.ProxyCreationEnabled = false;
                    List<Tournament_Seeding> tournament_seeding = new List<Tournament_Seeding>();
                    foreach (var seeding in data)
                    {
                        var tournament_seed = new Tournament_Seeding
                        {
                            TournamentId = seeding.tournament_id,
                            SeedNumber = seeding.seed_number,
                            Participant = seeding.participant
                        };
                        tournament_seeding.Add(tournament_seed);
                    }

                    await conn.SaveChangesAsync();

                    //var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournament_seeding = tournament_seeding,
                        message = "Seeding successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpPost, ValidateModel, Route("timelines")]
        public async Task<HttpResponseMessage> AddTournamentTimeline([FromBody]List<TournamentTimelinesList> data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 100;
                    conn.Configuration.ProxyCreationEnabled = false;
                    List<Tournament_Timelines> tournament_timelines = new List<Tournament_Timelines>();

                    foreach (var timeline in data)
                    {
                        var tournament_timeline = new Tournament_Timelines
                        {
                            TournamentId = timeline.tournament_id,
                            Title = timeline.title,
                            StartDate = timeline.start_date,
                            EndDate = timeline.end_date,
                            Description = timeline.description
                        };
                        conn.Tournament_Timelines.Add(tournament_timeline);
                    }

                    await conn.SaveChangesAsync();

                    //var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tournament_timelines = tournament_timelines,
                        message = "timelines successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [SwaggerOperation(Tags = new[] { "Tournament" })]
        [HttpPost, ValidateModel, Route("bracket")]
        public async Task<HttpResponseMessage> AddTournamentBracket([FromBody]List<BracketList> data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 100;
                    conn.Configuration.ProxyCreationEnabled = false;
                    List<ClearHomeEntity.Bracket> brackets = new List<ClearHomeEntity.Bracket>();


                    foreach (var bracket in data)
                    {
                        var tournament_bracket = new Bracket
                        {
                            TournamentId = bracket.tournament_id,
                            Round = bracket.round,
                            WinLose = bracket.win_lose,
                            DatetimeResult = bracket.date_time_result,
                            UpdatedDatetime = bracket.update_date_time,
                            UpdatedBy = bracket.updated_by,
                            GroupNumber = bracket.group_number,
                            RoundDates = bracket.round_dates,
                            Employee = conn.Employees.Where(x => x.Id == bracket.updated_by).FirstOrDefault()
                        };
                        //brackets.Add(tournament_bracket);
                        conn.Brackets.Add(tournament_bracket);
                    }
                   




                    await conn.SaveChangesAsync();

                    //var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        brackets = brackets,
                        message = " Bracket successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        public string convertPicture(string picture){
             var image_data = Regex.Match(picture, @"data:image/(?<type>.+?),(?<data>.+)");
             //user.ImageType = image_data.Groups["type"].Value;
                //user.Image = Convert.FromBase64String(image_data.Groups["data"].Value);
            return image_data.Groups["data"].Value;
        }
        public string typePicture(string picture)
        {
            var image_data = Regex.Match(picture, @"data:image/(?<type>.+?),(?<data>.+)");
            //user.ImageType = image_data.Groups["type"].Value;
            //user.Image = Convert.FromBase64String(image_data.Groups["data"].Value);
            return image_data.Groups["type"].Value;
        }
    }
}
