import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { RankingsService } from '../../services/rankings/rankings.service';

@Injectable()
export class RankingStore {

  private readonly _rankings: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private readonly _total: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _total_filtered: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _page: BehaviorSubject<number> = new BehaviorSubject(1);
  private readonly _limit: BehaviorSubject<number> = new BehaviorSubject(20);
  private readonly _loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(private _rankingsService: RankingsService) { }

  get rankings() {
    return this._rankings.asObservable();
  }

  get total() {
    return this._total.asObservable();
  }

  get total_filtered() {
    return this._total_filtered.asObservable();
  }

  get page() {
    return this._page.asObservable();
  }

  get limit() {
    return this._limit.asObservable();
  }

  get loading() {
    return this._loading.asObservable();
  }

  setLoading(loading: boolean) {
    this._loading.next(loading);
  }

  loadRankings(fromDate: string, toDate: string, search?: string): Observable<any> {
    return this._rankingsService.getRankings({
      limit: this._limit.getValue(),
      page: this._page.getValue(),
      search: search,
      fromDate: fromDate,
      toDate: toDate
    }).pipe(
      map(data => {
      this._rankings.next(data.powerrankinglist);
        //this._total.next(data.total);
        this._total_filtered.next(data.total_filtered);
        this._loading.next(false);
        return data;
      })
    );
  }

}
