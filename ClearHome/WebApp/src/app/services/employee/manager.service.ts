import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';

@Injectable()
export class ManagerService {

  constructor(private _http: HttpClient) { }

  getManagerNames(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}managers/names`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getManagerTsiScores(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      regional_name: typeof args.regional_name === 'undefined' ? null : args.regional_name,
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 50 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search,
      fromDate: args.fromDate,
      toDate: args.toDate
    });
    return this._http.get(`${ServerConfig.API}managers/tsi-scores`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
