import { Injectable } from '@angular/core';
import { AlertService } from 'ngx-alerts';

@Injectable()
export class AlertModalService {

  constructor(private _alert: AlertService) { }

  success(message?: string): void {
    this._alert.success(message);
  }

  danger(message?: string): void {
    this._alert.danger(message);
  }

  info(message?: string): void {
    this._alert.info(message);
  }

  warning(message?: string): void {
    this._alert.warning(message);
  }

}
