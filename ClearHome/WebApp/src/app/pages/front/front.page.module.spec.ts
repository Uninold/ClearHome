import { FrontPageModule } from './front.page.module';

describe('FrontPageModule', () => {
  let frontModule: FrontPageModule;

  beforeEach(() => {
    frontModule = new FrontPageModule();
  });

  it('should create an instance', () => {
    expect(frontModule).toBeTruthy();
  });
});
