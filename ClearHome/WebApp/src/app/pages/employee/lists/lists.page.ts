import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

import { EmployeeStore } from '../../../stores/employee/employee.store';

@Component({
  selector: '[app-employee-lists-page]',
  templateUrl: './lists.page.html',
  styleUrls: ['./lists.page.css']
})
export class EmployeeListsPageComponent implements OnInit {

  employees$: Observable<any[]>;
  total$: Observable<number>;
  total_filtered$: Observable<number>;
  limit$: Observable<number>;
  loading$: Observable<boolean>;

  page: number;
  search: string;
  type: string;

  constructor(
    private _employeeStore: EmployeeStore,
    private _modal: NgbModal
  ) { }

  ngOnInit() {
    this.employees$ = this._employeeStore.employees;
    this.total$ = this._employeeStore.total;
    this.total_filtered$ = this._employeeStore.total_filtered;
    this.limit$ = this._employeeStore.limit;
    this.loading$ = this._employeeStore.loading;

    this._employeeStore.page.subscribe(page => this.page = page);

    this._employeeStore.setLoading(true);
    this._employeeStore.setPage(1);
    this._employeeStore.loadEmployees().subscribe();
  }

  get type_label() {
    let label = 'All Employee Types';
    if (this.type === 'admin') {
      label = 'Admin';
    } else if (this.type === 'president') {
      label = 'President';
    } else if (this.type === 'director') {
      label = 'Director';
    } else if (this.type === 'vp') {
      label = 'VP of Sales';
    } else if (this.type === 'regional') {
      label = 'Regional';
    } else if (this.type === 'manager') {
      label = 'Manager';
    } else if (this.type === 'employee') {
      label = 'Rep';
    }
    return label;
  }

  changeType(type?: string): void {
    this.type = type;
    this._employeeStore.loadEmployees(this.search, type).subscribe();
  }

  onSearch($event: any): void {
    if ($event.keyCode === 13) {
      this._employeeStore.loadEmployees(this.search, this.type).subscribe();
    }
  }

  loadPage(page: number): void {
    if (page > 0) {
      this._employeeStore.setPage(page);
      this._employeeStore.loadEmployees().subscribe();
    }
  }

}
