﻿using System;
using System.Collections.Generic;
using RegionalModels = ClearHomeEntity.Models.Regional;

namespace ClearHomeEntity.Factories.Regional
{
    public class RegionalFactory
    {
        public static RegionalModels.RegionalTSIScore GetRegional(RegionalModels.RegionalTSIScoreModel regional)
        {
            var regionalItem = new RegionalModels.RegionalTSIScore
            {
                name = regional.RegionalName,
                tsi = Math.Round((decimal)regional.TSI, 2),
                qsi = Math.Round((decimal)regional.QSI, 2),
                dtv_activated = regional.DTVActivated,
                auto_pay = Math.Round((decimal)0, 2),
                low_risk = Math.Round((decimal)0, 2),
                processing_fee = Math.Round((decimal)0, 2),
                same_day = Math.Round((decimal)0, 2),
                dtv_sold = regional.DTVSold,
                internet_sold = regional.InternetSold,
                cellphone_sold = regional.CellPhoneSold,
                security_sold = regional.SecuritySold,
                total_sold = regional.DTVSold + regional.InternetSold + regional.InternetSold + regional.CellPhoneSold + regional.SecuritySold
            };
            if (regional.AutoPay > 0 && regional.DTVActivated > 0)
            {
                regionalItem.auto_pay = Math.Round((decimal)(regional.AutoPay / regional.DTVActivated), 2);
            }
            else
            {
                regionalItem.auto_pay = 0;
            }
            if (regional.LowRisk > 0 && regional.DTVActivated > 0)
            {
                regionalItem.low_risk = Math.Round((decimal)(regional.LowRisk / regional.DTVActivated), 2);
            }
            else
            {
                regionalItem.low_risk = 0;
            }
            if (regional.ProcessingFee > 0 && regional.DTVActivated > 0)
            {
                regionalItem.processing_fee = Math.Round((decimal)(regional.ProcessingFee / regional.DTVActivated), 2);
            }
            else
            {
                regionalItem.processing_fee = 0;
            }
            if (regional.SameDay > 0 && regional.DTVActivated > 0)
            {
                regionalItem.same_day = Math.Round((decimal)(regional.SameDay / regional.DTVActivated), 2);
            }
            else
            {
                regionalItem.same_day = 0;
            }
            return regionalItem;
        }

        public static List<RegionalModels.RegionalTSIScore> GetRegionals(ICollection<RegionalModels.RegionalTSIScoreModel> regionals)
        {
            var regionalList = new List<RegionalModels.RegionalTSIScore>();
            foreach (var regional in regionals)
            {
                regionalList.Add(GetRegional(regional));
            }
            return regionalList;
        }

    }
}
