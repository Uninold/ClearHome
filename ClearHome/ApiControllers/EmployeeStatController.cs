﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Employee;
using ClearHomeEntity.DataTransferObjects.Representative;
using ClearHomeEntity.Factories.Representative;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using RepModels = ClearHomeEntity.Models.Representative;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Representative API Controller
    /// </summary>
    [RoutePrefix("api/employeestat")]
    public class EmployeeStatController : ApiController
    {
        /// <summary>
        /// Get Representative TSI Scores
        /// </summary>
        [SwaggerOperation(Tags = new[] { "EmployeeStat" })]
        [HttpPost, ValidateModel, Route("tsi-scores")]
        public async Task<HttpResponseMessage> GetEmployeeStats([FromBody]EmployeeStatQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    // Check if there is id

                    //query = query.Where(x => x.TSIDate > data.fromDate && x.TSIDate < data.toDate);
                    //if (!CommonUtil.IsNullEmptyWhitespace(data?.fromDate))
                    //{
                        data.fromDate.AddHours(0);
                        data.toDate.AddDays(24);
                    query = query.Where(x => x.RepId.Equals(data.id));
                        query = query.Where(x => x.TSIDate > data.fromDate && x.TSIDate < data.toDate);
                    //}

                    // Get total reps tsi scores
                    total = query.GroupBy(x => x.RepName).Count();

                    var query2 = query.GroupBy(x => x.RepName).Select(x => new RepModels.RepTSIScoreModel
                    {
                        CMSID = x.FirstOrDefault().RepId,
                        RepName = x.Key,
                        //ManagerName = x.FirstOrDefault().ManagerName,
                        RegionalName = x.FirstOrDefault().RegionalName,
                        TSI = x.Sum(s => s.TSI),
                        QSI = 0,
                        DTVActivated = x.Where(s => s.DTVActivated == true).Count(),
                        DTVSold = x.Where(s => s.DTVSold == true).Count(),
                        InternetSold = x.Where(s => s.InternetSold == true).Count(),
                        CellPhoneSold = x.Where(s => s.CellPhoneSold == true).Count(),
                        SecuritySold = x.Where(s => s.SecuritySold == true).Count(),
                        AutoPay = x.Where(s => s.AutoPay == true).Count(),
                        LowRisk = x.Where(s => s.LowRisk == true).Count(),
                        ProcessingFee = x.Where(s => s.ProcessingFee == true).Count(),
                        SameDay = x.Where(s => s.SameDay == true).Count()
                    }).AsQueryable();

                    query2 = query2.OrderByDescending(x => x.TSI).ThenBy(x => x.RepName);

                    var tsiScore = await query2.Skip(() => offset).Take(() => limit).FirstOrDefaultAsync();

                    var tsiScoreItem = new RepModels.RepTSIScore
                    {
                        cms_id = 0,
                        name = "",
                        manager_name = "",
                        regional_name = "",
                        tsi = 0,
                        qsi = 0,
                        dtv_activated = 0,
                        dtv_sold = 0,
                        internet_sold = 0,
                        cellphone_sold = 0,
                        security_sold = 0,
                        auto_pay = 0,
                        low_risk = 0,
                        processing_fee = 0,
                        same_day = 0,
                        total_sold = 0
                    };
                    if(tsiScore != null)
                    {
                        tsiScoreItem = RepresentativeFactory.GetRepresentative(tsiScore);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employeestat = tsiScoreItem
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
