export class LoginModel {
  username: string;
  password: string;
  grant_type: string;
}

export class AuthModel {
  access_token: string;
  expires_in: number;
  token_type: string;
}
