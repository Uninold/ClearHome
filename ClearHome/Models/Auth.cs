﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClearHome.Models
{
    public class AuthUserItem
    {
        public int UserID { get; set; }
        public string Identity { get; set; }
    }

}