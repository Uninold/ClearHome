﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.EmployeeAuth;
using ClearHomeEntity.Factories.EmployeeAuth;
using Newtonsoft.Json;
using RestSharp;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EmployeeAuthModels = ClearHomeEntity.Models.EmployeeAuth;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Employee Auth API Controller
    /// </summary>
    [RoutePrefix("api/employee/auth")]
    public class EmployeeAuthController : ApiController
    {
        /// <summary>
        /// Get Employee Accounts
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Auth" })]
        [HttpGet, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> GetEmployeeAccounts([FromUri]EmployeeAuthsQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.EmployeeAuths.AsNoTracking().Where(x => x.IsSuperAdmin == false).AsQueryable();

                    // Get employee lists
                    total = query.Count();

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query = query.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || String.Concat(x.FirstName, " ", x.LastName).Contains(data.search) || x.Email.Contains(data.search) || x.Phone.Contains(data.search));
                    }

                    // Check if there is ids
                    if (data?.ids != null && data?.ids.Length > 0)
                    {
                        query = query.Where(x => data.ids.Contains(x.Id));
                    }

                    query = query.OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ThenBy(x => x.Email);

                    var employeeAccounts = await query.Skip(() => offset).Take(() => limit).ToListAsync();
                    total_filtered = employeeAccounts.Count();

                    var employeeAccountList = EmployeeAuthFactory.GetEmployeeAccounts(employeeAccounts);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employee_accounts = employeeAccountList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get Employee Account
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Auth" })]
        [HttpGet, ValidateModel, Route("{id}")]
        public async Task<HttpResponseMessage> GetEmployeeAccount(int id)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var employeeAccount = await conn.EmployeeAuths.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
                    if(employeeAccount == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Employee account does not exist");
                    }

                    var employeeAccountItem = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employee_account = employeeAccountItem
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Add Employee Account
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Auth" })]
        [HttpPost, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> AddEmployeeAccount([FromBody]AddEmployeeAuthDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;

                    // Check if password and confirm password match
                    if(data.password != data.confirm_password)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Password does not match");
                    }

                    // Check if employee already exist
                    if (conn.EmployeeAuths.AsNoTracking().Any(x => x.EmployeeId == data.employee_id))
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "This employee already have an account");
                    }

                    var employeeAccount = new EmployeeAuth
                    {
                        EmployeeId = data.employee_id,
                        FirstName = data.first_name,
                        LastName = data.last_name,
                        Email = data.email,
                        Phone = data.phone_number,
                        Username = data.username,
                        Password = data.password,
                        IsSuperAdmin = false
                    };
                    string DoceboUserName = "ch_" + data.username;
                    string DoceboPassword = data.password;

                    // Create Docebo Account
                    var httpClient = new RestClient("https://clear-home.docebosaas.com");
                    // Get token to be authorized
                    var oAuthRequest = new RestRequest("oauth2/token", Method.POST);
                    string oauthParams = JsonConvert.SerializeObject(new
                    {
                        grant_type = "password",
                        client_id = "hubtulgy-client",
                        client_secret = "0f7c732c205b3377feed76b1a5e3b8357ff956eb",
                        username = "admin",
                        password = "Clearhome",
                        scope = "api"
                    });
                    oAuthRequest.AddParameter("application/json; charset=utf-8", oauthParams, ParameterType.RequestBody);
                    var oAuthResponse = httpClient.Execute(oAuthRequest);
                    if (oAuthResponse.StatusCode == HttpStatusCode.OK)
                    {
                        // If authorized
                        var oAuthResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthSuccess>(oAuthResponse.Content);
                        // Create docebo user account
                        var userRequest = new RestRequest("manage/v1/user", Method.POST);
                        string userParams = JsonConvert.SerializeObject(new
                        {
                            userid = DoceboUserName,
                            password = DoceboPassword,
                            data.first_name,
                            data.last_name
                        });
                        userRequest.AddHeader("Authorization", "Bearer " + oAuthResponseContent.access_token);
                        userRequest.AddParameter("application/json; charset=utf-8", userParams, ParameterType.RequestBody);
                        var userResponse = httpClient.Execute(userRequest);
                        if (userResponse.StatusCode != HttpStatusCode.OK)
                        {
                            // If there are errors in adding user in docebo
                            var userErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserCreationError>(userResponse.Content);
                            string userErrorMessages = string.Join("<br/>", userErrorContent.message.Select(x => x.message));
                            return Request.CreateErrorResponse(userResponse.StatusCode, "Docebo User Creation Error(s): " + userErrorMessages);
                        }
                        // If docebo user successfully added
                        var userResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserCreationSuccess>(userResponse.Content);
                        // Set docebo user
                        employeeAccount.DoceboUserID = userResponseContent.data.user_id;
                        employeeAccount.DoceboUserName = DoceboUserName;
                        employeeAccount.DoceboPassword = DoceboPassword;
                    }
                    else
                    {
                        // If there is error in authorization
                        var oAuthErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthError>(oAuthResponse.Content);
                        return Request.CreateErrorResponse(oAuthResponse.StatusCode, "Docebo Authorization Error: " + oAuthErrorContent.error_description);
                    }

                    conn.EmployeeAuths.Add(employeeAccount);

                    // Save changes
                    await conn.SaveChangesAsync();

                    var newEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employee_account = newEmployeeAccount,
                        message = "New employee account successfully added"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Update Employee Account
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Auth" })]
        [HttpPut, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> UpdateEmployeeAccount([FromBody]UpdateEmployeeAuthDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;

                    // Check if password and confirm password match
                    if(!CommonUtil.IsNullEmptyWhitespace(data.password))
                    {
                        if (data.password != data.confirm_password)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Password does not match");
                        }
                    }

                    // Check if employee already exist
                    if (conn.EmployeeAuths.AsNoTracking().Any(x => x.Id != data.id && x.EmployeeId == data.employee_id))
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "This employee already have an account");
                    }

                    var employeeAccount = await conn.EmployeeAuths.FirstOrDefaultAsync(x => x.Id == data.id);
                    employeeAccount.EmployeeId = data.employee_id;
                    employeeAccount.FirstName = data.first_name;
                    employeeAccount.LastName = data.last_name;
                    employeeAccount.Email = data.email;
                    employeeAccount.Phone = data.phone_number;
                    employeeAccount.Username = data.username;

                    string DoceboUserName = "ch_" + data.username;
                    string DoceboPassword = employeeAccount.DoceboPassword;
                    if (employeeAccount.DoceboPassword == null)
                    {
                        DoceboPassword = employeeAccount.Password;
                    }
                    if (!CommonUtil.IsNullEmptyWhitespace(data.password))
                    {
                        DoceboPassword = data.password;
                    }

                    // Create/Update Docebo Account
                    var httpClient = new RestClient("https://clear-home.docebosaas.com");
                    // Get token to be authorized
                    var oAuthRequest = new RestRequest("oauth2/token", Method.POST);
                    string oauthParams = JsonConvert.SerializeObject(new
                    {
                        grant_type = "password",
                        client_id = "hubtulgy-client",
                        client_secret = "0f7c732c205b3377feed76b1a5e3b8357ff956eb",
                        username = "admin",
                        password = "Clearhome",
                        scope = "api"
                    });
                    oAuthRequest.AddParameter("application/json; charset=utf-8", oauthParams, ParameterType.RequestBody);
                    var oAuthResponse = httpClient.Execute(oAuthRequest);
                    if (oAuthResponse.StatusCode == HttpStatusCode.OK)
                    {
                        // If authorized
                        var oAuthResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthSuccess>(oAuthResponse.Content);
                        
                        // Check if docebo user id is null
                        if (employeeAccount.DoceboUserID == null)
                        {
                            // Create docebo user account
                            var userRequest = new RestRequest("manage/v1/user", Method.POST);
                            string userParams = JsonConvert.SerializeObject(new
                            {
                                userid = DoceboUserName,
                                password = DoceboPassword,
                                first_name = employeeAccount.FirstName,
                                last_name = employeeAccount.LastName
                            });
                            userRequest.AddHeader("Authorization", "Bearer " + oAuthResponseContent.access_token);
                            userRequest.AddParameter("application/json; charset=utf-8", userParams, ParameterType.RequestBody);
                            var userResponse = httpClient.Execute(userRequest);
                            if (userResponse.StatusCode != HttpStatusCode.OK)
                            {
                                // If there are errors in adding user in docebo
                                var userErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserCreationError>(userResponse.Content);
                                string userErrorMessages = string.Join("<br/>", userErrorContent.message.Select(x => x.message));
                                return Request.CreateErrorResponse(userResponse.StatusCode, "Docebo User Creation Error(s): " + userErrorMessages);
                            }
                            // If docebo user successfully added
                            var userResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserCreationSuccess>(userResponse.Content);
                            // Set docebo user
                            employeeAccount.DoceboUserID = userResponseContent.data.user_id;
                            employeeAccount.DoceboUserName = DoceboUserName;
                            employeeAccount.DoceboPassword = DoceboPassword;
                        }
                        else
                        {
                            // Update docebo user account
                            var userRequest = new RestRequest("manage/v1/user/" + employeeAccount.DoceboUserID, Method.PUT);
                            string userParams = JsonConvert.SerializeObject(new
                            {
                                userid = DoceboUserName,
                                password = DoceboPassword,
                                first_name = employeeAccount.FirstName,
                                last_name = employeeAccount.LastName
                            });
                            userRequest.AddHeader("Authorization", "Bearer " + oAuthResponseContent.access_token);
                            userRequest.AddParameter("application/json; charset=utf-8", userParams, ParameterType.RequestBody);
                            var userResponse = httpClient.Execute(userRequest);
                            if (userResponse.StatusCode != HttpStatusCode.OK)
                            {
                                // If there are errors in updating user in docebo
                                var userErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserUpdateError>(userResponse.Content);
                                string userErrorMessages = string.Join("<br/>", userErrorContent.message.message);
                                return Request.CreateErrorResponse(userResponse.StatusCode, "Docebo User Update Error(s): " + userErrorMessages);
                            }
                            // If docebo user successfully added
                            var userResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserCreationSuccess>(userResponse.Content);
                            // Set docebo user
                            employeeAccount.DoceboUserName = DoceboUserName;
                            employeeAccount.DoceboPassword = DoceboPassword;
                        }
                    }
                    else
                    {
                        // If there is error in authorization
                        var oAuthErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthError>(oAuthResponse.Content);
                        return Request.CreateErrorResponse(oAuthResponse.StatusCode, "Docebo Authorization Error: " + oAuthErrorContent.error_description);
                    }

                    // Check if password exist
                    if (!CommonUtil.IsNullEmptyWhitespace(data.password))
                    {
                        employeeAccount.Password = data.password;
                    }

                    // Save changes
                    await conn.SaveChangesAsync();

                    var updatedEmployeeAccount = EmployeeAuthFactory.GetEmployeeAuth(employeeAccount);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employee_account = updatedEmployeeAccount,
                        message = "New employee account successfully updated"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Delete Employee Account
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Account" })]
        [HttpDelete, ValidateModel, Route("{id}")]
        public async Task<HttpResponseMessage> RemoveEmployeeAccount(int id)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;

                    var employeeAccount = await conn.EmployeeAuths.FirstAsync(x => x.Id == id);

                    if (employeeAccount.DoceboUserID != null)
                    {
                        // Delete Docebo Account
                        var httpClient = new RestClient("https://clear-home.docebosaas.com");
                        // Get token to be authorized
                        var oAuthRequest = new RestRequest("oauth2/token", Method.POST);
                        string oauthParams = JsonConvert.SerializeObject(new
                        {
                            grant_type = "password",
                            client_id = "hubtulgy-client",
                            client_secret = "0f7c732c205b3377feed76b1a5e3b8357ff956eb",
                            username = "admin",
                            password = "Clearhome",
                            scope = "api"
                        });
                        oAuthRequest.AddParameter("application/json; charset=utf-8", oauthParams, ParameterType.RequestBody);
                        var oAuthResponse = httpClient.Execute(oAuthRequest);
                        if (oAuthResponse.StatusCode == HttpStatusCode.OK)
                        {
                            // If authorized
                            var oAuthResponseContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthSuccess>(oAuthResponse.Content);
                            // Delete docebo user account
                            var userRequest = new RestRequest("manage/v1/user/delete", Method.DELETE);
                            string userParams = JsonConvert.SerializeObject(new
                            {
                                user_ids = new int?[]
                                {
                                    employeeAccount.DoceboUserID
                                }
                            });
                            userRequest.AddHeader("Authorization", "Bearer " + oAuthResponseContent.access_token);
                            userRequest.AddParameter("application/json; charset=utf-8", userParams, ParameterType.RequestBody);
                            var userResponse = httpClient.Execute(userRequest);
                            if (userResponse.StatusCode != HttpStatusCode.OK)
                            {
                                // If there are errors in deleting user in docebo
                                var userErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboUserDeletionError>(userResponse.Content);
                                string userErrorMessages = string.Join("<br/>", userErrorContent.errorMessage);
                                return Request.CreateErrorResponse(userResponse.StatusCode, "Docebo User Deletion Error(s): " + userErrorMessages);
                            }
                        }
                        else
                        {
                            // If there is error in authorization
                            var oAuthErrorContent = JsonConvert.DeserializeObject<EmployeeAuthModels.DoceboOAuthError>(oAuthResponse.Content);
                            return Request.CreateErrorResponse(oAuthResponse.StatusCode, "Docebo Authorization Error: " + oAuthErrorContent.error_description);
                        }
                    }

                    conn.EmployeeAuths.Remove(employeeAccount);

                    // Save changes
                    await conn.SaveChangesAsync();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        id,
                        message = "Employee account successfully removed"
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}
