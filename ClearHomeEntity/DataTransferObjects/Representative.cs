﻿using System.ComponentModel.DataAnnotations;
using System;
namespace ClearHomeEntity.DataTransferObjects.Representative
{
    public class RepTSIScoresQueryDTO
    {
        public string regional_name { get; set; }
        public string manager_name { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class RepTSISalesQueryDTO
    {
        [Required]
        public int cms_id { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
    }

    public class RepNamesQueryDTO
    {
        public string search { get; set; }
    }
}
