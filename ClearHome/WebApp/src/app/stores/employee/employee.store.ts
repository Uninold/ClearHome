
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { map } from 'rxjs/operators';

import { EmployeeService } from '../../services/employee/employee.service';

@Injectable()
export class EmployeeStore {
  private readonly _employeestat: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private readonly _employees: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private readonly _total: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _total_filtered: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _page: BehaviorSubject<number> = new BehaviorSubject(1);
  private readonly _limit: BehaviorSubject<number> = new BehaviorSubject(50);
  private readonly _loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(private _employeeService: EmployeeService) { }

  get employeestat() {
    return this._employeestat.asObservable();
  }

  get employees() {
    return this._employees.asObservable();
  }

  get total() {
    return this._total.asObservable();
  }

  get total_filtered() {
    return this._total_filtered.asObservable();
  }

  get page() {
    return this._page.asObservable();
  }

  get limit() {
    return this._limit.asObservable();
  }

  get loading() {
    return this._loading.asObservable();
  }

  setPage(page: number) {
    this._page.next(page);
  }

  setLoading(loading: boolean) {
    this._loading.next(loading);
  }

  loadEmployees(search?: string, type?: string): Observable<any> {
    return this._employeeService.getEmployees({
      type: type,
      search: search,
      limit: this._limit.getValue(),
      page: this._page.getValue()
    }).pipe(
      map(data => {
        this._employees.next(data.employees);
        this._total.next(data.total);
        this._total_filtered.next(data.total_filtered);
        this._loading.next(false);
        return data;
      })
    );
  }

  loadEmployeeStat(id: number, fromDate: string, toDate: string): Observable<any> {
    return this._employeeService.getEmployeeStatTSIScores({
      id: id,
      fromDate: fromDate,
      toDate: toDate
    }).pipe(
      map(data => {
      this._employeestat.next(data);
        return data;
      })
    );
  }
}
