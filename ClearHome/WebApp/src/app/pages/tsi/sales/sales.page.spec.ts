import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TsiSalesPageComponent } from './sales.page';

describe('TsiSalesPageComponent', () => {
  let component: TsiSalesPageComponent;
  let fixture: ComponentFixture<TsiSalesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TsiSalesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsiSalesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
