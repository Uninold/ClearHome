import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PercentagePipeModule } from '../../pipes/percentage/percentage.module';

import { HomePageComponent } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    PercentagePipeModule
  ],
  declarations: [
    HomePageComponent,
  ]
})
export class HomePageModule { }
