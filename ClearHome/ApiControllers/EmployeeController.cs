﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Employee;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EmployeeModels = ClearHomeEntity.Models.Employee;
using ClearHomeEntity.Factories.Employee;
using System.Collections.Generic;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Employee API Controller
    /// </summary>
    [RoutePrefix("api/employees")]
    public class EmployeeController : ApiController
    {
        /// <summary>
        /// Get Employee List
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employees" })]
        [HttpGet, ValidateModel, Route("")]
        public async Task<HttpResponseMessage> GetEmployees([FromUri]EmployeeQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.Employees.AsNoTracking().Where(x => x.Inactive == "false").AsQueryable();

                    // Check if there is type
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.type))
                    {
                        query = query.Where(x => x.Type.Contains(data.type));
                    }

                    // Get employee lists
                    total = query.Count();

                    var query2 = query.Select(x => new EmployeeModels.EmployeeListModel
                    {
                        ID = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        PhoneNumber = x.Phone,
                        Type = x.Type
                    }).AsQueryable(); 

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query2 = query2.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || x.Email.Contains(data.search) || x.PhoneNumber.Contains(data.search) || x.Type.Contains(data.search));
                    }

                    query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ThenBy(x => x.Email);

                    var employees = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    total_filtered = employees.Count();

                    var employeeList = EmployeeFactory.GetEmployees(employees);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employees = employeeList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get All Employee List
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employees" })]
        [HttpGet, ValidateModel, Route("all")]
        public async Task<HttpResponseMessage> GetAllEmployees([FromUri]AllEmployeeQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.Employees.AsNoTracking().Where(x => x.Inactive == "0").AsQueryable();

                    // Check if there is type
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.type))
                    {
                        query = query.Where(x => x.Type.Contains(data.type));
                    }

                    // Check if there is employee ids
                    if (data?.employee_id != null && data?.employee_id != 0)
                    {
                        query = query.Where(x => x.Id == data.employee_id);
                    }

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query = query.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || String.Concat(x.FirstName," ",x.LastName).Contains(data.search) || x.Email.Contains(data.search) || x.Phone.Contains(data.search) || x.Type.Contains(data.search));
                    }

                    var query2 = query.Select(x => new EmployeeModels.EmployeeListModel
                    {
                        ID = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        PhoneNumber = x.Phone,
                        Type = x.Type
                    }).AsQueryable();

                    query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ThenBy(x => x.Email);

                    var employees = await query2.ToListAsync();

                    var employeeList = EmployeeFactory.GetEmployees(employees);
                    var total = employeeList.Count;

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employees = employeeList,
                        total
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get All Employee List
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employees" })]
        [HttpGet, ValidateModel, Route("type")]
        public async Task<HttpResponseMessage> GetFilteredEmployees([FromUri]AllEmployeeFilter data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vwReportingHierarchies.AsNoTracking().AsQueryable();
                    IQueryable<EmployeeModels.EmployeeListModel>query2;
                    List<EmployeeModels.EmployeeListModel> employees = new List<EmployeeModels.EmployeeListModel>();
                    // Check if there is type
                    query2 = query.GroupBy(x => x.RegId).Select(x => new EmployeeModels.EmployeeListModel
                    {
                        ID = x.FirstOrDefault().RegId,
                        FirstName = x.FirstOrDefault().RegionalFirst,
                        LastName = x.FirstOrDefault().RegionalLast
                    }).AsQueryable();

                    if (data?.region_filter != null)
                    {
                        query2 = query.Where(x => x.RegId == data.region_filter).GroupBy(x => x.MgrId).Select(x => new EmployeeModels.EmployeeListModel
                        {
                            ID = x.FirstOrDefault().EmpId,
                            FirstName = x.FirstOrDefault().RepFirst,
                            LastName = x.FirstOrDefault().RepLast
                        }).AsQueryable();
                    }
                    if (data?.office_filter != null)
                    {
                        if(data?.office_filter != 0)
                        {
                            query2 = query.Where(x => x.MgrId == data.office_filter).GroupBy(x => x.MgrId).Select(x => new EmployeeModels.EmployeeListModel
                            {
                                ID = x.FirstOrDefault().EmpId,
                                FirstName = x.FirstOrDefault().RepFirst,
                                LastName = x.FirstOrDefault().RepLast
                            }).AsQueryable();
                        }
                    }
                    query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    employees = await query2.ToListAsync();
                    //if (!CommonUtil.IsNullEmptyWhitespace(data?.type))
                    //{
                    //    switch (data.type)
                    //    {
                    //        case "Manager":

                    //            if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    //            {
                    //                //query2 = query2.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || String.Concat(x.FirstName, " ", x.LastName).Contains(data.search));
                    //                //query2 = query2.Where(x => String.Concat(x.FirstName, " ", x.LastName).Contains(data.search));
                    //                query2 = query.Where(x => String.Concat(x.RegionalFirst, " ", x.RegionalLast).Contains(data.search)).GroupBy(x => x.MgrId).Select(x => new EmployeeModels.EmployeeListModel
                    //                {
                    //                    ID = x.FirstOrDefault().EmpId,
                    //                    FirstName = x.FirstOrDefault().RepFirst,
                    //                    LastName = x.FirstOrDefault().RepLast
                    //                }).AsQueryable();
                    //            }
                    //            else
                    //            {
                    //                query2 = query.GroupBy(x => x.MgrId).Select(x => new EmployeeModels.EmployeeListModel
                    //                {
                    //                    ID = x.FirstOrDefault().EmpId,
                    //                    FirstName = x.FirstOrDefault().RepFirst,
                    //                    LastName = x.FirstOrDefault().RepLast
                    //                }).AsQueryable();
                    //            }
                    //            query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    //            employees = await query2.ToListAsync();
                    //            break;
                    //        case "Regional":
                    //            query2 = query.GroupBy(x => x.RegId).Select(x => new EmployeeModels.EmployeeListModel
                    //            {
                    //                ID = x.FirstOrDefault().RegId,
                    //                FirstName = x.FirstOrDefault().RegionalFirst,
                    //                LastName = x.FirstOrDefault().RegionalLast
                    //            }).AsQueryable();
                    //            if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    //            {
                    //                query2 = query2.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || String.Concat(x.FirstName, " ", x.LastName).Contains(data.search));
                    //            }
                    //            query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    //            employees = await query2.ToListAsync();
                    //            break;
                    //        case "Representative":
                    //             query2 = query.GroupBy(x => x.EmpId).Select(x => new EmployeeModels.EmployeeListModel
                    //             {
                    //                 ID = x.FirstOrDefault().EmpId,
                    //                 FirstName = x.FirstOrDefault().RepFirst,
                    //                 LastName = x.FirstOrDefault().RepLast
                    //             }).AsQueryable();
                    //            if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    //            {
                    //                query2 = query2.Where(x => x.FirstName.Contains(data.search) || x.LastName.Contains(data.search) || String.Concat(x.FirstName, " ", x.LastName).Contains(data.search));
                    //            }
                    //            query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    //            employees = await query2.ToListAsync();
                    //            break;
                    //    }

                    //}
                    //else
                    //{
                    //    query2 = query.Select(x => new EmployeeModels.EmployeeListModel
                    //    {
                    //        ID = x.EmpId,
                    //        FirstName = x.RepFirst,
                    //        LastName = x.RepLast
                    //    }).AsQueryable();
                    //    query2 = query2.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
                    //    employees = await query2.ToListAsync();
                    //}



                    var employeeList = EmployeeFactory.GetEmployees(employees);
                    var total = employeeList.Count;

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employees = employeeList,
                        total
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }



        /// <summary>
        /// Get All Employee List
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employees" })]
        [HttpGet, ValidateModel, Route("employee-with-tsi")]
        public async Task<HttpResponseMessage> GetAllEmployeesWithTSI([FromUri]EmployeeStatQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();
                    
                    // Check if there is employee ids
                    if (data?.id != null && data?.id != 0)
                    {
                        query = query.Where(x => x.RepId == data.id);
                    }
                    
                    var query2 = query.GroupBy(x => x.RepName).Select(x => new EmployeeModels.EmployeeListModelTSI
                    {
                        ID = x.FirstOrDefault().RepId,
                        RepName = x.FirstOrDefault().RepName,
                        TSI = x.Sum(s => s.TSI),
                    }).AsQueryable();
                    
                    var employees = await query2.FirstOrDefaultAsync();

                    if(employees==null)
                    {
                        employees = new EmployeeModels.EmployeeListModelTSI
                        {
                            ID = data.id,
                            RepName = data.rep_name,
                            TSI = (decimal)0.00
                        };
                    }
                    var emp = EmployeeFactory.GetEmployeeTSI(employees);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employee = emp
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
