import { AddNewCompetitionsModalComponent } from './add-new-competitions/add-new-competitions.modal';
import { ChangeYearModalComponent } from './change-year/change-year.modal';
import { OpenVideoModalComponent } from './open-video-modal/open-video.modal';
import { AddMemberModalComponent } from './add-member-modal/add-member-modal';
export const COMPETITIONS_MODALS = [
  AddNewCompetitionsModalComponent,
  ChangeYearModalComponent,
  OpenVideoModalComponent,
  AddMemberModalComponent
];
