import { CommonLayoutModule } from './common.layout.module';

describe('CommonLayoutModule', () => {
  let commonModule: CommonLayoutModule;

  beforeEach(() => {
    commonModule = new CommonLayoutModule();
  });

  it('should create an instance', () => {
    expect(commonModule).toBeTruthy();
  });
});
