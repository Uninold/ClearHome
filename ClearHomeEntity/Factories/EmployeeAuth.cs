﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeAuthModels = ClearHomeEntity.Models.EmployeeAuth;

namespace ClearHomeEntity.Factories.EmployeeAuth
{
    public class EmployeeAuthFactory
    {
        public static EmployeeAuthModels.EmployeeAccount GetEmployeeAuth(ClearHomeEntity.EmployeeAuth employee)
        {
            return new EmployeeAuthModels.EmployeeAccount
            {
                id = employee.Id,
                first_name = employee.FirstName,
                last_name = employee.LastName,
                email = employee.Email,
                phone_number = employee.Phone,
                username = employee.Username,
                employee_id = employee.EmployeeId,
                docebo_username = employee.DoceboUserName,
                docebo_user_id = employee.DoceboUserID
            };
        }

        public static EmployeeAuthModels.EmployeeUserCurrent GetEmployeeAuthCurrent(ClearHomeEntity.EmployeeAuth employee)
        {
            return new EmployeeAuthModels.EmployeeUserCurrent
            {
                id = employee.Id,
                first_name = employee.FirstName,
                last_name = employee.LastName,
                email = employee.Email,
                phone_number = employee.Phone,
                employee_id = employee.EmployeeId
            };
        }

        public static List<EmployeeAuthModels.EmployeeAccount> GetEmployeeAccounts(ICollection<ClearHomeEntity.EmployeeAuth> employeeAccounts)
        {
            var employeeAccountList = new List<EmployeeAuthModels.EmployeeAccount>();
            foreach (var employeeAccount in employeeAccounts)
            {
                employeeAccountList.Add(GetEmployeeAuth(employeeAccount));
            }
            return employeeAccountList;
        }

    }

}
