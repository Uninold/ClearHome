import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

// GUARDS
import { GuardsModule } from './guards/guards.module';

// INTERCEPTORS
import { InterceptorsModule } from './interceptors/interceptors.module';

// LAYOUTS
import { LayoutsModule } from './layouts/layouts.module';

// SERVICES
import { ServicesModule } from './services/services.module';

// Import BrowserAnimationsModule
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Import your library
import { AlertModule } from 'ngx-alerts';
// STORES
import { StoresModule } from './stores/stores.module';

// COMPONENTS
import { ModalsModule } from './components/modals/modals.module';

// APP
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';

// PAGES (Not Lazy-Loaded)
import { FrontPageModule } from './pages/front/front.page.module';
import { HomePageModule } from './pages/home/home.page.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppLoadService } from './services/app-load.service';
export function initializeAppFactory(provider: AppLoadService) {
  return () => provider.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    GuardsModule,
    InterceptorsModule,
    LayoutsModule,
    ServicesModule,
    StoresModule,
    ModalsModule,
    AppRoutingModule,
    FrontPageModule,
    HomePageModule,
    NgbModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
     // Specify your library as an import
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, position: 'right'})
  ],
  bootstrap: [AppComponent],
  providers: [
    AppLoadService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeAppFactory,
      deps: [AppLoadService],
      multi: true
    }
  ]
})
export class AppModule { }
