import * as storage from 'store';

import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
//import { CampaignService } from '../../../services/campaign';
//import { ResponseCallbackModel, ResponseModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeService } from '../../../../services/employee/employee.service';
import { EmbedVideoService } from 'ngx-embed-video';
declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-member-modal',
  templateUrl: './add-member-modal.html',
  styleUrls: ['./add-member-modal.css']
})
// tslint:disable-next-line:component-class-suffix
export class AddMemberModalComponent implements OnInit {
  @Input() office_filter: any;
  @Input() region_filter: any;
  rep: any;
  constructor(
    private modal: NgbModal,
    public activeModal: NgbActiveModal,
    private embedService: EmbedVideoService,
    private _employeeService: EmployeeService
  ) {
  }

  ngOnInit() {
    console.log('region', this.region_filter);
    console.log('office', this.office_filter);
    this._employeeService.getFilteredEmployees({ region_filter: this.region_filter.id, office_filter: this.office_filter.id }).subscribe(data => {
      this.rep = data;
      console.log('rep', data);
    });
    // this._campaign.getCampaignDetails(this.campaign_id, (responseGet: ResponseCallbackModel) => {
    //     this.campaign_details = responseGet.data;
    //     console.log(this.campaign_details);
    //     this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responseAddPhoto: ResponseCallbackModel) => {
    //         this.campaign_photos = responseAddPhoto.data;
    //         console.log(this.campaign_photos);
    //     });
    // });
  }

  closeModal() {
    this.activeModal.close();
  }

}
