import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { UserStore } from '../../../stores/user/user.store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
import { TournamentStore } from '../../../stores/tournament/tournament.store';
import { AddNewCompetitionsModalComponent } from '../../../components/modals/competitions/add-new-competitions/add-new-competitions.modal';
import { ChangeYearModalComponent } from '../../../components/modals/competitions/change-year/change-year.modal';

@Component({
  selector: 'app-competitions-lists',
  templateUrl: './lists.page.html',
  styleUrls: ['./lists.page.css']
})
export class CompetitionsListsComponent implements OnInit {
  tournaments$: Observable<any[]>;
  loading$: Observable<boolean>;
  currentUser$: Observable<any>;
  selectedYear: number;
  firstName: string;
  Image: string;
  constructor(
    private _modal: NgbModal,
    private _tournamentStore: TournamentStore,
    private _userStore: UserStore,
  ) {
    this.selectedYear = new Date().getFullYear();
    this.tournaments$ = this._tournamentStore.tournaments;
    this.loading$ = this._tournamentStore.loading;

    this._tournamentStore.loadTournaments().subscribe();
    
  }

  ngOnInit() {
  
  }
  addCompetitions(): void {
    const modalRef = this._modal.open(AddNewCompetitionsModalComponent, {
      size: 'lg', backdrop: 'static', keyboard: false
    });
  }

  changeYear(): void {
   const modalYear = this._modal.open(ChangeYearModalComponent);
  }

}
