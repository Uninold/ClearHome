import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

declare var $: any;

import { AuthStore } from '../../../stores/auth/auth.store';
import { ResponseModel } from '../../../models/common.model';
import { UserStore } from '../../../stores/user/user.store';

@Component({
  selector: '[app-common-header]',
  templateUrl: './header.component.html',
  styles: []
})
export class CommonHeaderComponent implements OnInit, AfterViewInit {

  currentUser$: Observable<any>;

  constructor(
    private _router: Router,
    private _authStore: AuthStore,
    private _userStore: UserStore
  ) { }

  ngOnInit() {
    this.currentUser$ = this._userStore.currentUser;
  }

  ngAfterViewInit() {
    $(document).ready(function () {
      $('.navbar-search-opener').click(function () {
        $('.header-pop-search').addClass('open');
      });
      $('.header-search-close').click(function (e) {
        e.preventDefault();
        $('.header-pop-search').removeClass('open');
      });
    });
    $('.right-sidebar-toggle').click(function (e) {
      e.preventDefault();
      $('body').toggleClass('right-sidebar-expand');
    });
  }

  logOut($event: any): void {
    $event.preventDefault();
    this._authStore.logOut((response: ResponseModel) => {
      this._router.navigate(['/login']);
    });
  }

  toggleSidebar($event: any): void {
    $event.preventDefault();
    const width = window.innerWidth > 0 ? window.innerWidth : screen.width;
    const body = $('body');
    if (width < 961) {
      $('.site-sidebar').toggle();
    } else if (body.hasClass('sidebar-expand')) {
      body.removeClass('sidebar-expand');
      body.addClass('sidebar-collapse');
      $('.side-user > a').removeClass('active');
      $('.side-user > a').siblings('.side-menu').hide();
      $('.side-menu .sub-menu').css('height', 'auto');
      $('.site-sidebar.scrollbar-enabled').perfectScrollbar('destroy').removeClass('ps');
      $('.side-menu').metisMenu('dispose');
    } else if (body.hasClass('sidebar-collapse')) {
      body.removeClass('sidebar-collapse');
      body.addClass('sidebar-expand');
      $('.site-sidebar.scrollbar-enabled').perfectScrollbar({
        suppressScrollX: true,
        wheelSpeed: 0.5
      });
      $('.side-menu').metisMenu({ preventDefault: true });
    }
    $event.stopImmediatePropagation();
  }

  toggleRightSidebar($event: any): void {
    $event.preventDefault();
    $(event.target).toggleClass('active');
    $('.side-panel').toggleClass('active');
    $event.stopImmediatePropagation();
  }

}
