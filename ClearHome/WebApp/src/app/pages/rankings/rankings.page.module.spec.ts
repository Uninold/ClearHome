import { RankingsPageModule } from './rankings.page.module';

describe('HomePageModule', () => {
  let rankingsModule: RankingsPageModule;

  beforeEach(() => {
    rankingsModule = new RankingsPageModule();
  });

  it('should create an instance', () => {
    expect(rankingsModule).toBeTruthy();
  });
});
