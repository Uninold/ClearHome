import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LoginPageRoutingModule } from './login.page.routing.module';

import { LoginPageComponent } from './login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoginPageRoutingModule
  ],
  declarations: [LoginPageComponent]
})
export class LoginPageModule { }
