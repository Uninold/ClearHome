﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Rankings;
using ClearHomeEntity.Factories.Representative;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using RankingsModels = ClearHomeEntity.Models.Rankings;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Representative API Controller
    /// </summary>
    [RoutePrefix("api/powerrankings")]
    public class PowerRankingsController : ApiController
    {
        /// <summary>
        /// Get Representative TSI Scores
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Power Rankings" })]
        [HttpGet, ValidateModel, Route("rankings")]
        public async Task<HttpResponseMessage> GetRankings([FromUri]RankingQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 100;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 10;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    data.fromDate.AddHours(0);
                    data.toDate.AddDays(24);
                    query = query.Where(x => x.TSIDate > data.fromDate && x.TSIDate < data.toDate);
                    

                    // Check if there is id
             
                    //query = query.Where(x => x.TSIDate.Contains(start_date));
                    //query = query.Where(x => x.RepName.StartsWith("b"));
                    // Get total reps tsi scores


                    total = query.GroupBy(x => x.RepName).Count();
                    var query2 = query.GroupBy(x => x.RepName).Select(x => new RankingsModels.RankingsListModel
                    {
                        CMSID = x.FirstOrDefault().RepId,
                        RepName = x.Key,
                        //ManagerName = x.FirstOrDefault().ManagerName,
                        RegionalName = x.FirstOrDefault().RegionalName,
                        DTVActivated = x.Where(s => s.DTVActivated == true).Count(),
                        TSI = x.Sum(s => s.TSI ?? 0),
                        LowRisk = x.Where(s => s.LowRisk == true).Count(),
                        AutoPay = x.Where(s => s.AutoPay == true).Count(),
                        SameDay = x.Where(s => s.SameDay == true).Count(),
                        ProcessingFee = x.Where(s => s.ProcessingFee == true).Count(),
                        InternetSold = x.Where(s => s.InternetSold == true).Count(),
                        CellPhoneSold = x.Where(s => s.CellPhoneSold == true).Count(),
                        SecuritySold = x.Where(s => s.SecuritySold == true).Count()
                    }).Take(10).AsQueryable();
                    //offset = (page - 1) * limit;
                    query2 = query2.OrderByDescending(x => x.TSI);
                    var rankings = await query2.ToListAsync();

                    //var rankings = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    total_filtered = rankings.Count();
                    var rankinglistitem = RankingsFactory.GetPowerRankings(rankings);
                    

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        powerrankinglist = rankinglistitem,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
