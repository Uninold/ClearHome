import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EmployeeAuthService } from '../../services/employee/employee-auth.service';

@Injectable()
export class EmployeeAuthStore {

  private readonly _employee_accounts: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private readonly _total: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _total_filtered: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _search: BehaviorSubject<string> = new BehaviorSubject(null);
  private readonly _page: BehaviorSubject<number> = new BehaviorSubject(1);
  private readonly _limit: BehaviorSubject<number> = new BehaviorSubject(50);
  private readonly _loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(private _employeeAuthService: EmployeeAuthService) { }

  get employee_accounts() {
    return this._employee_accounts.asObservable();
  }

  get total() {
    return this._total.asObservable();
  }

  get total_filtered() {
    return this._total_filtered.asObservable();
  }

  get search() {
    return this._search.asObservable();
  }

  get page() {
    return this._page.asObservable();
  }

  get limit() {
    return this._limit.asObservable();
  }

  get loading() {
    return this._loading.asObservable();
  }

  setSearch(search: string) {
    this._search.next(search);
  }

  setPage(page: number) {
    this._page.next(page);
  }

  setLoading(loading: boolean) {
    this._loading.next(loading);
  }

  loadEmployeeAccounts(): Observable<any> {
    return this._employeeAuthService.getEmployeeAccounts({
      search: this._search.getValue(),
      limit: this._limit.getValue(),
      page: this._page.getValue()
    }).pipe(
      map(data => {
        this._employee_accounts.next(data.employee_accounts);
        this._total.next(data.total);
        this._total_filtered.next(data.total_filtered);
        this._loading.next(false);
        return data;
      })
    );
  }

}
