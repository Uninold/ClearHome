﻿using System;

namespace ClearHomeEntity.DataTransferObjects.Employee
{
    public class EmployeeQueryDTO
    {
        public string type { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
    }

    public class AllEmployeeQueryDTO
    {
        public string type { get; set; }
        public string search { get; set; }
        public int? employee_id { get; set; }
    }

    public class AllEmployeeFilter
    {
        public int region_filter { get; set; }
        public int office_filter { get; set; }
        public string search { get; set; }
        public int? employee_id { get; set; }
    }

    public class EmployeeStatQueryDTO
    {
        public int id { get; set; }
        public string rep_name { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }

    }

}
