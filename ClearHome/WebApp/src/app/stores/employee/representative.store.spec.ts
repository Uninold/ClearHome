import { TestBed, inject } from '@angular/core/testing';

import { RepresentativeStore } from './representative.store';

describe('RepresentativeStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RepresentativeStore]
    });
  });

  it('should be created', inject([RepresentativeStore], (service: RepresentativeStore) => {
    expect(service).toBeTruthy();
  }));
});
