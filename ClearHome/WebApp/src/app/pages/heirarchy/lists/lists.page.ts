import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { UserStore } from '../../../stores/user/user.store';
declare var $: any;
import { EmployeeHierarchyStore } from '../../../stores/employee/employee-hierarchy.store';
@Component({
  selector: 'app-heirarchy-lists',
  templateUrl: './lists.page.html',
  styleUrls: ['./lists.page.css']
})
export class HeirarchyListsComponent implements OnInit {
  employeeHierarchy$: Observable<any>;
  currentUser$: Observable<any>;
  
  firstName: string;
  constructor(
    private _employeeHierarchyStore: EmployeeHierarchyStore,
    private _userStore: UserStore,
  ) { }

  ngOnInit() {
    this.currentUser$ = this._userStore.currentUser;
    this._employeeHierarchyStore.loadEmployeeHierarchy(this._userStore.currentUserEmployeeId).subscribe(res => {
    });
    this.employeeHierarchy$ = this._employeeHierarchyStore.employeeHierarchy;
    console.log('lo', this.employeeHierarchy$);
  }

}
