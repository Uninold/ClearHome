import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RankingsPageComponent } from './rankings.page';

const routes: Routes = [
  { path: 'powerrankings', component: RankingsPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RankingsPageRoutingModule { }
