﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClearHomeEntity.DataTransferObjects.Tournament
{
    public class AddTournament
    {
        [Required]
        public string name { get; set; }
        [Required]
        public string bg_image { get; set; }
        [Required]
        public string promotional_video_link { get; set; }
        [Required]
        public string details { get; set; }
        [Required]
        public string icon { get; set; }
        [Required]
        public int number_of_participants { get; set; }
        [Required]
        public int created_by { get; set; }
        [Required]
        public DateTime created_date_time { get; set; }
        [Required]
        public DateTime start_date { get; set; }
        [Required]
        public DateTime end_date { get; set; }
        [Required]
        public string type { get; set; }
        [Required]
        public string format { get; set; }
        [Required]
        public List<TournamentTimelinesList> timelines { get; set; }
        [Required]
        public List<TournamentPrizesList> prizes { get; set; }
        [Required]
        public List<TournamentSeedingList> seeding { get; set; }

        public List<TsiTiers> tsi_tiers { get; set; }

        //public List<TournamentConsolation> consolation { get; set; }
    }
    public class TournamentConsolation
    {
        [Required]
        public int tournament_id { get; set; }
        [Required]
        public string prize_title { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public int points { get; set; }
        [Required]
        public string badge { get; set; }
    }
    public class TournamentPrizesList
    {
        [Required]
        public int tournament_id { get; set; }
        [Required]
        public string prize_title { get; set; }

        [Required]
        public string description { get; set; }
        [Required]
        public int points { get; set; }

        [Required]
        public string badge { get; set; }
    }
    public class TournamentWinnersList
    {
        [Required]
        public int tournament_id { get; set; }
        [Required]
        public int tournament_prizes { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }

    public class TournamentTimelinesList
    {
        [Required]
        public int tournament_id { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public DateTime start_date { get; set; }
        [Required]
        public DateTime end_date { get; set; }
        [Required]
        public string description { get; set; }
    }

    public class TournamentSeedingList
    {
        [Required]
        public int tournament_id { get; set; }
        [Required]
        public int seed_number { get; set; }
        [Required]
        public int participant { get; set; }
    }

    public class BracketList
    {
        [Required]
        public int tournament_id { get; set; }
        
        public string round { get; set; }
        
        public string win_lose { get; set; }
        
        public DateTime date_time_result { get; set; }
        
        public DateTime update_date_time { get; set; }
        [Required]
        public int updated_by { get; set; }
        
        public int group_number { get; set; }
        
        public DateTime round_dates { get; set; }
    }

    public class TsiTiers
    {
        public decimal tsi_amount { get; set; }
    }

    public class TeamList
    {
        [Required]
        public int tournament_id { get; set; }

        public string team_name { get; set; }
    }
}
