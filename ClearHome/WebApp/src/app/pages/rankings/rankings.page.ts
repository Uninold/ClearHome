import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

import { map } from 'rxjs/operators';
import { UserStore } from '../../stores/user/user.store';
import { RankingStore } from '../../stores/rankings/rankings.store';
@Component({
  selector: '[app-rankings-page]',
  templateUrl: './rankings.page.html',
  styleUrls: ['./rankings.page.css']
})
export class RankingsPageComponent implements OnInit {
  rankings$: Observable<any[]>;
  currentUser$: Observable<any>;
  total_filtered$: Observable<number>;
  loading$: Observable<boolean>;
  dateEnd: { year: number, month: number, day: number };
  dateStart: { year: number, month: number, day: number };
  private _route$: Subscription;
  constructor(
    private _userStore: UserStore,
    private _route: ActivatedRoute,
    private _rankingStore: RankingStore
  ) {
    this.currentUser$ = this._userStore.currentUser;

    this.rankings$ = this._rankingStore.rankings;
    this.total_filtered$ = this._rankingStore.total_filtered;
    this.loading$ = this._rankingStore.loading;
    
    if (this._userStore.currentFromDateValue.year) {
      this._rankingStore.loadRankings(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
    } 
  }

  ngOnInit() {
  

  }

}
