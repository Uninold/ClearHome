import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';

import { EmployeeService } from '../../../../services/employee/employee.service';
import { EmployeeAuthService } from '../../../../services/employee/employee-auth.service';
import { EmployeeAuthStore } from '../../../../stores/employee/employee-auth.store';
import { EmployeeListModel } from '../../../../models/employee.model';

@Component({
  selector: 'app-edit-employee-account-modal',
  templateUrl: './edit-account.modal.html',
  styleUrls: ['./edit-account.modal.css']
})
export class EditAccountModalComponent implements OnInit {

  @Input('id') id: number;

  employees$: Observable<EmployeeListModel[]>;
  employeeLoading = false;
  employeeInput$ = new Subject<string>();

  updateAccountForm: FormGroup;

  employee: EmployeeListModel;
  loading: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private _fb: FormBuilder,
    private _employeeService: EmployeeService,
    private _employeeAuthService: EmployeeAuthService,
    private _employeeAuthStore: EmployeeAuthStore
  ) { }

  ngOnInit() {
    this.loading = false;
    this._initForm();
    this._getEmployeeAccount();
  }

  updateAccount() {
    const formData = this.updateAccountForm.getRawValue();
    if (this.updateAccountForm.valid) {
      this.loading = true;
      this._employeeAuthService.updateEmployeeAccount(formData).subscribe(
        data => {
          this.loading = false;
          this._employeeAuthStore.loadEmployeeAccounts().subscribe();
          this.activeModal.close();
        }
      );
    }
  }

  onChangeEmployee($event: any): void {
    if (typeof $event !== 'undefined') {
      this.updateAccountForm.patchValue({
        employee_id: $event.id,
        first_name: $event.first_name,
        last_name: $event.last_name,
        email: $event.email,
        phone_number: $event.phone_number
      });
    }
  }

  clearEmployee($event: any): void {
    this.updateAccountForm.patchValue({
      employee_id: null
    });
  }

  private _initForm(): void {
    this.updateAccountForm = this._fb.group({
      id: [this.id, Validators.required],
      employee_id: [null, Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      phone_number: ['', Validators.required],
      username: ['', Validators.required],
      password: '',
      confirm_password: ''
    });
  }

  private _getEmployeeAccount(): void {
    this._employeeAuthService.getEmployeeAccount(this.id).subscribe(
      data => {
        const employee_account = data.employee_account;

        this.updateAccountForm.patchValue({
          employee_id: employee_account.employee_id,
          first_name: employee_account.first_name,
          last_name: employee_account.last_name,
          email: employee_account.email,
          phone_number: employee_account.phone_number,
          username: employee_account.username
        });

        this._employeeService.getAllEmployees({ employee_id: employee_account.employee_id }).subscribe(
          data2 => {
            this.employee = data2[0];
            this._searchEmployees(data2);
          }
        );
      }
    );
  }

  private _searchEmployees(employees: EmployeeListModel[]) {
    this.employees$ = concat(
      of(employees), // default items
      this.employeeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.employeeLoading = true),
        switchMap(term => this._employeeService.getAllEmployees({ type: 'Employee', search: term }).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.employeeLoading = false)
        ))
      )
    );
  }

}
