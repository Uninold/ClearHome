﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Regional;
using ClearHomeEntity.Factories.Regional;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.WebPages;
using System.Threading.Tasks;
using System.Web.Http;
using RegionalModels = ClearHomeEntity.Models.Regional;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Regional API Controller
    /// </summary>
    [RoutePrefix("api/regionals")]
    public class RegionalController : ApiController
    {
        /// <summary>
        /// Get Regional TSI Scores
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Regionals" })]
        [HttpGet, ValidateModel, Route("tsi-scores")]
        public async Task<HttpResponseMessage> GetRegionalTSIScores([FromUri]RegionalTSIScoresQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;
                   
                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();
                    // Check if there is regional name

                    data.fromDate.AddHours(0);
                    data.toDate.AddHours(23);
                    query = query.Where(x => x.TSIDate >= data.fromDate && x.TSIDate <= data.toDate);
                    // Get all regionsl tsi scores
                    total = query.Count();
   
                    var query2 = query.GroupBy(x => x.RegionalName).Select(x => new RegionalModels.RegionalTSIScoreModel
                    {
                        RegionalName = x.Key,
                        TSI = x.Sum(s => s.TSI),
                        QSI = 0,
                        DTVActivated = x.Where(s => s.DTVActivated == true).Count(),
                        DTVSold = x.Where(s => s.DTVSold == true).Count(),
                        InternetSold = x.Where(s => s.InternetSold == true).Count(),
                        CellPhoneSold = x.Where(s => s.CellPhoneSold == true).Count(),
                        SecuritySold = x.Where(s => s.SecuritySold == true).Count(),
                        AutoPay = x.Where(s => s.AutoPay == true).Count(),
                        LowRisk = x.Where(s => s.LowRisk == true).Count(),
                        ProcessingFee = x.Where(s => s.ProcessingFee == true).Count(),
                        SameDay = x.Where(s => s.SameDay == true).Count()
                    }).AsQueryable();

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query2 = query2.Where(x => x.RegionalName.Contains(data.search));
                    }

                    query2 = query2.OrderByDescending(x => x.TSI).ThenBy(x => x.RegionalName);
                    
                    //var tsiScores = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    var tsiScores = await query2.ToListAsync();
                    total_filtered = tsiScores.Count();

                    var tsiScoreList = RegionalFactory.GetRegionals(tsiScores);

                    return Request.CreateResponse(HttpStatusCode.OK, new

                    {
                        tsi_scores = tsiScoreList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get Regional Names
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Regionals" })]
        [HttpGet, ValidateModel, Route("names")]
        public async Task<HttpResponseMessage> GetRegionalNames([FromUri]RegionalNamesQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query = query.Where(x => x.RegionalName.Contains(data.search));
                    }

                    var regionalNames = await query.GroupBy(x => x.RegionalName).Select(x => x.Key).OrderBy(x => x).ToArrayAsync();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        regional_names = regionalNames,
                        total = regionalNames.Length
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}
