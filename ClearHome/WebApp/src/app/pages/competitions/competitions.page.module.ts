import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CompetitionsPageRoutingModule } from './competitions.page.routing.module';
import { CompetitionsListsComponent } from './lists/lists.page';
import { CompetitionDetailsComponent } from './competition-details/competition-details.page';

@NgModule({
  imports: [
    CommonModule,
    CompetitionsPageRoutingModule,
    FormsModule
  ],
  declarations: [
    CompetitionsListsComponent,
    CompetitionDetailsComponent
  ]
})
export class CompetitionsPageModule { }
