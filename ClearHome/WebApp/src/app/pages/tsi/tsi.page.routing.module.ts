import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TsiScoresPageComponent } from './scores/scores.page';
import { TsiSalesPageComponent } from './sales/sales.page';

const routes: Routes = [
  { path: 'scores', redirectTo: 'scores/regionals' },
  { path: 'scores/:type', component: TsiScoresPageComponent },
  { path: 'sales/:cms_id', component: TsiSalesPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TsiPageRoutingModule { }
