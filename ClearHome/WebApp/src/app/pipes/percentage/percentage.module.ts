import { NgModule } from '@angular/core';

import { PercentagePipe } from './percentage.pipe';

@NgModule({
  exports: [
    PercentagePipe
  ],
  declarations: [
    PercentagePipe
  ]
})
export class PercentagePipeModule { }
