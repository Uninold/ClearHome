import { Component, OnInit, AfterViewInit } from '@angular/core';

declare var $: any;

@Component({
  selector: '[app-common-sidebar]',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class CommonSidebarComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  private _initializeSidebar(): void {
    $('.site-sidebar.scrollbar-enabled').perfectScrollbar({
      suppressScrollX: true,
      wheelSpeed: 0.2
    });
    $('.side-menu').metisMenu({ preventDefault: true });
    $('.side-menu').on('show.metisMenu, hide.metisMenu', () => {
      $('.site-sidebar.scrollbar-enabled').perfectScrollbar('update');
    });
    $('.side-menu').on('hide.metisMenu', () => {
      $('.site-sidebar.scrollbar-enabled').perfectScrollbar('update');
    });
    $('body').on('mouseenter mouseleave', '.site-sidebar', () => {
      $('.site-sidebar.scrollbar-enabled').perfectScrollbar('update');
    });
    $('body').on('click', '.side-menu > li > a', () => {
      setTimeout(function () {
        $('.site-sidebar.scrollbar-enabled').perfectScrollbar('update');
      }, 500);
    });
  }

}
