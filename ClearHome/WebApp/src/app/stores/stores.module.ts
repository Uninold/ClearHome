import { NgModule } from '@angular/core';

import { AuthStore } from './auth/auth.store';
import { EmployeeStore } from './employee/employee.store';
import { RegionalStore } from './employee/regional.store';
import { ManagerStore } from './employee/manager.store';
import { RepresentativeStore } from './employee/representative.store';
import { TsiSalesStore } from './sales/tsi-sales.store';
import { UserStore } from './user/user.store';
import { EmployeeAuthStore } from './employee/employee-auth.store';
import { EmployeeHierarchyStore } from './employee/employee-hierarchy.store';
import { RankingStore } from './rankings/rankings.store';
import { TournamentStore } from './tournament/tournament.store';

@NgModule({
  imports: [],
  providers: [
    AuthStore,
    EmployeeStore,
    EmployeeAuthStore,
    RegionalStore,
    ManagerStore,
    RepresentativeStore,
    TsiSalesStore,
    EmployeeHierarchyStore,
    UserStore,
    RankingStore,
    TournamentStore
  ]
})
export class StoresModule { }
