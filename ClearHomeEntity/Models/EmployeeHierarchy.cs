﻿using System;
using System.Collections.Generic;

namespace ClearHomeEntity.Models.EmployeeHierarchy
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Inactive { get; set; }
        public int? ManagerId { get; set; }
        public string ManagerFirst { get; set; }
        public string ManagerLast { get; set; }
        public int? RegionalId { get; set; }
        public string RegionalFirst { get; set; }
        public string RegionalLast { get; set; }
        public int? RegionalExecId { get; set; }
        public string RegionalExecFirst { get; set; }
        public string RegionalExecLast { get; set; }
        public int? DVPId { get; set; }
        public string DVPFirst { get; set; }
        public string DVPLast { get; set; }
        public int? VicePresidentId { get; set; }
        public string VicePresidentFirst { get; set; }
        public string VicePresidentLast { get; set; }
        public int? SeniorVicePresidentId { get; set; }
        public string SeniorVicePresidentFirst { get; set; }
        public string SeniorVicePresidentLast { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyFirst { get; set; }
        public string CompanyLast { get; set; }
        public int? ChannelId { get; set; }
        public string ChannelFirst { get; set; }
        public string ChannelLast { get; set; }
    }
    public class BasicEmpInfo {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Inactive { get; set; }
    }

    public class EmployeeHierarchy
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string Inactive { get; set; }
        public BasicEmpInfo manager { get; set; }
        public BasicEmpInfo regional { get; set; }
        public BasicEmpInfo regional_exe { get; set; }
        public BasicEmpInfo d_vice_president { get; set; }
        public EmployeeHierarchyVP vice_president { get; set; }
    }
    public class EmployeeHierarchyVP
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string Inactive { get; set; }
        public BasicEmpInfo s_vice_president { get; set; }
        public BasicEmpInfo company { get; set; }
        public BasicEmpInfo channel { get; set; }
    }

}
