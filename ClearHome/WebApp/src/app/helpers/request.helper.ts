import { HttpParams } from '@angular/common/http';

export class RequestHelper {

  public static Format(params: any = {}): HttpParams {
    let new_params = new HttpParams();
    for (const prop in params) {
      if (params.hasOwnProperty(prop)) {
        if (params[prop] != null && params[prop] !== '') {
          if (typeof params[prop] === 'object' || params[prop] instanceof Array) {
            new_params = new_params.append(prop, JSON.stringify(params[prop]));
          } else {
            new_params = new_params.append(prop, params[prop].toString());
          }
        }
      }
    }
    return new_params;
  }

  public static ToQueryString(params: any = {}): string {
    const searchParams = Object.keys(params).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }).join('&');
    return searchParams;
  }

}
