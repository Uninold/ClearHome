import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TsiPageRoutingModule } from './tsi.page.routing.module';
import { PercentagePipeModule } from '../../pipes/percentage/percentage.module';

import { TsiScoresPageComponent } from './scores/scores.page';
import { TsiSalesPageComponent } from './sales/sales.page';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TsiPageRoutingModule,
    PercentagePipeModule,
    NgbModule.forRoot()
  ],
  declarations: [
    TsiScoresPageComponent,
    TsiSalesPageComponent
  ]
})
export class TsiPageModule { }
