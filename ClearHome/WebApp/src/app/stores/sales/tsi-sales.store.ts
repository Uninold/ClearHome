import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { RegionalService } from '../../services/employee/regional.service';

@Injectable()
export class TsiSalesStore {

  private readonly _tsi_sales: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private readonly _total: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _total_filtered: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _page: BehaviorSubject<number> = new BehaviorSubject(1);
  private readonly _limit: BehaviorSubject<number> = new BehaviorSubject(20);
  private readonly _loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(private _regionalService: RegionalService) { }

  get tsi_sales() {
    return this._tsi_sales.asObservable();
  }

  get total() {
    return this._total.asObservable();
  }

  get total_filtered() {
    return this._total_filtered.asObservable();
  }

  get page() {
    return this._page.asObservable();
  }

  get limit() {
    return this._limit.asObservable();
  }

  get loading() {
    return this._loading.asObservable();
  }

  setLoading(loading: boolean) {
    this._loading.next(loading);
  }

  loadTsiSales(search?: string): Observable<any> {
    return this._regionalService.getRegionalTsiScores({
      limit: this._limit.getValue(),
      page: this._page.getValue(),
      search: search
    }).pipe(
      map(data => {
        this._tsi_sales.next(data.tsi_scores);
        this._total.next(data.total);
        this._total_filtered.next(data.total_filtered);
        this._loading.next(false);
        return data;
      })
    );
  }

}
