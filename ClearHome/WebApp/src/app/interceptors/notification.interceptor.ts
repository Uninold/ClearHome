import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { NotificationService } from '../services/notification/notification.service';
import { NotoficationHelper } from '../helpers/notification.helper';

@Injectable()
export class NotificationInterceptor implements HttpInterceptor {

  constructor(private _notification: NotificationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const isUrlNotifiable = NotoficationHelper.isUrlNotifiable(req.url);
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && req.method !== 'GET' && isUrlNotifiable) {
          if (typeof event.body.message !== 'undefined') {
            this._notification.success(event.body.message);
          }
        }
        return event;
      }),
      catchError((err: any, caught) => {
        if (err instanceof HttpErrorResponse && req.method !== 'GET' && isUrlNotifiable) {
          switch (err.status) {
            case 403:
              this._notification.error('Oops! The page you requested is forbidden.');
              break;
            case 500:
              if (typeof err.error.exceptionMessage !== 'undefined' && err.error.exceptionMessage !== '') {
                this._notification.error(err.error.exceptionMessage);
              } else if (typeof err.error.message !== 'undefined' && err.error.message !== '') {
                this._notification.error(err.error.message);
              } else if (typeof err.message !== 'undefined' && err.message !== '') {
                this._notification.error(err.message);
              } else {
                this._notification.error('Hey! Internal server error.');
              }
              break;
            case 400:
              if (typeof err.error.message !== 'undefined' && err.error.message !== '') {
                this._notification.error(err.error.message);
              } else if (typeof err.message !== 'undefined' && err.message !== '') {
                this._notification.error(err.message);
              } else {
                this._notification.error('Oops! Bad Request.');
              }
              break;
            case 401:
              if (typeof err.error.message !== 'undefined' && err.error.message !== '') {
                this._notification.error(err.error.message);
              } else if (typeof err.message !== 'undefined' && err.message !== '') {
                this._notification.error(err.message);
              } else {
                this._notification.error('Sorry! You are not allowed to access.');
              }
              break;
            default:
              if (typeof err.error.message !== 'undefined' && err.error.message !== '') {
                this._notification.error(err.error.message);
              } else if (typeof err.message !== 'undefined' && err.message !== '') {
                this._notification.error(err.message);
              } else {
                this._notification.error('Something went wrong! Check your server.');
              }
              break;
          }
        }
        return throwError(err);
      })
    );
  }

}
