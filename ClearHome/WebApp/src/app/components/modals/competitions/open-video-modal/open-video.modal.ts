import * as storage from 'store';

import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
//import { CampaignService } from '../../../services/campaign';
//import { ResponseCallbackModel, ResponseModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { VideoModel } from '../../../models/campaign.model';
//import { InterestMessageModal } from '../../../modals/youtuber';
//import { INTEREST_MESSAGE_OPTIONS } from '../../../configs/modal-options.config';
//import { YoutuberService } from '../../../services/youtuber/youtuber.service';
//import { YoutubeDataAPI } from '../../../services/youtube_data_api/youtube_data_api.service';
import { EmbedVideoService } from 'ngx-embed-video';
declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'open-video.modal',
  templateUrl: './open-video.modal.html',
  styleUrls: ['./open-video.modal.css']
})
// tslint:disable-next-line:component-class-suffix
export class OpenVideoModalComponent implements OnInit {
  @Input() youtuber_url: any;
  videoDetails: any;
  channelId: string;
  urlvideo: string;
  google: any;
  iframe_html: any;
  campaign_details: any;
  campaign_photos: any;
  nav: any = 'campaign';
  videoTitle: string;
  videoDescription: string;
  viewCount: number;
  commentCount: number;
  likeCount: number;
  dislikeCount: number;
  noVideo: boolean;
  constructor(
    private modal: NgbModal,
    public activeModal: NgbActiveModal,
    private embedService: EmbedVideoService
  ) {
    this.noVideo = true;
    this.urlvideo = 'qfZm_lwJaD8';
  }

  ngOnInit() {
    this.iframe_html = this.embedService.embed_youtube(this.urlvideo, { attr: { width: 900, height: 600 } });
    this.getVideo(this.youtuber_url);
    console.log('x', this.youtuber_url);
    // this._campaign.getCampaignDetails(this.campaign_id, (responseGet: ResponseCallbackModel) => {
    //     this.campaign_details = responseGet.data;
    //     console.log(this.campaign_details);
    //     this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responseAddPhoto: ResponseCallbackModel) => {
    //         this.campaign_photos = responseAddPhoto.data;
    //         console.log(this.campaign_photos);
    //     });
    // });
  }

  closeModal() {
    this.activeModal.close();
  }

  getVideo(youtuber_url) {
    this.iframe_html = this.embedService.embed_youtube(this.urlvideo, { attr: { width: 900, height: 600 } });
    console.log('iframe', this.iframe_html);
  }

}
