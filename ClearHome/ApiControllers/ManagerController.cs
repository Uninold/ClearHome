﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Manager;
using ClearHomeEntity.Factories.Manager;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ManagerModels = ClearHomeEntity.Models.Manager;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Manager API Controller
    /// </summary>
    [RoutePrefix("api/managers")]
    public class ManagerController : ApiController
    {
        /// <summary>
        /// Get Manager TSI Scores
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Managers" })]
        [HttpGet, ValidateModel, Route("tsi-scores")]
        public async Task<HttpResponseMessage> GetRegionalTSIScores([FromUri]ManagerTSIScoresQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    int page = 1;
                    int limit = 100;
                    int offset = 0;
                    int total = 0;
                    int total_filtered = 0;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();
                    data.fromDate.AddHours(0);
                    data.toDate.AddHours(24);
                    query = query.Where(x => x.TSIDate >= data.fromDate && x.TSIDate <= data.toDate);
                    // Check if there is regional name
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.regional_name))
                    {
                        query = query.Where(x => x.RegionalName.Contains(data.regional_name));
                    }

                    // Get total mananger tsi scores
                    total = query.GroupBy(x => x.ManagerName).Count();

                    var query2 = query.GroupBy(x => x.ManagerName).Select(x => new ManagerModels.ManagerTSIScoreModel
                    {
                        ManagerName = x.Key,
                        RegionalName = x.FirstOrDefault().RegionalName,
                        TSI = x.Sum(s => s.TSI),
                        QSI = 0,
                        DTVActivated = x.Where(s => s.DTVActivated == true).Count(),
                        DTVSold = x.Where(s => s.DTVSold == true).Count(),
                        InternetSold = x.Where(s => s.InternetSold == true).Count(),
                        CellPhoneSold = x.Where(s => s.CellPhoneSold == true).Count(),
                        SecuritySold = x.Where(s => s.SecuritySold == true).Count(),
                        AutoPay = x.Where(s => s.AutoPay == true).Count(),
                        LowRisk = x.Where(s => s.LowRisk == true).Count(),
                        ProcessingFee = x.Where(s => s.ProcessingFee == true).Count(),
                        SameDay = x.Where(s => s.SameDay == true).Count()
                    }).AsQueryable();

                    // Check if there is page
                    if (data?.page != null && data.page > 0)
                    {
                        page = data.page;
                    }

                    // Check if there is limit
                    if (data?.limit != null && data.limit > 0)
                    {
                        limit = data.limit;
                    }

                    offset = (page - 1) * limit;

                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query2 = query2.Where(x => x.ManagerName.Contains(data.search));
                    }

                    query2 = query2.OrderByDescending(x => x.TSI).ThenBy(x => x.ManagerName);
                    
                    //var tsiScores = await query2.Skip(() => offset).Take(() => limit).ToListAsync();
                    var tsiScores = await query2.ToListAsync();
                    total_filtered = tsiScores.Count();

                    var tsiScoreList = ManagerFactory.GetManagers(tsiScores);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        tsi_scores = tsiScoreList,
                        total,
                        total_filtered
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Get Manager Names
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Managers" })]
        [HttpGet, ValidateModel, Route("names")]
        public async Task<HttpResponseMessage> GetManagerNames([FromUri]ManagerNamesQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 200;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vw2018TSI.AsNoTracking().AsQueryable();
                    // Check if there is search
                    if (!CommonUtil.IsNullEmptyWhitespace(data?.search))
                    {
                        query = query.Where(x => x.RegionalName.Contains(data.search));
                    }

                    var managerNames = await query.GroupBy(x => x.RegionalName).Select(x => x.Key).OrderBy(x => x).ToArrayAsync();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        manager_names = managerNames,
                        total = managerNames.Length
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}
