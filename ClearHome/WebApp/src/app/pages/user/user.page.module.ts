import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserPageRoutingModule } from './user.page.routing.module';

import { UserProfilePageComponent } from './profile/profile.page';

@NgModule({
  imports: [
    CommonModule,
    UserPageRoutingModule
  ],
  declarations: [UserProfilePageComponent]
})
export class UserPageModule { }
