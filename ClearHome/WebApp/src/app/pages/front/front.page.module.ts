import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontPageComponent } from './front.page';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FrontPageComponent]
})
export class FrontPageModule { }
