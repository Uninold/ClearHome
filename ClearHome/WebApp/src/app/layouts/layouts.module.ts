import { NgModule } from '@angular/core';

import { LandingLayoutModule } from './landing/landing.layout.module';
import { CommonLayoutModule } from './common/common.layout.module';

@NgModule({
  imports: [
    LandingLayoutModule,
    CommonLayoutModule
  ],
})
export class LayoutsModule { }
