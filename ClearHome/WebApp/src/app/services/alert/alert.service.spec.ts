import { TestBed, inject } from '@angular/core/testing';

import { AlertModalService } from './alert.service';

describe('NotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertModalService]
    });
  });

  it('should be created', inject([AlertModalService], (service: AlertModalService) => {
    expect(service).toBeTruthy();
  }));
});
