﻿using Swashbuckle.Swagger.Annotations;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ClearHome.Utilities;
using System;
using System.Net;
using ClearHomeEntity;
using System.Linq;
using System.Data.Entity;
using ClearHomeEntity.Factories.EmployeeAuth;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Account API Controller
    /// </summary>
    [RoutePrefix("api/accounts")]
    public class AccountController : ApiController
    {
        /// <summary>
        /// Get Current User
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Accounts" })]
        [HttpGet, Authorize, Route("current")]
        public async Task<HttpResponseMessage> GetCurrentUser()
        {
            try
            {
                var userData = await AuthUtil.GetUserDataFromAuth();

                using (var conn = new ClearHomeContainer())
                {
                    conn.Configuration.ProxyCreationEnabled = false;

                    var user = await conn.EmployeeAuths.AsNoTracking().FirstOrDefaultAsync(x => x.Id == userData.UserID);
                    if (user == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "User not found");
                    }

                    var currentUser = EmployeeAuthFactory.GetEmployeeAuthCurrent(user);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        user = currentUser
                    });
                }

                
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}
