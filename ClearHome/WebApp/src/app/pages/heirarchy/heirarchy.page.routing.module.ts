import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeirarchyListsComponent } from './lists/lists.page';

const routes: Routes = [
  { path: 'lists', component: HeirarchyListsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeirarchyPageRoutingModule { }
