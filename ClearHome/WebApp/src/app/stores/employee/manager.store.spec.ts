import { TestBed, inject } from '@angular/core/testing';

import { ManagerStore } from './manager.store';

describe('ManagerStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagerStore]
    });
  });

  it('should be created', inject([ManagerStore], (service: ManagerStore) => {
    expect(service).toBeTruthy();
  }));
});
