import { NgModule } from '@angular/core';

import { StartPagePipe } from './start.pipe';
import { EndPagePipe } from './end.pipe';

@NgModule({
  exports: [
    StartPagePipe,
    EndPagePipe
  ],
  declarations: [
    StartPagePipe,
    EndPagePipe
  ]
})
export class PagingPipeModule { }
