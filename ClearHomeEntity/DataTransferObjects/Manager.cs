﻿using System;
namespace ClearHomeEntity.DataTransferObjects.Manager
{
    public class ManagerTSIScoresQueryDTO
    {
        public string regional_name { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class ManagerNamesQueryDTO
    {
        public string search { get; set; }
    }

}
