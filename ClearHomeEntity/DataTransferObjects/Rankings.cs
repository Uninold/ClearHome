﻿using System;

namespace ClearHomeEntity.DataTransferObjects.Rankings
{
    public class RankingQueryDTO
    {
        public string type { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    //public class AllEmployeeQueryDTO
    //{
    //    public string type { get; set; }
    //    public string search { get; set; }
    //    public int? employee_id { get; set; }
    //}
    
    //public class EmployeeStatQueryDTO
    //{
    //    public int id { get; set; }
    //}

}
