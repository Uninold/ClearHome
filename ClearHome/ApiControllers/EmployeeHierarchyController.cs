﻿using ClearHome.Filters;
using ClearHome.Utilities;
using ClearHomeEntity;
using ClearHomeEntity.DataTransferObjects.Employee;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EmployeeModels = ClearHomeEntity.Models.EmployeeHierarchy;
using ClearHomeEntity.Factories.Employee;

namespace ClearHome.ApiControllers
{
    /// <summary>
    /// Employee API Controller
    /// </summary>
    [RoutePrefix("api/employeehierarchy")]
    public class EmployeeHierarchyController : ApiController
    {
        /// <summary>
        /// Get Employee List
        /// </summary>
        [SwaggerOperation(Tags = new[] { "Employee Hierarchy" })]
        [HttpGet, ValidateModel, Route("")]
        public HttpResponseMessage GetEmployeeHierarchy([FromUri]EmployeeStatQueryDTO data)
        {
            try
            {
                using (var conn = new ClearHomeContainer())
                {
                    conn.Database.CommandTimeout = 1000;
                    conn.Configuration.ProxyCreationEnabled = false;

                    var query = conn.vwHierarchies.AsNoTracking().AsQueryable();
                    var queryBasicInfo = conn.Employees.AsNoTracking().AsQueryable();

                    // Check if there is type

                    query = query.Where(x => x.RepId == data.id);



                    var query2 = query.Select(x => new EmployeeModels.Employee
                    {
                        Id = x.RepId,
                        FirstName = x.RepFirst,
                        LastName = x.RepLast,
                        Inactive = x.Inactive,
                        ManagerId = x.ManagerId,
                        ManagerFirst = x.MangerFirst,
                        ManagerLast = x.ManagerLast,
                        RegionalId = x.RegionalId,
                        RegionalFirst = x.RegionalFirst,
                        RegionalLast = x.RegionalLast,
                        RegionalExecId = x.RegionalExecId,
                        RegionalExecFirst = x.RegionalExecFirst,
                        RegionalExecLast = x.RegionalExecLast,
                        DVPId = x.DVPId,
                        DVPFirst = x.DVPFirst,
                        DVPLast = x.DVPLast,
                        VicePresidentId = x.VicePresidentId,
                        VicePresidentFirst = x.VicePresidentFirst,
                        VicePresidentLast = x.VicePresidentLast,
                        SeniorVicePresidentId = x.SeniorVicePresidentId,
                        SeniorVicePresidentFirst = x.SeniorVicePresidentFirst,
                        SeniorVicePresidentLast = x.SeniorVicePresidentLast,
                        CompanyId = x.CompanyId,
                        CompanyFirst = x.CampanyFirst,
                        CompanyLast = x.CompanyLast,
                        ChannelId = x.ChannelId,
                        ChannelFirst = x.ChannelFirst,
                        ChannelLast = x.ChannelLast
                    }).AsQueryable();
                    var employeeList = query2.FirstOrDefault();
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        employeeListName = employeeList
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
