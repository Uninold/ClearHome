﻿using System;
using System.Collections.Generic;
using RankingsModels = ClearHomeEntity.Models.Rankings;

namespace ClearHomeEntity.Factories.Representative
{
    public class RankingsFactory
    {
       
        public static RankingsModels.RankingsListModel GetPowerRanking(RankingsModels.RankingsListModel rep)
        {
            var repItem = new RankingsModels.RankingsListModel
            {
                CMSID = rep.CMSID,
                RepName = rep.RepName,
                RegionalName = rep.RegionalName,
                ManagerName = rep.ManagerName,
                TSI = Math.Round((decimal)rep.TSI, 2),
                AdditionalServices = Math.Round((decimal) (rep.CellPhoneSold + rep.InternetSold + rep.SecuritySold) , 2), 
                DTVActivated = rep.DTVActivated,
                AutoPay = Math.Round((decimal)0, 2),
                LowRisk = Math.Round((decimal)0, 2),
                ProcessingFee = Math.Round((decimal)0, 2),
                SameDay = Math.Round((decimal)0, 2)
                //total_sold = rep.DTVSold + rep.InternetSold + rep.InternetSold + rep.CellPhoneSold + rep.SecuritySold
            };
            if (rep.AutoPay > 0 && rep.DTVActivated > 0)
            {
                repItem.AutoPay = Math.Round((decimal)(rep.AutoPay / rep.DTVActivated), 2);
            }
            else
            {
                repItem.AutoPay = 0;
            }
            if (rep.LowRisk > 0 && rep.DTVActivated > 0)
            {
                repItem.LowRisk = Math.Round((decimal)(rep.LowRisk / rep.DTVActivated), 2);
            }
            else
            {
                repItem.LowRisk = 0;
            }
            if (rep.ProcessingFee > 0 && rep.DTVActivated > 0)
            {
                repItem.ProcessingFee = Math.Round((decimal)(rep.ProcessingFee / rep.DTVActivated), 2);
            }
            else
            {
                repItem.ProcessingFee = 0;
            }
            if (rep.SameDay > 0 && rep.DTVActivated > 0)
            {
                repItem.SameDay = Math.Round((decimal)(rep.SameDay / rep.DTVActivated), 2);
            }
            else
            {
                repItem.SameDay = 0;
            }
            return repItem;
        }

        public static List<RankingsModels.RankingsListModel> GetPowerRankings(ICollection<RankingsModels.RankingsListModel> reps)
        {
            var rankingList = new List<RankingsModels.RankingsListModel>();
            foreach (var rep in reps)
            {
                rankingList.Add(GetPowerRanking(rep));
            }
            return rankingList;
        }

    }
}
