﻿using System.Collections.Generic;
using EmployeeModels = ClearHomeEntity.Models.EmployeeHierarchy;

namespace ClearHomeEntity.Factories.Employee
{
    public class EmployeeHierarchyFactory
    {
        public static EmployeeModels.BasicEmpInfo GetEmployee(EmployeeModels.BasicEmpInfo employee)
        {
            return new EmployeeModels.BasicEmpInfo
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Inactive = employee.Inactive
            };
        }

        public static List<EmployeeModels.BasicEmpInfo> GetEmployees(ICollection<EmployeeModels.BasicEmpInfo> employees)
        {
            var employeeList = new List<EmployeeModels.BasicEmpInfo>();
            foreach (var employee in employees)
            {
                employeeList.Add(GetEmployee(employee));
            }
            return employeeList;
        }
        
        public static EmployeeModels.EmployeeHierarchy GetEmployeeHierarchy(EmployeeModels.Employee employee, List<EmployeeModels.BasicEmpInfo> employeeNames = null)
        {
            var employeeList = new EmployeeModels.EmployeeHierarchy();
            //employeeList.id = employee.Id;
            //employeeList.first_name = employee.FirstName;
            //employeeList.last_name = employee.LastName;
            //employeeList.Inactive = employee.Inactive;
            //if(employee.ManagerId > 0 || employee.ManagerId != null)
            //{
            //    foreach(var emp in employeeNames)
            //    {
            //        if(emp.Id == employee.ManagerId)
            //        {
            //            employeeList.manager = emp;
            //            break;
            //        }
            //    }
            //}
            //else
            //{
            //    employeeList.manager = new EmployeeModels.BasicEmpInfo();
            //}
            //if (employee.RegionalId > 0 || employee.RegionalId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.RegionalId)
            //        {
            //            employeeList.regional = emp;
            //            break;
            //        }
            //    }
            //    if (employeeList.regional == null)
            //    {
            //        employeeList.regional = new EmployeeModels.BasicEmpInfo();
            //    }
            //}
            //else
            //{
            //    employeeList.regional = new EmployeeModels.BasicEmpInfo();
            //}
            //if (employee.RegionalExecId > 0 || employee.RegionalExecId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.RegionalExecId)
            //        {
            //            employeeList.regional_exe = emp;
            //            break;
            //        }
            //    }
            //    if (employeeList.regional_exe == null)
            //    {
            //        employeeList.regional_exe = new EmployeeModels.BasicEmpInfo();
            //    }
            //}
            //else
            //{
            //    employeeList.regional_exe = new EmployeeModels.BasicEmpInfo();
            //}
            //if (employee.DVicePresidentId > 0 || employee.DVicePresidentId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.DVicePresidentId)
            //        {
            //            employeeList.d_vice_president = emp;
            //            break;
            //        }
            //    }
            //    if (employeeList.d_vice_president == null)
            //    {
            //        employeeList.d_vice_president = new EmployeeModels.BasicEmpInfo();
            //    }
            //}
            //else
            //{
            //    employeeList.d_vice_president = new EmployeeModels.BasicEmpInfo();
            //}
            //if (employee.VicePresidentId > 0 || employee.VicePresidentId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.VicePresidentId)
            //        {
            //            var employeeListVP = new EmployeeModels.EmployeeHierarchyVP();
            //            employeeListVP.id = emp.Id;
            //            employeeListVP.first_name = emp.FirstName;
            //            employeeListVP.last_name = emp.LastName;
            //            employeeListVP.Inactive = emp.Inactive;
            //            employeeListVP.s_vice_president = new EmployeeModels.BasicEmpInfo();
            //            employeeListVP.company = new EmployeeModels.BasicEmpInfo();
            //            employeeListVP.channel = new EmployeeModels.BasicEmpInfo();
            //            employeeList.vice_president = employeeListVP;
            //        }
            //    }
            //    if (employeeList.vice_president == null)
            //    {
            //        employeeList.vice_president = new EmployeeModels.EmployeeHierarchyVP();
            //        employeeList.vice_president.s_vice_president = new EmployeeModels.BasicEmpInfo();
            //        employeeList.vice_president.company = new EmployeeModels.BasicEmpInfo();
            //        employeeList.vice_president.channel = new EmployeeModels.BasicEmpInfo();
            //    }
            //}
            //else
            //{
            //    employeeList.vice_president = new EmployeeModels.EmployeeHierarchyVP();
            //}
            //if (employee.SVicePresidentId > 0 || employee.SVicePresidentId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.SVicePresidentId)
            //        {
            //            employeeList.vice_president.s_vice_president = emp;
            //        }
            //    }
            //}
            //else
            //{
            //    employeeList.vice_president.s_vice_president = new EmployeeModels.BasicEmpInfo();
            //}

            //if (employee.CompanyId > 0 || employee.CompanyId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.CompanyId)
            //        {
            //            employeeList.vice_president.company = emp;
            //        }
            //    }
            //}
            //else
            //{
            //    employeeList.vice_president.company = new EmployeeModels.BasicEmpInfo();
            //}

            //if (employee.ChannelId > 0 || employee.ChannelId != null)
            //{
            //    foreach (var emp in employeeNames)
            //    {
            //        if (emp.Id == employee.ChannelId)
            //        {
            //            employeeList.vice_president.channel = emp;
            //        }
            //    }
            //}
            //else
            //{
            //    employeeList.vice_president.channel = new EmployeeModels.BasicEmpInfo();
            //}


            return employeeList;

        }
    }
}
