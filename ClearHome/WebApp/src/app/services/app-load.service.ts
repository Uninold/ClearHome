import { Injectable } from '@angular/core';

import { AuthStore } from '../stores/auth/auth.store';
import { UserStore } from '../stores/user/user.store';
import { ResponseModel } from '../models/common.model';

@Injectable({
  providedIn: 'root'
})
export class AppLoadService {

  constructor(
    private _authStore: AuthStore,
    private _userStore: UserStore
  ) { }

  load() {
    return new Promise((resolve, reject) => {
      if (this._authStore.isUserAuthorized()) {
        this._userStore.loadCurrentUser().subscribe(
          data => resolve(true),
          error => {
            this._authStore.logOut((response: ResponseModel) => {
              resolve(true);
            });
          }
        );
      } else {
        resolve(true);
      }
    });
  }
}
