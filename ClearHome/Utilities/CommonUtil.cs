﻿using System;

namespace ClearHome.Utilities
{
    /// <summary>
    /// Common Utilities
    /// </summary>
    public class CommonUtil
    {
        /// <summary>
        /// If string is null, whitespace or empty
        /// </summary>
        public static bool IsNullEmptyWhitespace(string text)
        {
            return String.IsNullOrEmpty(text) || String.IsNullOrWhiteSpace(text);
        }

        internal static bool IsNullEmptyWhitespace(DateTime? startDate)
        {
            throw new NotImplementedException();
        }

        internal static bool IsNullEmptyWhitespace(int? id)
        {
            throw new NotImplementedException();
        }
    }
}