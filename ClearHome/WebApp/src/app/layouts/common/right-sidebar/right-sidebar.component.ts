import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { UserStore } from '../../../stores/user/user.store';
import { EmployeeStore } from '../../../stores/employee/employee.store';
import { Observable, Subscription } from 'rxjs';
import { RegionalStore } from '../../../stores/employee/regional.store';
import { ManagerStore } from '../../../stores/employee/manager.store';
import { RepresentativeStore } from '../../../stores/employee/representative.store';
import { RankingStore } from '../../../stores/rankings/rankings.store';

import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { ActivatedRoute } from '@angular/router';
import { last } from '@angular/router/src/utils/collection';

@Component({
  selector: '[app-common-right-sidebar]',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.css']
})
export class CommonRightSidebarComponent implements OnInit {
  selectedDate: any;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  from: NgbDateStruct;
  to: NgbDateStruct;
  private _route$: Subscription;

  @ViewChild('myDiv') el: ElementRef;
  constructor(
    private calendar: NgbCalendar,
    private _employeeStore: EmployeeStore,
    private _userStore: UserStore,
    private _regionalStore: RegionalStore,
    private _managerStore: ManagerStore,
    private _repStore: RepresentativeStore,
    private _route: ActivatedRoute,
    private _rankingStore: RankingStore) {
  }
  
  ngOnInit() {
    var element, name, arr;
    element = document.getElementsByName("myDiv");
    name = "active";
    element[0].classList.add(name);
    var lastWeek = this.calendar.getToday;

    this.fromDate = this.calendar.getToday();
    this.toDate = this.calendar.getToday();
    
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
  }

  dateChange() {
    var element, name, arr;
    element = document.getElementsByName("myDiv");
    name = "active";
    for (var i = 0; i < element.length; i++) {
      for (var i = 0; i < element.length; i++) {
        element[i].classList.remove(name);
      }
    }
    //this.from = this.fromDate;
    //this.to = this.toDate;

    //if (this.from > this.to) {
    //  this.toDate = this.from;
    //}
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {

      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);

      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {

      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selected(button) {
    var element, name, arr;
    element = document.getElementsByName("myDiv");
    name = "active";

    for (var i = 0; i < element.length; i++) {
      for (var i = 0; i < element.length; i++) {
        element[i].classList.remove(name);
      }
    }
    button.target.classList.add('active');
  }
  selectToday(button) {
    this.selected(button);
    this.fromDate = this.calendar.getToday();
    this.toDate = this.calendar.getToday();
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {

      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);

      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {

      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectYesterday(button) {
    this.selected(button);
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", 1);
    this.toDate = this.calendar.getToday();
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);

      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {

      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectTwoDays(button) {
    this.selected(button);
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", 2);
    this.toDate = this.calendar.getToday();
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
   //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
  //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
 
      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);
    
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {
 
      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectWeekToDate(button) {
    this.selected(button);
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", this.calendar.getWeekday(this.calendar.getToday()) - 1);
    this.toDate = this.calendar.getNext(this.calendar.getToday(), "d", 7 - this.calendar.getWeekday(this.calendar.getToday()));
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {

      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);
 
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {
 
      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectTodayLastWeek(button) {
    this.selected(button);
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", 6 + this.calendar.getWeekday(this.calendar.getToday()));

    this.toDate = this.calendar.getToday();
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
 
      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);
  
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {
   
      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectLastWeek(button) {
    this.selected(button);
   
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", 6 + this.calendar.getWeekday(this.calendar.getToday()));

    this.toDate = this.calendar.getPrev(this.calendar.getToday(), "d",  this.calendar.getWeekday(this.calendar.getToday()));
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);

    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
     
      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);
     
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {
   
      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
  selectLastMonth(button) {
    this.selected(button);
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), "d", 6 + this.calendar.getWeekday(this.calendar.getToday()));
    this.toDate = this.calendar.getPrev(this.calendar.getToday(), "d", this.calendar.getWeekday(this.calendar.getToday()));
    this._userStore.setcurrentFromDate(this.fromDate);
    this._userStore.setcurrentToDate(this.toDate);
    //profile stats
    this._employeeStore.loadEmployeeStat(
      this._userStore.currentUserEmployeeId,
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day
    ).subscribe();
    //tsi
    if (!this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
    
      this._regionalStore.setLoading(true);
      this._regionalStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day, ).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && !this._route.snapshot.queryParamMap.get('manager-name')) {
      this._managerStore.setLoading(true);
     
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      this._managerStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName).subscribe();
    } else if (this._route.snapshot.queryParamMap.get('regional-name') && this._route.snapshot.queryParamMap.get('manager-name')) {

      this._repStore.setLoading(true);
      const regionalName = this._route.snapshot.queryParamMap.get('regional-name');
      const managerName = this._route.snapshot.queryParamMap.get('manager-name');
      this._repStore.loadTsiScores(
        this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
        this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day,
        regionalName, managerName).subscribe();
    }
    //power rankings
    this._rankingStore.setLoading(true);
    this._rankingStore.loadRankings(
      this._userStore.currentFromDateValue.year + '-' + this._userStore.currentFromDateValue.month + '-' + this._userStore.currentFromDateValue.day,
      this._userStore.currentToDateValue.year + '-' + this._userStore.currentToDateValue.month + '-' + this._userStore.currentToDateValue.day).subscribe();
  }
}
