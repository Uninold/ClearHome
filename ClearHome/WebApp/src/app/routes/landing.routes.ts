import { Route } from '@angular/router';

import { NoAuthGuard } from '../guards/no-auth.guard';

import { LandingLayoutComponent } from '../layouts/landing/landing.layout';

import { FrontPageComponent } from '../pages/front/front.page';

// LANDING LAYOUT ROUTES
export const LANDING_ROUTES: Route = {
  path: '',
  component: LandingLayoutComponent,
  children: [
    { path: '', component: FrontPageComponent, pathMatch: 'full' }
  ]
};
