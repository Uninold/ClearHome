﻿using ClearHome.Utilities;
using ClearHomeEntity;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ClearHome.Providers
{
    /// <summary>
    /// Authorization Server Provider
    /// </summary>
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// Validate Client Authentication
        /// </summary>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        /// <summary>
        /// Grant Resource Owner Credentials
        /// </summary>
        ///
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                if (!CommonUtil.IsNullEmptyWhitespace(context.UserName) && !CommonUtil.IsNullEmptyWhitespace(context.Password))
                {
                    ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim("identity", context.UserName));

                    using (var conn = new ClearHomeContainer())
                    {
                        var user = await conn.EmployeeAuths.AsNoTracking().Select(x => new
                        {
                            x.Id,
                            x.Username,
                            x.Email,
                            x.Phone,
                            x.Password
                        }).FirstOrDefaultAsync(x => (x.Username == context.UserName || x.Email == context.UserName || x.Phone == context.UserName) && x.Password == context.Password);
                        if (user == null)
                        {
                            context.SetError("invalid_grant", "The user name or password is incorrect.");
                            return;
                        }

                        identity.AddClaim(new Claim("user_id", user.Id.ToString()));
                    }

                    context.Validated(identity);
                }
            }
            catch (Exception e)
            {
                context.SetError("invalid_grant", e.Message);
            }
        }

    }
}