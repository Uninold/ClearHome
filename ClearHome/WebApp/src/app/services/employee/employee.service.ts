import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';
import { EmployeeListModel } from '../../models/employee.model';

@Injectable()
export class EmployeeService {

  constructor(private _http: HttpClient) { }

  getEmployees(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      type: typeof args.type === 'undefined' ? null : args.type,
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 50 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}employees`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getAllEmployees(args: any = {}): Observable<EmployeeListModel[]> {
    const params = RequestHelper.Format({
      type: typeof args.type === 'undefined' ? null : args.type,
      search: typeof args.search === 'undefined' ? null : args.search,
      employee_id: typeof args.employee_id === 'undefined' ? null : args.employee_id
    });
    return this._http.get(`${ServerConfig.API}employees/all`, { params }).pipe(
      map((response: any) => {
        return <EmployeeListModel[]>response.employees;
      })
    );
  }

  getFilteredEmployees(args: any = {}): Observable<EmployeeListModel[]> {
    const params = RequestHelper.Format({
      region_filter: typeof args.region_filter === 'undefined' ? null : args.region_filter,
      office_filter: typeof args.office_filter === 'undefined' ? null : args.office_filter,
      search: typeof args.search === 'undefined' ? null : args.search,
      employee_id: typeof args.employee_id === 'undefined' ? null : args.employee_id
    });
    return this._http.get(`${ServerConfig.API}employees/type`, { params }).pipe(
      map((response: any) => {
        return <EmployeeListModel[]>response.employees;
      })
    );
  }

  getEmployeeTSI(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      id: typeof args.id === 'undefined' ? null : args.id,
      rep_name: typeof args.id === 'undefined' ? null : args.rep_name,
      fromDate: args.fromDate,
      toDate: args.toDate
    });
    return this._http.get(`${ServerConfig.API}employees/employee-with-tsi`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getEmployeeStatTSIScores(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      id: typeof args.id === 'undefined' ? null : args.id,
      fromDate:  args.fromDate,
      toDate:  args.toDate
    });
    return this._http.post(`${ServerConfig.API}employeestat/tsi-scores`, params).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
