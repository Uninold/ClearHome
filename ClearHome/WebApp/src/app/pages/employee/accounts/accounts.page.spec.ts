import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAccountsPageComponent } from './accounts.page';

describe('EmployeeAccountsPageComponent', () => {
  let component: EmployeeAccountsPageComponent;
  let fixture: ComponentFixture<EmployeeAccountsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeeAccountsPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAccountsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
