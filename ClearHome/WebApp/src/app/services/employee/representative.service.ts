import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';

@Injectable()
export class RepresentativeService {

  constructor(private _http: HttpClient) { }

  getRepNames(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}representatives/names`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getRepTSIScores(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      regional_name: typeof args.regional_name === 'undefined' ? null : args.regional_name,
      manager_name: typeof args.manager_name === 'undefined' ? null : args.manager_name,
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 100 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search,
      fromDate: args.fromDate,
      toDate: args.toDate
    });
    return this._http.get(`${ServerConfig.API}representatives/tsi-scores`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getRepTSISales(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      rep_id: typeof args.rep_id === 'undefined' ? null : args.rep_id,
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 100 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}representatives/tsi-sales`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
