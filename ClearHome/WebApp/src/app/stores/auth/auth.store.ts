import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const storage = require('store');

import { AuthService } from '../../services/auth/auth.service';
import { LoginModel, AuthModel } from '../../models/auth.model';

@Injectable()
export class AuthStore {

  private _token: string;
  private _isAuthorized: boolean;

  constructor(private _authService: AuthService) {
    this.isUserAuthorized();
  }

  get token() {
    return this._token;
  }

  get isAuthorized() {
    return this._isAuthorized;
  }

  isUserAuthorized(): boolean {
    const authData = <AuthModel>storage.get('authData');
    this._isAuthorized = !!authData;
    if (authData) {
      this._token = authData.access_token;
    } else {
      this._token = '';
    }
    return this._isAuthorized;
  }

  logIn(auth_data: LoginModel): Observable<AuthModel> {
    return this._authService.authenticate(auth_data).pipe(
      map(data => {
        this._isAuthorized = true;
        this._token = data.access_token;
        storage.set('authData', data);
        return data;
      })
    );
  }

  logOut(callback: any = () => { }): void {
    setTimeout(() => {
      this._token = '';
      this._isAuthorized = false;
      storage.clearAll();
      callback({
        error: 0,
        data: [],
        message: 'Sucessfully logged out.'
      });
    });
  }

}
