import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { ServerConfig } from '../../config/server.config';

@Injectable()
export class UserService {

  constructor(private _http: HttpClient) { }

  getCurrentUser(): Observable<any> {
    return this._http.get(`${ServerConfig.API}accounts/current`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
