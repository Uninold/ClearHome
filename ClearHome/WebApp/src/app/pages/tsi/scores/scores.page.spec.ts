import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TsiScoresPageComponent } from './scores.page';

describe('TsiScoresPageComponent', () => {
  let component: TsiScoresPageComponent;
  let fixture: ComponentFixture<TsiScoresPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TsiScoresPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsiScoresPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
