import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { UserStore } from '../../../stores/user/user.store';

@Component({
  selector: '[app-user-profile-page]',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class UserProfilePageComponent implements OnInit {

  currentUser$: Observable<any>;

  constructor(
    private _userStore: UserStore
  ) { }

  ngOnInit() {
    this.currentUser$ = this._userStore.currentUser;
  }

}
