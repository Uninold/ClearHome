﻿using System.Web.Mvc;

namespace ClearHome.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }
    }
}