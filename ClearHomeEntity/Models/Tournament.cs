﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClearHomeEntity.Models.Tournament
{
    public class TournamentModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string bgImage { get; set; }
        public string bgImageType { get; set; }
        public string promotionalVideoLink { get; set; }
        public string details { get; set; }
        public string icon { get; set; }
        public string iconType { get; set; }
        public int? numberOfParticipants { get; set; }
        public int createdBy { get; set; }
        public DateTime? createdDateTime { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string type { get; set; }
        public string format { get; set; }
        public bool? status { get; set; }
        public List<Tournament_Timelines> timelines { get; set; }
        public List<TournamentPrizesModel> prizes { get; set; }
        public List<TournamentSeedingWithNameModel> seeding { get; set; }
        //public List<TournamentPrizesModel> consolation { get; set; }
        public List<BracketModel> brackets { get; set; }
 
    }

    public class TournamentPrizesModel
    {
        public int id { get; set; }
        public int tournamentId { get; set; }
        public string prize_title { get; set; }
        public string description { get; set; }
        public int? points { get; set; }
        public string badge { get; set; }
        public string badgeType { get; set; }
    }

    public class TournamentWinnersModel
    {
        public int id { get; set; }
        public int tournamentId { get; set; }
        public int tournamentPrizes { get; set; }
        public DateTime date { get; set; }
    }

    
    public class TournamentTimelinesModel
    {
        public int id { get; set; }
        public int tournamentId { get; set; }
        public string title { get; set; }
        public DateTime date { get; set; }
        public string description { get; set; }
    }

    public class TournamentSeedingWithNameModel
    {
        public int id { get; set; }
        public int tournamentId { get; set; }
        public string repName { get; set; }
        public decimal? tsi { get; set; }
        public int? seedNumber { get; set; }
        public int participant { get; set; }
    }

    public class TournamentSeedingModel
    {
        public int id { get; set; }
        public int tournamentId { get; set; }
        public int? seedNumber { get; set; }
        public int participant { get; set; }
    }

    public class BracketModel
    {
        public int id { get; set; }
        public int tournamentSeedingId { get; set; }
        public string round { get; set; }
        public string winLose { get; set; }
        public DateTime? datetimeResult { get; set; }
        public DateTime? updateDatetime { get; set; }
        public int updatedBy { get; set; }
        public int? groupNumber { get; set; }
        public DateTime? roundDates { get; set; }
    }
    
    public class TournamentList
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public string type { get; set; }
    }


}
