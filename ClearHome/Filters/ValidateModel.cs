﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ClearHome.Filters
{
    /// <summary>
    /// Validate Model from HTTP Request filters
    /// </summary>
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Execute when there are errors in the model
        /// </summary>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                string errorMessages = string.Join("<br/>", actionContext.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorMessages);
            }
        }
    }
}