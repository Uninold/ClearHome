import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthStore } from '../stores/auth/auth.store';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private _authStore: AuthStore) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.get('noauth') === 'true') {
      return next.handle(req.clone({ headers: req.headers.delete('noauth') }));
    }
    if (this._authStore.isUserAuthorized()) {
      const headers = {
        'Authorization': `Bearer ${this._authStore.token}`
      };
      return next.handle(req.clone({ headers: new HttpHeaders(headers) }));
    }
    return next.handle(req);
  }

}
