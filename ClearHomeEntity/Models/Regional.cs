﻿using System;

namespace ClearHomeEntity.Models.Regional
{
    public class RegionalTSIScoreModel
    {
        public string RegionalName { get; set; }
        public decimal? TSI { get; set; }
        public decimal? QSI { get; set; }
        public decimal? DTVActivated { get; set; }
        public decimal? DTVSold { get; set; }
        public decimal? InternetSold { get; set; }
        public decimal? CellPhoneSold { get; set; }
        public decimal? SecuritySold { get; set; }
        public decimal? AutoPay { get; set; }
        public decimal? LowRisk { get; set; }
        public decimal? ProcessingFee { get; set; }
        public decimal? SameDay { get; set; }
        public string TSIDate { get; set; }
    }

    public class RegionalTSIScore
    {
        public string name { get; set; }
        public decimal? tsi { get; set; }
        public decimal? qsi { get; set; }
        public decimal? dtv_activated { get; set; }
        public decimal? dtv_sold { get; set; }
        public decimal? internet_sold { get; set; }
        public decimal? cellphone_sold { get; set; }
        public decimal? security_sold { get; set; }
        public decimal? auto_pay { get; set; }
        public decimal? low_risk { get; set; }
        public decimal? processing_fee { get; set; }
        public decimal? same_day { get; set; }
        public decimal? total_sold { get; set; }
    }

}
