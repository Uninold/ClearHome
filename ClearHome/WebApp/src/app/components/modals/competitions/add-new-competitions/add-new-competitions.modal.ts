import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl, Form } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';

import { TournamentService } from '../../../../services/tournament/tournament.service';
import { TournamentStore } from '../../../../stores/tournament/tournament.store';
import { UserStore } from '../../../../stores/user/user.store';

import { AddMemberModalComponent } from '../../../../components/modals/competitions/add-member-modal/add-member-modal';
import { EmployeeListModel } from '../../../../models/employee.model';
import { EmployeeService } from '../../../../services/employee/employee.service';
import { EmployeeAuthService } from '../../../../services/employee/employee-auth.service';
import { EmployeeAuthStore } from '../../../../stores/employee/employee-auth.store';
import { NgSelectComponent } from '@ng-select/ng-select';

@Component({
  selector: 'app-new-competitions-modal',
  templateUrl: './add-new-competitions.modal.html',
  styleUrls: ['./add-new-competitions.modal.css']
})
export class AddNewCompetitionsModalComponent implements OnInit {
  @ViewChild('selectSeed') ref: NgSelectComponent;
  employees$: Observable<EmployeeListModel[]>;
  region: any;
  office: any;

  employeeLoading = false;
  employeeInput$ = new Subject<string>();

  addAccountForm: FormGroup;
  bracket_format = ['Single Elimination', 'Double Elimination', 'Round Robin'];
  tournament_types = ['Bracket Style Tournament', 'Project Leagues', 'Quota Competition', 'First to a goal', 'Top Sales Competition'];
  employee_types = ['Regional', 'Manager', 'Representative'];
  team_composition = ['Teams', 'Individual'];
  tournament_photo: any;
  addTournamentForm: FormGroup;
  timeline: FormArray;
  tournament_based: any;
  prize: FormArray;
  seeding: FormArray;
  consolation: FormArray;
  tsi_tiers: FormArray;
  selectedItem: any;
  timelineControl: FormGroup;
  consolationControl: FormGroup;
  prizeControl: FormGroup;
  currentUser$: Observable<any>;

  addTournamentTimelineForm: FormGroup;

  public options: Object = {
    placeholderText: 'Details',
    insertImage: false,
    videoUpload: false,
    fileUpload: false,
    charCounterCount: false,
    fileUploadURL: false,
    imageUploadURL: false,
    videoUploadURL: false,
    toolbarButtons: [
      'fullscreen',
      'bold',
      'italic',
      'underline',
      'strikeThrough',
      'subscript',
      'superscript',
      '|',
      'fontFamily',
      'fontSize',
      'color',
      'inlineClass',
      'inlineStyle',
      'paragraphStyle',
      'lineHeight', '|',
      'paragraphFormat',
      'align', 'formatOL',
      'formatUL', 'outdent',
      'indent',
      'quote',
      '-',
      /*'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|',*/
      'emoticons',
      'fontAwesome',
      'specialCharacters',
      'insertHR', 'selectAll',
      'clearFormatting',
      /*'|', 'print', 'getPDF', */
      'spellChecker', /*'help', 'html',*/
      '|', 'undo', 'redo'],
    quickInsertButtons: false
  };

  constructor(
    private _fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private _userStore: UserStore,
    private _modal: NgbModal,
    public _tournamentService: TournamentService,
    public _tournamentStore: TournamentStore,
    private _employeeService: EmployeeService,
    private _employeeAuthService: EmployeeAuthService,
    private _employeeAuthStore: EmployeeAuthStore
  ) {
    this.currentUser$ = this._userStore.currentUser;
    this._employeeService.getFilteredEmployees().subscribe(data => {
      this.region = data;
      console.log(this.region);
      });

  }
 
  ngOnInit() {

    this._initForm();
    this._searchEmployees();
    this.addTournamentForm.controls['created_by'].setValue(this._userStore.currentUserEmployeeId);
    
    console.log(this.tournament_based);

  }
  changeRegFilter() {
    this._employeeService.getFilteredEmployees({ region_filter: this.addTournamentForm.controls['region_filter'].value.id }).subscribe(data => {
      this.addTournamentForm.controls['office_filter'].setValue(null);
      this.office = data;
      console.log(this.office);
    });
  }
  private _searchEmployees() {
    this.employees$ = concat(
      of([]), // default items
      this.employeeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.employeeLoading = true),
        switchMap(term => this._employeeService.getFilteredEmployees({ type: 'Manager', search: term }).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.employeeLoading = false)
        ))
      )
    );
  }
 

  validateStartDate() {
    this.timeline = <FormArray>this.addTournamentForm.controls['timelines'];
    //this.prize.controls[position].controls['badge'].setValue(myReader.result);
    if (this.addTournamentForm.controls['start_date'].value > this.addTournamentForm.controls['end_date'].value) {
      this.addTournamentForm.controls['end_date'].setValue(this.addTournamentForm.controls['start_date'].value);
    }
    for (var i = 0; i < this.timeline.length; i++) {
      this.timelineControl = <FormGroup>this.timeline.controls[i];
      if (this.addTournamentForm.controls['end_date'].value < this.timelineControl.controls['start_date'].value || this.timelineControl.controls['start_date'].value < this.addTournamentForm.controls['start_date'].value) {
        this.timelineControl.controls['start_date'].setValue(this.addTournamentForm.controls['end_date'].value);
      }
      if (this.addTournamentForm.controls['end_date'].value < this.timelineControl.controls['end_date'].value || this.timelineControl.controls['end_date'].value < this.addTournamentForm.controls['start_date'].value) {
        this.timelineControl.controls['end_date'].setValue(this.addTournamentForm.controls['end_date'].value);
      }
    }
  }
  validateEndDate() {
    this.timeline = <FormArray>this.addTournamentForm.controls['timelines'];
    //this.prize.controls[position].controls['badge'].setValue(myReader.result);
    if (this.addTournamentForm.controls['end_date'].value < this.addTournamentForm.controls['start_date'].value) {
      this.addTournamentForm.controls['start_date'].setValue(this.addTournamentForm.controls['end_date'].value);
    }
    for (var i = 0; i < this.timeline.length; i++) {
      this.timelineControl = <FormGroup>this.timeline.controls[i];
      if (this.addTournamentForm.controls['end_date'].value < this.timelineControl.controls['start_date'].value || this.timelineControl.controls['start_date'].value < this.addTournamentForm.controls['start_date'].value) {
        this.timelineControl.controls['start_date'].setValue(this.addTournamentForm.controls['end_date'].value);
      }
      if (this.addTournamentForm.controls['end_date'].value < this.timelineControl.controls['end_date'].value || this.timelineControl.controls['end_date'].value < this.addTournamentForm.controls['start_date'].value) {
        this.timelineControl.controls['end_date'].setValue(this.addTournamentForm.controls['end_date'].value);

      }
    }
   
  }

  validateTimelineDate(position: any) {
    this.timeline = <FormArray>this.addTournamentForm.controls['timelines'];
    // this.prize.controls[position].controls['badge'].setValue(myReader.result);
    this.timelineControl = <FormGroup>this.timeline.controls[position];
    // tslint:disable-next-line:max-line-length
    if (this.addTournamentForm.controls['end_date'].value < this.timelineControl.controls['date'].value || this.timelineControl.controls['date'].value < this.addTournamentForm.controls['start_date'].value) {
      this.timelineControl.controls['date'].setValue(this.addTournamentForm.controls['end_date'].value);
    }
  }

  changeListenerBgImage($event): void {
    const fileType = 'png';
    this.readThisBgImage($event.target);
  }
  readThisBgImage(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      // this.tournament_photo = myReader.result;
      this.addTournamentForm.controls['bg_image'].setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }
  changeListenerPrizes($event, position): void {
    const fileType = 'png';
    this.readThisPrizes($event.target, position);
  }

  readThisPrizes(inputValue: any, position: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {

      this.prize = <FormArray>this.addTournamentForm.controls['prizes'];
      // this.prize.controls[position].controls['badge'].setValue(myReader.result);
      this.prizeControl = <FormGroup>this.prize.controls[position];
      this.prizeControl.controls['badge'].setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }

  changeListenerConsolation($event, position): void {
    const fileType = 'png';
    this.readThisConsolation($event.target, position);
  }

  readThisConsolation(inputValue: any, position: any): void {
    console.log('consolation');
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.consolation = <FormArray>this.addTournamentForm.controls['consolation'];
      // this.prize.controls[position].controls['badge'].setValue(myReader.result);
      this.consolationControl = <FormGroup>this.consolation.controls[position];
      this.consolationControl.controls['badge'].setValue(myReader.result);
    };
    myReader.readAsDataURL(file);
  }
  changeListener($event): void {
    const fileType = 'png';
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.tournament_photo = myReader.result;

      this.addTournamentForm.controls['icon'].setValue(this.tournament_photo);
    };
    myReader.readAsDataURL(file);
  }

  addTournament() {
    const formData = this.addTournamentForm.getRawValue();
    console.log(formData);
    //if (this.addTournamentForm.valid) {
    //  this._tournamentService.addTournament(formData).subscribe(
    //    data => {
    //      this._tournamentService.addTournamentTimeline(formData.timelines);
    //      this._tournamentService.addTournamentPrizes(formData.prizes);
    //      this._tournamentStore.loadTournaments().subscribe();
    //      this.activeModal.close();
    //    }
    //  );
    //}
  }
  addMemberModel(): void {
   const modal = this._modal.open(AddMemberModalComponent, {
     keyboard: false
    });
    modal.componentInstance.region_filter = this.addTournamentForm.controls['region_filter'].value;
    modal.componentInstance.office_filter = this.addTournamentForm.controls['office_filter'].value;

  }
  addTimeline() {
    this.timeline = <FormArray>this.addTournamentForm.controls['timelines'];
    this.timeline.push(this.createTimeline());
  }
  removeTimeline(index) {
    this.timeline = <FormArray>this.addTournamentForm.controls['timelines'];
    // this.timeline.push(this.createTimeline());
    this.timeline.removeAt(index);
  }
  addPrize() {
    this.prize = <FormArray>this.addTournamentForm.controls['prizes'];
    this.prize.push(this.createPrize());
  }
  removePrize(index) {
    this.prize = <FormArray>this.addTournamentForm.controls['prizes'];
    // this.timeline.push(this.createTimeline());
    this.prize.removeAt(index);
  }
  addConsolation() {
    this.consolation = <FormArray>this.addTournamentForm.controls['consolation'];
    this.consolation.push(this.createPrize());
  }
  removeConsolation(index) {
    this.consolation = <FormArray>this.addTournamentForm.controls['consolation'];
    // this.timeline.push(this.createTimeline());
    this.consolation.removeAt(index);
  }
  addTSITier() {
    this.tsi_tiers = <FormArray>this.addTournamentForm.controls['tsitiers'];
    this.tsi_tiers.push(this.createTsiTier());
  }
  removeTSITier(index) {
    this.tsi_tiers = <FormArray>this.addTournamentForm.controls['tsitiers'];
    // this.timeline.push(this.createTimeline());
    this.tsi_tiers.removeAt(index);
  }
  addSeed() {
    this.seeding = <FormArray>this.addTournamentForm.controls['seeding'];
    this.seeding.push(this.createSeed());
  }
  removeSeed(index) {
    this.seeding = <FormArray>this.addTournamentForm.controls['seeding'];
    // this.timeline.push(this.createTimeline());
    this.seeding.removeAt(index);
  }
  addMember($event: any) {
    this.employeeLoading = false;
    if (this.addTournamentForm.controls['seeding'].value.some(e => e.participant === $event.id) == false) {
      this.seeding = <FormArray>this.addTournamentForm.controls['seeding'];
      this._employeeService.getEmployeeTSI({
        id: $event.id,
        rep_name: $event.first_name + ' ' + $event.last_name
      }).subscribe(data => {
        this.seeding.push(this.createMember(data.employee));
        this.addTournamentForm.controls['number_of_participants'].setValue(this.addTournamentForm.controls['seeding'].value.length);
        console.log(this.addTournamentForm);
      });
    }

    this.ref.handleClearClick();
  }
  removeMember(index) {
    this.seeding = <FormArray>this.addTournamentForm.controls['seeding'];
    // this.timeline.push(this.createTimeline());
    this.seeding.removeAt(index);
  }
  private _initForm(): void {
    this.addTournamentForm = this._fb.group({
      name: ['', Validators.required],
      bg_image: ['', Validators.required],
      promotional_video_link: ['', Validators.required],
      details: ['', Validators.required],
      icon: ['', Validators.required],
      number_of_participants: [0, Validators.required],
      created_by: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      type: [null, Validators.required],
      region_filter: [null],
      office_filter: [null],
      format: [null],
      timelines: this._fb.array([], Validators.minLength(1)),
      prizes: this._fb.array([], Validators.minLength(1)),
      seeding: this._fb.array([], Validators.minLength(2)),
      tsitiers: this._fb.array([])
      //consolation: this._fb.array([])
    });

  }
  createTimeline(): FormGroup {
    return this._fb.group({
      title: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  createPrize(): FormGroup {
    return this._fb.group({
      prize_title: ['', Validators.required],
      points: [null, Validators.required],
      description: ['', Validators.required],
      badge: ['', Validators.required]
    });
  }
  createTsiTier(): FormGroup {
    return this._fb.group({
      tsi_amount: [null, Validators.required]
    });
  }
  createSeed(): FormGroup {
    return this._fb.group({
      team_title: [null, Validators.required],
      team_members: this._fb.array([], Validators.minLength(1)),
    });
  }
  createMember(employee: any): FormGroup {
    return this._fb.group({
      participant: employee.id,
      participant_name: employee.repName,
      participant_tsi: employee.tsi
    });
  }
  // private _searchEmployees() {
  //  this.employees$ = concat(
  //    of([]), // default items
  //    this.employeeInput$.pipe(
  //      debounceTime(200),
  //      distinctUntilChanged(),
  //      tap(() => this.employeeLoading = true),
  //      switchMap(term => this._employeeService.getAllEmployees({ type: 'Employee', search: term }).pipe(
  //        catchError(() => of([])), // empty list on error
  //        tap(() => this.employeeLoading = false)
  //      ))
  //    )
  //  );
  // }

}
