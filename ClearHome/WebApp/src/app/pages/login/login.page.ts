import { Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { LoginModel } from '../../models/auth.model';
import { AuthStore } from '../../stores/auth/auth.store';

import { UserStore } from '../../stores/user/user.store';

declare var $: any;

@Component({
  selector: '[app-login-page]',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPageComponent implements OnInit {

  auth: LoginModel;
  error_message: string;
  loading: boolean;

  constructor(
    private _authStore: AuthStore,
    private _userStore: UserStore,
    private _router: Router
  ) { }

  ngOnInit() {
    this.auth = new LoginModel();
    this.loading = false;
    $('body').prop('class', 'body-bg-full login-page ch-theme');
  }

  authenticate(form: any): void {
    if (form.form.valid) {
      this.loading = true;
      this.auth.grant_type = 'password';
      this._authStore.logIn(this.auth).pipe(
        switchMap(data => {
          this.error_message = null;
          return this._userStore.loadCurrentUser();
        }),
        catchError(error => {
          this.error_message = error.error.error_description;
          this.loading = false;
          return throwError(error);
        })
      ).subscribe(
        data => this._router.navigate(['/home'])
      );
    }
  }

}
