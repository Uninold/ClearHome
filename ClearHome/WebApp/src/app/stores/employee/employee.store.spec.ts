import { TestBed, inject } from '@angular/core/testing';

import { EmployeeStore } from './employee.store';

describe('EmployeeStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeStore]
    });
  });

  it('should be created', inject([EmployeeStore], (service: EmployeeStore) => {
    expect(service).toBeTruthy();
  }));
});
