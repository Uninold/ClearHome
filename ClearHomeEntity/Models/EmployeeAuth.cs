﻿using System.Collections.Generic;

namespace ClearHomeEntity.Models.EmployeeAuth
{
    public class EmployeeAuthUserNameModel
    {
        public int EmployeeId { get; set; }
        public string Username { get; set; }
    }

    public class EmployeeAccount
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string username { get; set; }
        public int? employee_id { get; set; }
        public string docebo_username { get; set; }
        public int? docebo_user_id { get; set; }
    }

    public class EmployeeUserCurrent
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public int? employee_id { get; set; }
    }

    public class DoceboOAuthError
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }

    public class DoceboOAuthSuccess
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public string scope { get; set; }
        public string refresh_token { get; set; }
    }

    public class DoceboUserCreationSuccess
    {
        public DoceboUserCreationData data { get; set; }
        public string version { get; set; }
        public string[] _links { get; set; }
    }

    public class DoceboUserCreationData
    {
        public bool success { get; set; }
        public int user_id { get; set; }
    }

    public class DoceboUserCreationError
    {
        public string name { get; set; }
        public List<DoceboErrorMessage> message { get; set; }
        public int code { get; set; }
        public int status { get; set; }
    }

    public class DoceboUserUpdateError
    {
        public string name { get; set; }
        public DoceboErrorMessage message { get; set; }
        public int code { get; set; }
        public int status { get; set; }
    }

    public class DoceboUserDeletionError
    {
        public bool success { get; set; }
        public string errorMessage { get; set; }
        public int errorCode { get; set; }
    }

    public class DoceboErrorMessage
    {
        public string field { get; set; }
        public string message { get; set; }
    }

}
