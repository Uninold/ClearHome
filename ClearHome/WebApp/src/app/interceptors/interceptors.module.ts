import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthInterceptor } from './auth.interceptor';
import { NotificationInterceptor } from './notification.interceptor';

@NgModule({
  providers: [
    [
      { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: NotificationInterceptor, multi: true }
    ]
  ]
})
export class InterceptorsModule { }
