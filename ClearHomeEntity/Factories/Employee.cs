﻿using System.Collections.Generic;
using System;
using EmployeeModels = ClearHomeEntity.Models.Employee;

namespace ClearHomeEntity.Factories.Employee
{
    public class EmployeeFactory
    {
        public static EmployeeModels.EmployeeList GetEmployee(EmployeeModels.EmployeeListModel employee)
        {
            return new EmployeeModels.EmployeeList
            {
                id = employee.ID,
                first_name = employee.FirstName,
                last_name = employee.LastName,
                email = employee.Email,
                phone_number = employee.PhoneNumber,
                type = employee.Type
            };
        }

        public static List<EmployeeModels.EmployeeList> GetEmployees(ICollection<EmployeeModels.EmployeeListModel> employees)
        {
            var employeeList = new List<EmployeeModels.EmployeeList>();
            foreach (var employee in employees)
            {
                employeeList.Add(GetEmployee(employee));
            }
            return employeeList;
        }


        public static EmployeeModels.EmployeeListModelTSI GetEmployeeTSI(EmployeeModels.EmployeeListModelTSI emp)
        {
            var employee = new EmployeeModels.EmployeeListModelTSI
            {
                ID = emp.ID,
                RepName = emp.RepName,
                TSI = Math.Round((decimal)emp.TSI, 2),
            };

            return employee;
        }

        public static List<EmployeeModels.EmployeeListModelTSI> GetEmployeesTSI(ICollection<EmployeeModels.EmployeeListModelTSI> employees)
        {
            var employeeList = new List<EmployeeModels.EmployeeListModelTSI>();
            foreach (var employee in employees)
            {
                employeeList.Add(GetEmployeeTSI(employee));
            }
            return employeeList;
        }
    }
}
