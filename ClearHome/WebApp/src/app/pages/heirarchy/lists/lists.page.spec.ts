import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeirarchyListsComponent } from './lists.page';

describe('HeirarchyListsComponent', () => {
  let component: HeirarchyListsComponent;
  let fixture: ComponentFixture<HeirarchyListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeirarchyListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeirarchyListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
