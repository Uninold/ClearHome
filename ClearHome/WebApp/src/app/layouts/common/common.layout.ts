import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: '[app-common-layout]',
  templateUrl: './common.layout.html',
  styles: []
})
export class CommonLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('body').prop('class', 'header-dark ch-page sidebar-light sidebar-collapse ch-theme');
  }

}
