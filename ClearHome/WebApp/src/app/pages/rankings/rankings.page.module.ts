import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PercentagePipeModule } from '../../pipes/percentage/percentage.module';

import { RankingsPageRoutingModule } from './rankings.page.routing.module';
import { RankingsPageComponent } from './rankings.page';

@NgModule({
  imports: [
    CommonModule,
    RankingsPageRoutingModule,
    PercentagePipeModule
  ],
  declarations: [
    RankingsPageComponent,
  ]
})
export class RankingsPageModule { }
