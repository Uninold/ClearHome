﻿using System;

namespace ClearHomeEntity.DataTransferObjects.Regional
{
    public class RegionalTSIScoresQueryDTO
    {
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class RegionalNamesQueryDTO
    {
        public string search { get; set; }
    }
}
