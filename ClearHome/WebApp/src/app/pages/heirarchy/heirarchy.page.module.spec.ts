import { HeirarchyPageModule } from './heirarchy.page.module';

describe('HeirarchyPageModule', () => {
  let heirarchyModule: HeirarchyPageModule;

  beforeEach(() => {
    heirarchyModule = new HeirarchyPageModule();
  });

  it('should create an instance', () => {
    expect(heirarchyModule).toBeTruthy();
  });
});
