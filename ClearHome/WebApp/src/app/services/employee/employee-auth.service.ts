import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';

@Injectable()
export class EmployeeAuthService {

  constructor(private _http: HttpClient) { }

  getEmployeeAccounts(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      page: typeof args.page === 'undefined' ? 1 : args.page,
      limit: typeof args.limit === 'undefined' ? 50 : args.limit,
      search: typeof args.search === 'undefined' ? null : args.search
    });
    return this._http.get(`${ServerConfig.API}employee/auth`, { params }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getEmployeeAccount(id: number): Observable<any> {
    return this._http.get(`${ServerConfig.API}employee/auth/${id}`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  addEmployeeAccount(data: any): Observable<any> {
    return this._http.post(`${ServerConfig.API}employee/auth`, data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  updateEmployeeAccount(data: any): Observable<any> {
    return this._http.put(`${ServerConfig.API}employee/auth`, data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  deleteEmployeeAccount(id: number): Observable<any> {
    return this._http.delete(`${ServerConfig.API}employee/auth/${id}`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
