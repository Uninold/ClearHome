import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenVideoModalComponent } from './open-video.modal';

describe('SubmitCampaignModal', () => {
  let component: OpenVideoModalComponent;
  let fixture: ComponentFixture<OpenVideoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OpenVideoModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenVideoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
