import { EmployeePageModule } from './employee.page.module';

describe('EmployeePageModule', () => {
  let employeeModule: EmployeePageModule;

  beforeEach(() => {
    employeeModule = new EmployeePageModule();
  });

  it('should create an instance', () => {
    expect(employeeModule).toBeTruthy();
  });
});
