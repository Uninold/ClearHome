import { AddAccountModalComponent } from './add-account/add-account.modal';
import { EditAccountModalComponent } from './edit-account/edit-account.modal';

export const ACCOUNT_MODALS = [
  AddAccountModalComponent,
  EditAccountModalComponent
];
