import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonRightSidebarComponent } from './right-sidebar.component';

describe('CommonRightSidebarComponent', () => {
  let component: CommonRightSidebarComponent;
  let fixture: ComponentFixture<CommonRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
