import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserService } from '../../services/user/user.service';


@Injectable()
export class UserStore {

  private readonly _currentUser: BehaviorSubject<any> = new BehaviorSubject({});
  private readonly _currentFromDate: BehaviorSubject<any> = new BehaviorSubject({});
  private readonly _currentToDate: BehaviorSubject<any> = new BehaviorSubject({});


  constructor(
    private _userService: UserService,
  ) { }

  setcurrentToDate(toDate: any) {
    this._currentToDate.next(toDate);
  }
  setcurrentFromDate(fromDate: any) {
   this._currentFromDate.next(fromDate);
  }

  get currentToDate() {
    return this._currentToDate.asObservable();
  }
  get currentFromDate() {
    return this._currentFromDate.asObservable();
  }
  get currentToDateValue() {
    return this._currentToDate.getValue();
  }
  get currentFromDateValue() {
    return this._currentFromDate.getValue();
  }
  get currentUser() {
    return this._currentUser.asObservable();
  }

  get currentUserEmployeeId() {
    const currentUser = this._currentUser.getValue();
    if (typeof currentUser !== 'undefined') {
      return currentUser.employee_id;
    }
    return 0;
  }

  loadCurrentUser() {
    return this._userService.getCurrentUser().pipe(
      map(data => {
        this._currentUser.next(data.user);
        return data;
      })
    );
  }

}
