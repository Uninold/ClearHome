import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionsListsComponent } from './lists.page';

describe('HeirarchyListsComponent', () => {
  let component: CompetitionsListsComponent;
  let fixture: ComponentFixture<CompetitionsListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompetitionsListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionsListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
