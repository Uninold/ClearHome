export class ResponseModel {
  error: number;
  message: string;
  data: any;
}
