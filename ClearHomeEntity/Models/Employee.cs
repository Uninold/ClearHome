﻿namespace ClearHomeEntity.Models.Employee
{
    public class EmployeeListModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
    }

    public class EmployeeList
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public string type { get; set; }
    }

    public class EmployeeHierarchy
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int manager_id { get; set; }
        public int regional_id { get; set; }
        public int? director_id { get; set; }
        public int? president_id { get; set; }
    }

    public class EmployeeListModelTSI
    {
        public int ID { get; set; }
        public string RepName { get; set; }
        public decimal? TSI { get; set; }
    }

}
