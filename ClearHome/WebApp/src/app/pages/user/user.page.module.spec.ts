import { UserPageModule } from './user.page.module';

describe('UserPageModule', () => {
  let userModule: UserPageModule;

  beforeEach(() => {
    userModule = new UserPageModule();
  });

  it('should create an instance', () => {
    expect(userModule).toBeTruthy();
  });
});
