import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { EmployeePageRoutingModule } from './employee.page.routing.module';
import { PagingPipeModule } from '../../pipes/paging/paging.pipe.module';
import { PercentagePipeModule } from '../../pipes/percentage/percentage.module';

import { EmployeeListsPageComponent } from './lists/lists.page';
import { EmployeeAccountsPageComponent } from './accounts/accounts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    PagingPipeModule,
    PercentagePipeModule,
    EmployeePageRoutingModule
  ],
  declarations: [
    EmployeeListsPageComponent,
    EmployeeAccountsPageComponent
  ]
})
export class EmployeePageModule { }
