import { PagingPipeModule } from './paging.pipe.module';

describe('PagingPipeModule', () => {
  let pagingModule: PagingPipeModule;

  beforeEach(() => {
    pagingModule = new PagingPipeModule();
  });

  it('should create an instance', () => {
    expect(pagingModule).toBeTruthy();
  });
});
