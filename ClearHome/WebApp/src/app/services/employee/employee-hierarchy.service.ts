import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerConfig } from '../../config/server.config';
import { RequestHelper } from '../../helpers/request.helper';
import { EmployeeListModel } from '../../models/employee.model';

@Injectable()
export class EmployeeHierarchyService {

  constructor(private _http: HttpClient) { }

  getEmployeeHierarchy(args: any = {}): Observable<any> {
    const params = RequestHelper.Format({
      id: typeof args.id === 'undefined' ? null : args.id
    });
    return this._http.get(`${ServerConfig.API}employeehierarchy`, {params}).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
