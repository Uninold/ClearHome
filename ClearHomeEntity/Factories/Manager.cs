﻿using System;
using System.Collections.Generic;
using ManagerModels = ClearHomeEntity.Models.Manager;

namespace ClearHomeEntity.Factories.Manager
{
    public class ManagerFactory
    {
        public static ManagerModels.ManagerTSIScore GetManager(ManagerModels.ManagerTSIScoreModel manager)
        {
            var managerItem = new ManagerModels.ManagerTSIScore
            {
                name = manager.ManagerName,
                regional_name = manager.RegionalName,
                tsi = Math.Round((decimal)manager.TSI, 2),
                qsi = Math.Round((decimal)manager.QSI, 2),
                dtv_activated = manager.DTVActivated,
                auto_pay = Math.Round((decimal)0, 2),
                low_risk = Math.Round((decimal)0, 2),
                processing_fee = Math.Round((decimal)0, 2),
                same_day = Math.Round((decimal)0, 2),
                dtv_sold = manager.DTVSold,
                internet_sold = manager.InternetSold,
                cellphone_sold = manager.CellPhoneSold,
                security_sold = manager.SecuritySold,
                total_sold = manager.DTVSold + manager.InternetSold + manager.InternetSold + manager.CellPhoneSold + manager.SecuritySold
            };
            if (manager.AutoPay > 0)
            {
                managerItem.auto_pay = Math.Round((decimal)(manager.AutoPay / manager.DTVActivated), 2);
            }
            if (manager.LowRisk > 0)
            {
                managerItem.low_risk = Math.Round((decimal)(manager.LowRisk / manager.DTVActivated), 2);
            }
            if (manager.ProcessingFee > 0)
            {
                managerItem.processing_fee = Math.Round((decimal)(manager.ProcessingFee / manager.DTVActivated), 2);
            }
            if (manager.SameDay > 0)
            {
                managerItem.same_day = Math.Round((decimal)(manager.SameDay / manager.DTVActivated), 2);
            }
            return managerItem;
        }

        public static List<ManagerModels.ManagerTSIScore> GetManagers(ICollection<ManagerModels.ManagerTSIScoreModel> managers)
        {
            var managerList = new List<ManagerModels.ManagerTSIScore>();
            foreach (var manager in managers)
            {
                managerList.Add(GetManager(manager));
            }
            return managerList;
        }

    }
}
