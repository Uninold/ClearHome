import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {

  constructor(private _toastr: ToastrService) { }

  success(message?: string, title: string = 'Success'): void {
    this._toastr.success(message, title);
  }

  error(message?: string, title: string = 'Error'): void {
    this._toastr.error(message, title);
  }

  info(message?: string, title: string = 'Info'): void {
    this._toastr.info(message, title);
  }

  warning(message?: string, title: string = 'Warning'): void {
    this._toastr.warning(message, title);
  }

}
