﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModel = ClearHomeEntity.Models.Tournament;

namespace ClearHomeEntity.Factories.Tournament
{
    public class TournamentFactory
    {
        public static TournamentModel.TournamentModel GetTournament(ClearHomeEntity.Tournament tournament)
        {
            var conn = new ClearHomeContainer();
                 conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Tournament_Timelines.AsNoTracking().AsQueryable();
            var query2 = conn.Tournament_Prizes.AsNoTracking().AsQueryable();
            return new TournamentModel.TournamentModel
            {
                id = tournament.Id,
                name = tournament.Name,
                bgImage = System.Convert.ToBase64String(tournament.BgImage),
                bgImageType = tournament.BgImageType,
                promotionalVideoLink = tournament.PromotionalVideoLink,
                details = tournament.Details,
                icon = System.Convert.ToBase64String(tournament.Icon),
                iconType = tournament.IconType,
                numberOfParticipants = tournament.NumberOfParticipants,
                createdBy = tournament.CreatedBy,
                createdDateTime = tournament.CreatedDateTime,
                startDate = tournament.StartDate,
                endDate = tournament.EndDate,
                type = tournament.Type,
                format = tournament.Format,
                status = tournament.Status,
                timelines = conn.Tournament_Timelines.Where(x => x.TournamentId == tournament.Id).ToList(),
                prizes = GetPrizes(conn.Tournament_Prizes.Where(x => x.TournamentId == tournament.Id).ToList()),
                seeding = GetSeeding(conn.Tournament_Seeding.Where(x => x.TournamentId == tournament.Id).ToList()),
                //consolation = GetPrizes(conn.Tournament_Prizes.Where(x => x.TournamentId == tournament.Id && x.Place == 0).ToList()),
                brackets = GetBrackets(conn.Tournament_Seeding.Where(x => x.TournamentId == tournament.Id).ToList())

            };
        }
        public static TournamentModel.BracketModel GetBracket(ClearHomeEntity.Bracket bracket)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Brackets.AsNoTracking().AsQueryable();
            


            return new TournamentModel.BracketModel
            {
                id = bracket.Id,
                datetimeResult = bracket.DatetimeResult,
                groupNumber = bracket.GroupNumber,
                round = bracket.Round,
                roundDates = bracket.RoundDates,
                updateDatetime = bracket.UpdatedDatetime,
                updatedBy = bracket.UpdatedBy,
                winLose = bracket.WinLose,
            };
        }
        public static List<TournamentModel.BracketModel> GetBrackets(ICollection<ClearHomeEntity.Tournament_Seeding> seeding)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Tournament_Timelines.AsNoTracking().AsQueryable();
            var query2 = conn.Tournament_Prizes.AsNoTracking().AsQueryable();
            var bracketList = new List<TournamentModel.BracketModel>();

            foreach (var seed in seeding)
            {
               var brackets =  conn.Brackets.Where(x => x.TournamentId == seed.Id).ToList();
                foreach(var bracket in brackets)
                {
                    bracketList.Add(GetBracket(bracket));
                }
            }
            return bracketList;
        }
        public static TournamentModel.TournamentPrizesModel GetPrize(ClearHomeEntity.Tournament_Prizes prize)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Tournament_Timelines.AsNoTracking().AsQueryable();
            var query2 = conn.Tournament_Prizes.AsNoTracking().AsQueryable();
            return new TournamentModel.TournamentPrizesModel
            {
                id = prize.Id,
                tournamentId = prize.TournamentId,
                badge = System.Convert.ToBase64String(prize.Badge),
                badgeType = prize.BadgeType,
                description = prize.Description,
                prize_title = prize.PrizeTitle,
                points = prize.Points

            };
        }
        
        public static List<TournamentModel.TournamentPrizesModel> GetPrizes(ICollection<ClearHomeEntity.Tournament_Prizes> prizes)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Tournament_Timelines.AsNoTracking().AsQueryable();
            var query2 = conn.Tournament_Prizes.AsNoTracking().AsQueryable();
            var prizesList = new List<TournamentModel.TournamentPrizesModel>();
            foreach (var prize in prizes)
            {
                prizesList.Add(GetPrize(prize));
            }
                return prizesList;
        }
        public static TournamentModel.TournamentSeedingWithNameModel GetSeed(ClearHomeEntity.Tournament_Seeding seed)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var result = conn.Employees.Where(x => x.Id == seed.Participant).FirstOrDefault();
            var query = conn.vw2018TSI.AsNoTracking().AsQueryable();

                query = query.Where(x => x.RepId == seed.Participant);
     

            return new TournamentModel.TournamentSeedingWithNameModel
            {
                id = seed.Id,
                tournamentId = seed.TournamentId,
                repName = result.FirstName + ' ' + result.LastName,
                participant = seed.Participant,
                tsi = Math.Round((decimal)query.GroupBy(x => x.RepName).Select(x => x.Sum(s => s.TSI ?? 0)).FirstOrDefault(), 2),
                seedNumber = seed.SeedNumber
            };
        }

        public static List<TournamentModel.TournamentSeedingWithNameModel> GetSeeding(ICollection<ClearHomeEntity.Tournament_Seeding> seeding)
        {
            var conn = new ClearHomeContainer();
            conn.Database.CommandTimeout = 100;
            conn.Configuration.ProxyCreationEnabled = false;
            var query = conn.Tournament_Timelines.AsNoTracking().AsQueryable();
            var query2 = conn.Tournament_Prizes.AsNoTracking().AsQueryable();
            var seedList = new List<TournamentModel.TournamentSeedingWithNameModel>();
            foreach (var seed in seeding)
            {
                seedList.Add(GetSeed(seed));
            }
            return seedList;
        }
        //public static EmployeeAuthModels.EmployeeUserCurrent GetEmployeeAuthCurrent(ClearHomeEntity.EmployeeAuth employee)
        //{
        //    return new EmployeeAuthModels.EmployeeUserCurrent
        //    {
        //        id = employee.Id,
        //        first_name = employee.FirstName,
        //        last_name = employee.LastName,
        //        email = employee.Email,
        //        phone_number = employee.Phone,
        //        employee_id = employee.EmployeeId
        //    };
        //}

        public static List<TournamentModel.TournamentModel> GetTournaments(ICollection<ClearHomeEntity.Tournament> tournaments)
        {
            var tournamentList = new List<TournamentModel.TournamentModel>();
            foreach (var tournament in tournaments)
            {
                tournamentList.Add(GetTournament(tournament));
            }
            return tournamentList;
        }

    }

}
