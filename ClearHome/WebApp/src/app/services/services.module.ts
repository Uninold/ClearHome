import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AlertModule } from 'ngx-alerts';

import { AuthService } from './auth/auth.service';
import { NotificationService } from './notification/notification.service';
import { AlertModalService } from './alert/alert.service';
import { RegionalService } from './employee/regional.service';
import { ManagerService } from './employee/manager.service';
import { RepresentativeService } from './employee/representative.service';
import { EmployeeService } from './employee/employee.service';
import { UserService } from './user/user.service';
import { EmployeeAuthService } from './employee/employee-auth.service';
import { EmployeeHierarchyService } from './employee/employee-hierarchy.service';
import { RankingsService } from './rankings/rankings.service';
import { TournamentService } from './tournament/tournament.service';
@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      maxOpened: 1,
      autoDismiss: true
    }), 
    AlertModule.forRoot({
      maxMessages: 5,
      timeout: 5000,
      position: 'right'
    })
  ],
  providers: [
    AuthService,
    AlertModalService,
    NotificationService,
    EmployeeService,
    EmployeeAuthService,
    RegionalService,
    ManagerService,
    RepresentativeService,
    UserService,
    EmployeeHierarchyService,
    RankingsService,
    TournamentService
  ]
})
export class ServicesModule { }
