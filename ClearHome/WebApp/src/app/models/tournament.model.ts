export class TournamentModel {
  id: number;
  bgImage: string;
  bgImageType: string;
  createdBy: number;
  createdDateTime: string;
  details: string;
  startDate: string;
  endDate: string;
  format: string;
  icon: string;
  iconType: string;
  name: string;
  numberOfParticipants: number;
  promotionalVideoLink: string;
  type: string;
  status: boolean;
  prizes: Array<PrizesModel>;
  consolation: Array<PrizesModel>;
  seeding: Array<SeedingModel>;
  timelines: Array<TimelineModel>;
}

export class PrizesModel {
  id: number;
  badge: string;
  badgeType: string;
  description: string;
  place: number;
  points: number;
  tournamendId: number;
}

export class SeedingModel {
  id: number;
  participant: number;
  repName: string;
  seedNumber: number;
  tournamentId: number;
  tsi: number;
}
export class TimelineModel {
  date: string;
  description: string;
  id: number;
  title: string;
  tournamentId: number;
}

export class ByeModel {
  seed_number: number;
  selected: boolean;
}

export class BracketModel {
  tournament_seeding_id: number;
  round: string;
  win_lose: string;
  date_time_result: string;
  update_date_time: string;
  updated_by: number;
  group_number: number;
  round_dates: string;
}
