import { TestBed, inject } from '@angular/core/testing';

import { TsiSalesStore } from './tsi-sales.store';

describe('TsiSalesStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TsiSalesStore]
    });
  });

  it('should be created', inject([TsiSalesStore], (service: TsiSalesStore) => {
    expect(service).toBeTruthy();
  }));
});
