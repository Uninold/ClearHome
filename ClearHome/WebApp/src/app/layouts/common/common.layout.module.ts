import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { CommonLayoutComponent } from './common.layout';
import { CommonHeaderComponent } from './header/header.component';
import { CommonSidebarComponent } from './sidebar/sidebar.component';
import { CommonFooterComponent } from './footer/footer.component';
import { CommonRightSidebarComponent } from './right-sidebar/right-sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule.forRoot(),
    FormsModule
  ],
  declarations: [
    CommonLayoutComponent,
    CommonHeaderComponent,
    CommonSidebarComponent,
    CommonFooterComponent,
    CommonRightSidebarComponent
  ]
})
export class CommonLayoutModule { }
