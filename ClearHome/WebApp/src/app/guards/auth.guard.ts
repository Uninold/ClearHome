import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthStore } from '../stores/auth/auth.store';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private _authStore: AuthStore,
    private _router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authStore.isUserAuthorized()) {
      return true;
    }
    this._router.navigate(['/login']);
    return false;
  }

}
