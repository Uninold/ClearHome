import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListsPageComponent } from './lists.page';

describe('EmployeeListsPageComponent', () => {
  let component: EmployeeListsPageComponent;
  let fixture: ComponentFixture<EmployeeListsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeListsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeListsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
