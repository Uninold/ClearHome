﻿using System.ComponentModel.DataAnnotations;

namespace ClearHomeEntity.DataTransferObjects.EmployeeAuth
{
    public class EmployeeAuthsQueryDTO
    {
        public int page { get; set; }
        public int limit { get; set; }
        public string search { get; set; }
        public int[] ids { get; set; }
    }

    public class EmployeeAuthQueryDTO
    {
        [Required]
        public int employee_id { get; set; }
    }

    public class AddEmployeeAuthDTO
    {
        [Required]
        public int employee_id { get; set; }
        [Required]
        public string first_name { get; set; }
        [Required]
        public string last_name { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string phone_number { get; set; }
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string confirm_password { get; set; }
    }
    
    public class UpdateEmployeeAuthDTO
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int employee_id { get; set; }
        [Required]
        public string first_name { get; set; }
        [Required]
        public string last_name { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string phone_number { get; set; }
        [Required]
        public string username { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string password { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string confirm_password { get; set; }
    }

}
