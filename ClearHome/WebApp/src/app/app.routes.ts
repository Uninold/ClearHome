import { Routes } from '@angular/router';

import { LANDING_ROUTES } from './routes/landing.routes';
import { COMMON_ROUTES } from './routes/common.routes';

import { NoAuthGuard } from './guards/no-auth.guard';

// APP ROUTES
export const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  LANDING_ROUTES,
  COMMON_ROUTES,
  { path: 'login', loadChildren: './pages/login/login.page.module#LoginPageModule', canActivate: [NoAuthGuard] },
  { path: '**', redirectTo: '' }
];
