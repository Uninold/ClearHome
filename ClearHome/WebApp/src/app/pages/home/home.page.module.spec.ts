import { HomePageModule } from './home.page.module';

describe('HomePageModule', () => {
  let homeModule: HomePageModule;

  beforeEach(() => {
    homeModule = new HomePageModule();
  });

  it('should create an instance', () => {
    expect(homeModule).toBeTruthy();
  });
});
