import { Component, OnInit, AfterViewInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { UserStore } from '../../../stores/user/user.store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotificationService } from '../../../services/notification/notification.service';
import { TournamentStore } from '../../../stores/tournament/tournament.store';
import { TournamentService } from '../../../services/tournament/tournament.service';
import { TournamentModel, ByeModel, BracketModel } from '../../../models/tournament.model';
import { EmployeeHierarchyStore } from '../../../stores/employee/employee-hierarchy.store';
import { OpenVideoModalComponent } from '../../../components/modals/competitions/open-video-modal/open-video.modal';
import { EmbedVideoService } from 'ngx-embed-video';
import { url } from 'inspector';
import { AlertModalService } from '../../../services/alert/alert.service';
import { AlertService } from 'ngx-alerts';
declare var $: any;
@Component({
  selector: 'app-competition-details',
  templateUrl: './competition-details.page.html',
  styleUrls: ['./competition-details.page.css']
})
export class CompetitionDetailsComponent implements OnInit, AfterViewInit {
 
  employeeHierarchy$: Observable<any>;
  tournament$: Observable<any>;
  iframe_html: any;
  urlvideo: any;
  bracket = [];
  url_video: any;
  currentUser$: Observable<any>;
  selectedYear: number;
  firstName: string;
  loading$: Observable<boolean>;
  generated_bracket: number = 0;
  byes_counter: number = 0;
  byes_selected_counter: number = 0;
  selected_byes: Array<ByeModel>;
  brackets: Array<BracketModel> = [];
  
  constructor(
    private _modal: NgbModal,
    private _employeeHierarchyStore: EmployeeHierarchyStore,
    private _userStore: UserStore,
    private _tournamentStore: TournamentStore,
    private _route: ActivatedRoute,
    private router: Router,
    private embedService: EmbedVideoService,
    private _notification: NotificationService,
    private _alert: AlertService,
    private _tournamentService: TournamentService
  ) {
   
    let id = +this._route.snapshot.paramMap.get('id');
    this.tournament$ = this._tournamentStore.tournament;
    this.loading$ = this._tournamentStore.loading;
    this._tournamentStore.loadTournament(id).subscribe(res => {
      this.url_video = this._tournamentStore.tournamentgetValue;
      console.log(this._tournamentStore.tournamentgetValue);
      this.iframe_html = this.embedService.embed(this.url_video.promotionalVideoLink, { attr: { width: 656, height: 437 } });
    });


    
  }

  ngOnInit() {
    this.urlvideo = 'https://www.youtube.com/watch?v=qfZm_lwJaD8';
  }
  
  ngAfterViewInit(): void {
  }
  selectBye(values: any, seed) {
    if (seed.selected == true) {
      if (this.byes_selected_counter >= this.byes_counter) {
        this._notification.error("All BYEs are allocated!");
        this.byes_selected_counter++;
      }
      else if (this.byes_selected_counter < this.byes_counter) {
        this.byes_selected_counter++;
      }
    }
    else {
      this.byes_selected_counter--;
    }
    
  }
  selectionBye() {
    let limiter = 2;
    let seedlength = this._tournamentStore.tournamentgetValue.seeding.length;
    if (limiter != seedlength) {
      while (seedlength > limiter) {
        limiter = limiter * 2;
        while (seedlength < limiter) {
          this.byes_counter += 1;
          seedlength++;
        }
      }
      while (seedlength < limiter) {
        this.byes_counter += 1;
        seedlength++;
      }
    }
    this.generated_bracket = 1;
    this.selected_byes = [];
    for (let seed of this._tournamentStore.tournamentgetValue.seeding) {
      this.selected_byes.push({ seed_number : seed.seedNumber, selected : false });
    }
    if (this.byes_counter == 0) {
      this.currentSeedBracket();
      this.generated_bracket = 3;
    }
  }
  currentSeedBracket() {
    let currentData: any;
    if (this.byes_selected_counter > this.byes_counter) {
      this._notification.error("All BYEs are allocated!");
    } else {
      let tournament = [];
      currentData = {
        teams: [
        ],
        results: [
          [ /* WINNER BRACKET */
            //  [[3, 5], [2, 4], [6, 3], [2, 3], [1, 5], [5, 3], [7, 2], [1, 2]],
            //  [[1, 2], [3, 4], [5, 6], [7, 8]],
            // [[9, 1], [8, 2]],
            // [[1, 3]]
            //]
            //, [         /* LOSER BRACKET */
            //  [[5, 1], [1, 2], [3, 2], [6, 9]],
            //  [[8, 2], [1, 2], [6, 2], [1, 3]],
            //  [[1, 2], [3, 1]],
            //  [[3, 0], [1, 9]],
            //  [[3, 2]],
            //  [[4, 2]]
            //], [         /* FINALS */
            //  [[3, 8], [1, 2]],
            //  [[2, 1]]
            //  ]
          ]]
      };

      
      let limiter = 2;
      let counter = this.byes_counter;
      var byes: Array<number> = [];
      for (let bye of this.selected_byes) {
        if (bye.selected == true) {
          byes.push(bye.seed_number);
        }
      }
      if (counter == 0) {
        for (let seed of this._tournamentStore.tournamentgetValue.seeding) {
          tournament.push(seed);
        }
      } else {
        let temp = [];
        for (let i = 0; i < this._tournamentStore.tournamentgetValue.seeding.length; i++) {
          tournament.push(this._tournamentStore.tournamentgetValue.seeding[i]);
         
            if (byes.indexOf(this._tournamentStore.tournamentgetValue.seeding[i].seedNumber) !== -1) {
              if (tournament.length % 2 == 1) {
                tournament.push({ id: 0, participant: null, repName: null, tournamentId: this._tournamentStore.tournamentgetValue.seeding[0].tournamentId, tsi: null, seedNumber: null });
                counter--;
              } else {
                temp.push(tournament[tournament.length - 1]);
                tournament.pop();
                //tournament.push(this._tournamentStore.tournamentgetValue.seeding[i + 1]);
                //tournament.push(temp);
                //tournament.push({ id: 0, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
                //counter--;
              }
            }
        }
        if (tournament.length % 2 == 1) {
          tournament.push({ id: 0, participant: null, repName: null, tournamentId: this._tournamentStore.tournamentgetValue.seeding[0].tournamentId, tsi: null, seedNumber: null });
          counter--;
        } 
        for (let k = 0; k < temp.length; k++) {
          tournament.push(temp[k]);
          tournament.push({ id: 0, participant: null, repName: null, tournamentId: this._tournamentStore.tournamentgetValue.seeding[0].tournamentId, tsi: null, seedNumber: null });
          counter--;
        }
        for (let j = 0; j < counter; j++) {
          tournament.push({ id: 0, participant: null, repName: null, tournamentId: this._tournamentStore.tournamentgetValue.seeding[0].tournamentId, tsi: null, seedNumber: null });
        }
      }
      //if (limiter != tournament.length) {
      //  while (tournament.length > limiter) {
      //    limiter = limiter * 2;
      //    while (tournament.length < limiter) {
      //      this.byes_counter += 1;
      //      tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
      //    }
      //  }
      //  while (tournament.length < limiter) {
      //    tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
      //  }
      //}
      this.brackets = [];
      let bracket_counter = 1;
      for (let seed of tournament) {
        if (seed.id != null) {
          this.brackets.push({
            tournament_seeding_id: seed.id,
            date_time_result: null,
            group_number: bracket_counter,
            round: null,
            round_dates: null,
            updated_by: this._userStore.currentUserEmployeeId,
            update_date_time: null,
            win_lose: null
          });
        }
        this.bracket.push(seed.repName);
        if (this.bracket.length == 2) {
          bracket_counter++;
          currentData.teams.push(this.bracket);
          this.bracket = [];
        }
      }
      
        $(function () {
          $('#big').bracket({
            skipConsolationRound: true,
            init: currentData,
            teamWidth: 200,
            save: function(){}, /* without save() labels are disabled */
            decorator: {
              edit: acEditFn,
              render: acRenderFn}
          })
        });
      //this._tournamentService.addTournamentBracket(this.brackets).subscribe(data => {
      //  $(function () {
      //    $('#big').bracket({
      //      skipConsolationRound: true,
      //      init: currentData,
      //      teamWidth: 200
      //    })
      //  });
      //});
  }
    function acEditFn(container, data, doneCb) {
      var input = $('<input type="text">')
      input.val(data)
      input.bigData({ source: currentData })
      input.blur(function () { doneCb(input.val()) })
      input.keyup(function (e) { if ((e.keyCode || e.which) === 13) input.blur() })
      container.html(input)
      input.focus()
    }

    function acRenderFn(container, data, score, state) {
      switch (state) {
        case 'empty-bye':
          container.append('BYE')
          return;
        case 'empty-tbd':
          container.append('TBD')
          return;

        case 'entry-no-score':
        case 'entry-default-win':
        case 'entry-complete':
          //var fields = data.split(':')
          //if (fields.length != 2)
          container.append(data)
        //else
        //  container.append('<img src="site/png/' + fields[0] + '.png"> ').append(fields[1])
        //return;
      }
    }
  }
  randomBracket() {
    var tournament = [];
    this.generated_bracket = 2;
    let randomData = {
      teams: [
      ],
      results: [
        [ /* WINNER BRACKET */
          //  [[3, 5], [2, 4], [6, 3], [2, 3], [1, 5], [5, 3], [7, 2], [1, 2]],
          //  [[1, 2], [3, 4], [5, 6], [7, 8]],
          // [[9, 1], [8, 2]],
          // [[1, 3]]
          //]
          //, [         /* LOSER BRACKET */
          //  [[5, 1], [1, 2], [3, 2], [6, 9]],
          //  [[8, 2], [1, 2], [6, 2], [1, 3]],
          //  [[1, 2], [3, 1]],
          //  [[3, 0], [1, 9]],
          //  [[3, 2]],
          //  [[4, 2]]
          //], [         /* FINALS */
          //  [[3, 8], [1, 2]],
          //  [[2, 1]]
          //  ]
        ]]
    };
    for (let seed of this._tournamentStore.tournamentgetValue.seeding) {
      tournament.push(seed);
    }
    
    let limiter = 2;

    if (limiter != tournament.length) {
      while (tournament.length > limiter) {
        limiter = limiter * 2;
        while (tournament.length < limiter) {
          tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
        }
      }
      while (tournament.length < limiter) {
        tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
      }
    }
    this.brackets = [];
    let bracket_counter = 1;
    var tournamentseed = this.shuffleInPlace(tournament);
    for (let seed of tournamentseed) {
      if (seed.id != null) {
        this.brackets.push({
          tournament_seeding_id: seed.id,
          date_time_result: null,
          group_number: bracket_counter,
          round: null,
          round_dates: null,
          updated_by: this._userStore.currentUserEmployeeId,
          update_date_time: null,
          win_lose: null
        });
      }
      this.bracket.push(seed.repName);
      if (this.bracket.length == 2) {
        bracket_counter++;
        randomData.teams.push(this.bracket);
        this.bracket = [];
      }
    }
    function acEditFn(container, data, doneCb) {
      var input = $('<input type="text">')
      input.val(data)
      input.bigData({ source: randomData })
      input.blur(function () { doneCb(input.val()) })
      input.keyup(function (e) { if ((e.keyCode || e.which) === 13) input.blur() })
      container.html(input)
      input.focus()
    }

    function acRenderFn(container, data, score, state) {
      switch (state) {
        case 'empty-bye':
          container.append('BYE')
          return;
        case 'empty-tbd':
          container.append('TBD')
          return;

        case 'entry-no-score':
        case 'entry-default-win':
        case 'entry-complete':
          //var fields = data.split(':')
          //if (fields.length != 2)
          container.append(data)
        //else
        //  container.append('<img src="site/png/' + fields[0] + '.png"> ').append(fields[1])
        //return;
      }
    }
    $(function () {
      $('#big').bracket({
        skipConsolationRound: true,
        init: randomData,
        teamWidth: 200,
        save: function () { }, /* without save() labels are disabled */
        decorator: {
          edit: acEditFn,
          render: acRenderFn
        }
      })
    })
  }
  //randomBracket() {
  //  let tournament = this._tournamentStore.tournamentgetValue.seeding;
  //  console.log('randon number', this.shuffleInPlace(tournament));
  //  let bigData = {
  //    teams: [
  //    ],
  //    results: [
  //      [ /* WINNER BRACKET */
  //        //  [[3, 5], [2, 4], [6, 3], [2, 3], [1, 5], [5, 3], [7, 2], [1, 2]],
  //        //  [[1, 2], [3, 4], [5, 6], [7, 8]],
  //        // [[9, 1], [8, 2]],
  //        // [[1, 3]]
  //        //]
  //        //, [         /* LOSER BRACKET */
  //        //  [[5, 1], [1, 2], [3, 2], [6, 9]],
  //        //  [[8, 2], [1, 2], [6, 2], [1, 3]],
  //        //  [[1, 2], [3, 1]],
  //        //  [[3, 0], [1, 9]],
  //        //  [[3, 2]],
  //        //  [[4, 2]]
  //        //], [         /* FINALS */
  //        //  [[3, 8], [1, 2]],
  //        //  [[2, 1]]
  //        //  ]
  //      ]]
  //  };
  //  var hel = bigData;
  //  console.log('bigdata', hel);

  //  bigData.teams = [];
  //  bigData.results = [];
  //  //if (tournament.length % 2 == 1) {
  //  //  console.log('odd', tournament);

  //  //  tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
  //  //}
  //  tournament.push({ id: null, participant: null, repName: "arnold", tournamentId: null, tsi: null, seedNumber: null });

  //  let limiter = 2;

  //  if (limiter != tournament.length) {
  //    while (tournament.length > limiter) {

  //      console.log('limiter', limiter);
  //      limiter = limiter * 2;
  //      while (tournament.length < limiter) {
  //        tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
  //      }
  //    }
  //    while (tournament.length < limiter) {
  //      tournament.push({ id: null, participant: null, repName: null, tournamentId: null, tsi: null, seedNumber: null });
  //    }
  //  }

  //  var tournamentseed = this.shuffleInPlace(tournament);
  //  for (let seed of tournamentseed) {
  //    this.bracket.push(seed.repName);
  //    if (this.bracket.length == 2) {
  //      bigData.teams.push(this.bracket);
  //      this.bracket = [];
  //    }
  //  }
  //  function acEditFn(container, data, doneCb) {
  //    var input = $('<input type="text">')
  //    input.val(data)
  //    input.bigData({ source: bigData })
  //    input.blur(function () { doneCb(input.val()) })
  //    input.keyup(function (e) { if ((e.keyCode || e.which) === 13) input.blur() })
  //    container.html(input)
  //    input.focus()
  //  }

  //  function acRenderFn(container, data, score, state) {
  //    switch (state) {
  //      case 'empty-bye':
  //        container.append('BYE')
  //        return;
  //      case 'empty-tbd':
  //        container.append('TBD')
  //        return;

  //      case 'entry-no-score':
  //      case 'entry-default-win':
  //      case 'entry-complete':
  //        //var fields = data.split(':')
  //        //if (fields.length != 2)
  //        container.append(data)
  //      //else
  //      //  container.append('<img src="site/png/' + fields[0] + '.png"> ').append(fields[1])
  //      //return;
  //    }
  //  }
  //  $(function () {
  //    console.log('hello'); $('#big').bracket({
  //      skipConsolationRound: true,
  //      init: bigData,
  //      teamWidth: 200
  //    })
  //  })
  //}
  viewPromotionalVideo(youtuber_url: any) {
    let modal = this._modal.open(OpenVideoModalComponent, {
      size: 'lg'
    });
    modal.componentInstance.youtuber_url = youtuber_url;
  }
  shuffleInPlace<T>(array: T[]): T[] {
  // if it's 1 or 0 items, just return
  if (array.length <= 1) return array;

  // For each index in array
  for (let i = 0; i < array.length; i++) {

    // choose a random not-yet-placed item to place there
    // must be an item AFTER the current item, because the stuff
    // before has all already been placed
    const randomChoiceIndex = this.getRandomInt(i, array.length);

    // place our random choice in the spot by swapping
    [array[i], array[randomChoiceIndex]] = [array[randomChoiceIndex], array[i]];
  }

  return array;
  }
  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
  }
 
}
