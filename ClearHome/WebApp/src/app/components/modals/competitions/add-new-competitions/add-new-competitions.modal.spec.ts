import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewCompetitionsModalComponent } from './add-new-competitions.modal';

describe('AddNewCompetitionsModalComponent', () => {
  let component: AddNewCompetitionsModalComponent;
  let fixture: ComponentFixture<AddNewCompetitionsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddNewCompetitionsModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCompetitionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
