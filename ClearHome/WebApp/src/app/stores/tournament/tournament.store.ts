import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TournamentService } from '../../services/tournament/tournament.service';
import { TournamentModel } from '../../models/tournament.model';

@Injectable()
export class TournamentStore {

  private readonly _tournament: BehaviorSubject<TournamentModel> = new BehaviorSubject(new TournamentModel());
  private readonly _tournaments: BehaviorSubject<any[]> = new BehaviorSubject([]);

  private readonly _total: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _total_filtered: BehaviorSubject<number> = new BehaviorSubject(0);
  private readonly _search: BehaviorSubject<string> = new BehaviorSubject(null);
  private readonly _page: BehaviorSubject<number> = new BehaviorSubject(1);
  private readonly _limit: BehaviorSubject<number> = new BehaviorSubject(50);
  private readonly _loading: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(private _tournamentService: TournamentService) { }

  get tournament() {
    return this._tournament.asObservable();
  }

  get tournamentgetValue() {
    return this._tournament.getValue();
  }

  get tournaments() {
    return this._tournaments.asObservable();
  }
  get total() {
    return this._total.asObservable();
  }

  get total_filtered() {
    return this._total_filtered.asObservable();
  }

  get search() {
    return this._search.asObservable();
  }

  get page() {
    return this._page.asObservable();
  }

  get limit() {
    return this._limit.asObservable();
  }

  get loading() {
    return this._loading.asObservable();
  }

  setSearch(search: string) {
    this._search.next(search);
  }

  setPage(page: number) {
    this._page.next(page);
  }

  setLoading(loading: boolean) {
    this._loading.next(loading);
  }

  loadTournament(id: number): Observable<any> {
    this._loading.next(true);

    return this._tournamentService.getTournament(id).pipe(
      map(data => {
        this._tournament.next(data.tournament);
        this._loading.next(false);
        return data;
      })
    );
  }

  loadTournaments(): Observable<any> {
    this._loading.next(true);

    return this._tournamentService.getTournaments().pipe(
      map(data => {
        this._tournaments.next(data.tournamentList);
        this._loading.next(false);
        return data;
      })
    );
  }
}
