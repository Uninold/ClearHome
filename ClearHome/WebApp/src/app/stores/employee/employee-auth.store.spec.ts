import { TestBed, inject } from '@angular/core/testing';

import { EmployeeAuthStore } from './employee-auth.store';

describe('EmployeeAuthStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeAuthStore]
    });
  });

  it('should be created', inject([EmployeeAuthStore], (service: EmployeeAuthStore) => {
    expect(service).toBeTruthy();
  }));
});
