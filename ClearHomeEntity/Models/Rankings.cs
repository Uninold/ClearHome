﻿namespace ClearHomeEntity.Models.Rankings
{
    public class RankingsListModel
    {
        public int CMSID { get; set; }
        public string RepName { get; set; }
        public string ManagerName { get; set; }
        public string RegionalName { get; set; }
        public decimal? TSI { get; set; }
        public decimal? DTVActivated { get; set; }
        public decimal? LowRisk { get; set; }
        public decimal? AutoPay { get; set; }
        public decimal? SameDay { get; set; }
        public decimal? ProcessingFee { get; set; }
        public decimal? AdditionalServices { get; set; }
        public decimal? InternetSold { get; set; }
        public decimal? CellPhoneSold { get; set; }
        public decimal? SecuritySold { get; set; }
    }

    public class EmployeeList
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public string type { get; set; }
    }

    public class EmployeeHierarchy
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int manager_id { get; set; }
        public int regional_id { get; set; }
        public int? director_id { get; set; }
        public int? president_id { get; set; }
    }

}
