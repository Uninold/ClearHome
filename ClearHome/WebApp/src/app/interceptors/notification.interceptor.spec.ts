import { TestBed, inject } from '@angular/core/testing';

import { NotificationInterceptor } from './notification.interceptor';

describe('NotificationInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationInterceptor]
    });
  });

  it('should be created', inject([NotificationInterceptor], (service: NotificationInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
