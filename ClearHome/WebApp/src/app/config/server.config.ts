export class ServerConfig {
  public static get URL(): string {
    return 'http://hub.tulgy.com/';
  }
  public static get API(): string {
    return 'http://hub.tulgy.com/api/';
  }
}
