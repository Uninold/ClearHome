import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LandingLayoutComponent } from './landing.layout';
import { LandingHeaderComponent } from './header/header.component';
import { LandingFooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    LandingLayoutComponent,
    LandingHeaderComponent,
    LandingFooterComponent
  ]
})
export class LandingLayoutModule { }
